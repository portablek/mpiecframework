﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    /// <summary>
    /// Class to hold MPiec controller memory status
    /// </summary>
    public class MPiecMemory
    {
        public UInt32 ApplicationSize { get; set; }
        public UInt32 ApplicationFree { get; set; }

        public UInt32 DataSize { get; set; }
        public UInt32 DataFree { get; set; }

        public UInt32 RetainSize { get; set; }
        public UInt32 RetainFree { get; set; }
    }
}
