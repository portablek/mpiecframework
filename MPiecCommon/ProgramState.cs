﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    /// <summary>
    /// State of the PLC on the device
    /// </summary>
    public enum ProgramState
    {
        /// <summary>
        /// Ready to run
        /// </summary>
        On,

        /// <summary>
        /// Program is loading
        /// </summary>
        Loading,
        
        /// <summary>
        /// Program is loaded will start running
        /// </summary>
        Starting,
        
        /// <summary>
        /// Program is running
        /// </summary>
        Running,
        
        /// <summary>
        /// A breakpoint has been requested, but is not yet reached
        /// </summary>
        HaltRequested,
        
        /// <summary>
        /// Breakpoint reached, program halted
        /// </summary>
        Halt,
        
        /// <summary>
        /// Program will stop running
        /// </summary>
        Stopping,
        
        /// <summary>
        /// Program is stopped
        /// </summary>
        Stop,
        
        /// <summary>
        /// Program reset has been requested
        /// </summary>
        Resetting
    }
}
