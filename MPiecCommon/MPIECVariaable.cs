﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MPiecFramework
{
    /// <summary>
    /// Class to represent a variable on the MPiec device
    /// </summary>
    public class MPiecVariable
    {
        /// <summary>
        /// Static location to indicate globally-accessable variable
        /// </summary>
        public static string Global = "GlobalVariables";

        /// <summary>
        /// Static location to indicate instance-local variable
        /// </summary>
        public static string Instance = "InstanceVariables";

        /// <summary>
        /// Where the variable is stored on the device.  Usually "GlobalVariables" or "InstanceVariables", may also contain
        /// structure names if variable is a field in an on-device structure.
        /// </summary>
        public string Location { get; private set; }

        /// <summary>
        /// Variable Name on device.  May contain dot-syntax structure and field name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Variable type as defined in MotionWorks
        /// </summary>
        public MotionWorksType VariableType { get; private set; }

        /// <summary>
        /// Variable value
        /// </summary>
        public dynamic Value { get; set; }

        /// <summary>
        /// TRUE if variable is BOOL and the given value evaluates to true, or is numeric and set to non-zero, FALSE otherwise (including null value)
        /// If called with a dictionary of values that contains a value for this variable (by DeviceName), returns TRUE if the dictionary value is true
        /// </summary>
        /// <param name="VariableValues">Dictionary of variable values to use</param>
        public bool IsTrue(Dictionary<string, dynamic> VariableValues = null)
        {
            dynamic thisValue = Value;

            if (VariableValues == null)
                return false;

            if (VariableValues.Keys.Contains(Name))
                thisValue = VariableValues[Name];
            else if (VariableValues.Keys.Contains(DeviceName))
                thisValue = VariableValues[DeviceName];

            return IsTrue(thisValue);
        }

        /// <summary>
        /// TRUE if variable is BOOL and the given value evaluates to true, or is numeric and set to non-zero, FALSE otherwise (including null value)
        /// If called with a dictionary of values that contains a value for this variable (by DeviceName), returns TRUE if the dictionary value is true
        /// </summary>
        /// <param name="Value">Value to evaluate for true</param>
        public bool IsTrue(dynamic Value)
        {
            if (Value == null)
                return false;

            if (VariableType == MotionWorksType.BOOL)
            {
                bool isSet = false;
                bool.TryParse(Convert.ToString(Value).Trim(), out isSet);
                return isSet;
            }

            return ((float)Convert.ChangeType(Value, typeof(float)) == 0);
        }

        /// <summary>
        /// Get a string to use for setting the controller variable to the given value
        /// </summary>
        /// <param name="Value">Value to set variable to</param>
        /// <returns>Text to send to write to the variable</returns>
        public string GetWriteCommand(object Value)
        {
            return Name + ":" + VariableType + "=" + Value.ToString();
            //BoolInput1: BOOL = true,BoolInput2: BOOL = false,RealInput1: REAL = 1.234,RealInput2: REAL = 0.251
        }

        /// <summary>
        /// Get a string to use for reading the controller variable's current value
        /// </summary>
        /// <returns>Text to send to read the variable</returns>
        public string GetReadCommand()
        {
            return Name + ":" + VariableType;
        }

        /// <summary>
        /// Fully decorated name of variable on device
        /// </summary>
        public string DeviceName { get { return "@" + Location + "." + Name; } }

        /// <summary>
        /// Create a new, empty variable in the Global variable space
        /// </summary>
        public MPiecVariable()
        {
            this.Location = MPiecVariable.Global;
            this.Name = "NewVariable";
            this.VariableType = MotionWorksType.DWORD;
            this.Value = null;
        }

        /// <summary>
        /// Create a new variable instance
        /// </summary>
        /// <param name="Location">Where the variable is located on the device (MPIECVariable.Global, "InstanceVariables", etc). May contain a structure name.</param>
        /// <param name="Name">Name of variable on device. May contain a strucutre and field names seperated by "."</param>
        /// <param name="VarType">Variable type as defined in MotionWorks</param>
        /// <param name="Default">Value to assign to this instance at creation; may not reflect value on device</param>
        public MPiecVariable(string Location, string Name, MotionWorksType VarType, dynamic Default = null)
        {
            this.Location = Location;
            this.Name = Name;
            this.VariableType = VarType;
            this.Value = Default;
        }

        /// <summary>
        /// Create a new variable instance from a Variable Definition string.  Variable Definition Strings have the format:
        /// 
        /// "VARNAME:VARTYPE" or "VARNAME:VARTYPE=VALUE"
        /// 
        /// where:
        /// VARNAME: Name of variable on device
        /// VARTYPE: Variable type as defined in MotionWorks
        /// VALUE: String representation of variable value
        /// </summary>
        /// <param name="Definition">Definition string of variable instance</param>
        /// <param name="Location">Where variable is located on device ("InstanceVariables", etc). May contain a structure name. If not specified, MPIECVariable.Global is assumed.</param>
        /// <remarks>If unable to parse the definition for any reason, throws an ArgumentException with innerexception set to specific error</remarks>
        public MPiecVariable(string Definition, string Location = null)
        {
            string VarName;
            MotionWorksType VarType;
            dynamic Value = null;

            try
            {
                string[] Parts = Definition.Split(':');
                VarName = Parts[0];

                string[] TypeVal = Parts[1].Split('=');
                VarType = (MotionWorksType)Enum.Parse(typeof(MotionWorksType), TypeVal[0]);
                if (TypeVal.Count() > 1)
                    Value = VarType.Parse(TypeVal[1]);

                if (String.IsNullOrEmpty(Location))
                    Location = Global;
            } 
            catch (Exception ex)
            {
                throw new ArgumentException("Unable to parse variable definition: \"" + Definition + "\"", ex);
            }
            this.Location = Location;
            this.Name = VarName;
            this.VariableType = VarType;
            this.Value = Value;
        }

        /// <summary>
        /// Create an instance of a global variable on the device
        /// </summary>
        /// <param name="Name">Variable name</param>
        /// <param name="VarType">Variable type as defined in MotionWorks</param>
        /// <param name="Default">Value for this instance at creation (may not reflect value of variable on device)</param>
        /// <returns>New variable instance</returns>
        static public MPiecVariable GlobalVariable(string Name, MotionWorksType VarType, dynamic Default = null)
        {
            return new MPiecVariable(MPiecVariable.Global, Name, VarType, Default);
        }

        /// <summary>
        /// Create an instance of an local variable on the device
        /// </summary>
        /// <param name="Name">Variable name.  This is usually in the format "RESOURCE.STRUCT.NAME", etc.</param>
        /// <param name="VarType">Variable type as defined in MotionWorks</param>
        /// <param name="Default">Value for this instance at creation (may not reflect value of variable on device)</param>
        /// <returns>New variable instance</returns>
        static public MPiecVariable LocalVariable(string Name, MotionWorksType VarType, dynamic Default = null)
        {
            return new MPiecVariable(MPiecVariable.Instance, Name, VarType, Default);
        }
    }
}
