﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Events
{
    /// <summary>
    /// Arguments for errors encountered when attempting to read to variables on the device 
    /// </summary>
    public class MPIECVariableReadErrorEventArgs : MPIECEventArgs
    {
        /// <summary>
        /// Collection of variable values being read
        /// </summary>
        public List<MPiecVariable> VariableSettings;

        public MPIECVariableReadErrorEventArgs(List<MPiecVariable> Variables, Exception generatingException) : base(generatingException)
        {
            VariableSettings = Variables;
        }
    }
}
