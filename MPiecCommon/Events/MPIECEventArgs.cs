﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Events
{
    /// <summary>
    /// General arguments passed to all MPiec controller events
    /// </summary>
    public class MPIECEventArgs : EventArgs
    {
        /// <summary>
        /// Time/date event occured, in UTC
        /// </summary>
        public DateTime Timestamp { get; internal set; } = DateTime.UtcNow;

        /// <summary>
        /// General messages associated with this event, may be empty
        /// </summary>
        virtual public String Message { get
            {
                return ((GeneratingException != null) ? GeneratingException.Message : string.Empty);
            }
        }

        /// <summary>
        /// Does this event have an associated Message property?
        /// </summary>
        public bool HasMessage {  get { return !String.IsNullOrEmpty(Message); } }

        /// <summary>
        /// Caught exception that generated this event, if any
        /// </summary>
        public Exception GeneratingException { get; internal set; } = null;

        /// <summary>
        /// Create a new MPIECEventArgs instance with a generating exception
        /// </summary>
        /// <param name="generatingException">Caught exception that generated this event</param>
        public MPIECEventArgs(Exception generatingException) : base()
        {
            this.GeneratingException = generatingException;
        }
    }
}
