﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Events
{
    /// <summary>
    /// Arguments for errors in attempting to control the PLC within MPiec controllers
    /// These error events generally have a generating exception from underlying code with a more specific error message.
    /// </summary>
    public class MPIECControlErrorEventArgs : MPIECEventArgs
    {
        public MPIECControlErrorEventArgs(Exception generatingException = null) : base(generatingException)
        { }
    }

}
