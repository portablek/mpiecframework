﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Events
{
    /// <summary>
    /// Arguments for errors encountered when attempting to write to variables on the device 
    /// </summary>
    public class MPIECVariableWriteErrorEventArgs : MPIECEventArgs
    {
        /// <summary>
        /// Collection of variable values being written
        /// </summary>
        public List<MPiecVariable> VariableSettings;

        public MPIECVariableWriteErrorEventArgs(List<MPiecVariable> Variables, Exception generatingException) : base(generatingException)
        {
            VariableSettings = Variables;
        }
    }
}