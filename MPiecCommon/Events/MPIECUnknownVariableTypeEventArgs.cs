﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Events
{
    /// <summary>
    /// Arguments for error events when attempting to access a variable on the device of unknown or unsupported type
    /// These error events generally have a generating exception from underlying code with a more specific error message.
    /// </summary>
    public class MPIECUnknownVariableTypeEventArgs : MPIECEventArgs
    {
        /// <summary>
        /// Variable type that caused this event
        /// </summary>
        public MotionWorksType VariableType { get; private set; }
        public MPIECUnknownVariableTypeEventArgs(MotionWorksType variableType, Exception generatingException = null) : base(generatingException)
        {
            VariableType = variableType;
        }
    }
}