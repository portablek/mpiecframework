﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Events
{
    /// <summary>
    /// Arguments for error events when attempting to read or write settings on the device
    /// These error events generally have a generating exception from underlying code with a more specific error message.
    /// </summary>
    public class MPIECSettingNotFoundEventArgs : MPIECEventArgs
    {
        /// <summary>
        /// Namae of the setting that caused this event
        /// </summary>
        public string Setting { get; private set; }

        public MPIECSettingNotFoundEventArgs(string SettingName, Exception generatingException = null) : base(generatingException)
        {
            Setting = SettingName;
        }
    }
}
