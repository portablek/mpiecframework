﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Events
{
    /// <summary>
    /// Arguments when an error is encountered reading or writing a file on the device
    /// </summary>
    public class MPIECFileEventArgs : MPIECEventArgs
    {
        /// <summary>
        /// Name of file on device being accessed
        /// </summary>
        public string Filename { get; private set; }

        /// <summary>
        /// TRUE if the error was encountered attempting to write to the file, FALSE if the error was reading the file
        /// </summary>
        public bool WasWriting { get; private set; }

        public MPIECFileEventArgs(string fileName, bool WasWriting, Exception generatingException) : base(generatingException)
        {
            Filename = fileName;
            this.WasWriting = WasWriting;
        }
    }
}
