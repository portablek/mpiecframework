﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Events
{
    /// <summary>
    /// Arguments for missing, unknown, or invalid subscription name errors
    /// These error events generally have a generating exception from underlying code with a more specific error message.
    /// </summary>
    public class MPIECInvalidSubscriptionEventArgs : MPIECEventArgs
    {
        /// <summary>
        /// The subscription id that caused this event
        /// </summary>
        public string ID { get; private set; }

        public MPIECInvalidSubscriptionEventArgs(String SubscriptionID, Exception generatingException) : base(generatingException)
        {
            this.ID = SubscriptionID;
        }
    }
}
