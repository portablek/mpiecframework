﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Events
{
    /// <summary>
    /// Arguments for general errors in communicating with MPiec controllers (ethernet timeout/reachability errors, etc)
    /// These error events generally have a generating exception from underlying code with a more specific error message.
    /// </summary>
    public class MPIECCommunicationErrorEventArgs : MPIECEventArgs
    {
        public MPIECCommunicationErrorEventArgs(Exception generatingException = null) : base(generatingException)
        { }
    }
}
