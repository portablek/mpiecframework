﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Events
{
    /// <summary>
    /// Successful variable read event happened
    /// </summary>
    public class MPIECSubscriptionReadEventArgs : EventArgs
    {
        /// <summary>
        /// Dictionary containing (VariableNmae):(Value) pairs
        /// </summary>
        public Dictionary<string, dynamic> VariableValues { get; private set; }

        public MPIECSubscriptionReadEventArgs(Dictionary<string, dynamic> VariableInfo) : base()
        {
            this.VariableValues = VariableInfo;
        }
    }
}
