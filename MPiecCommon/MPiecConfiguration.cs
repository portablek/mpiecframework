﻿using System;
using System.Collections.Generic;
using System.IO;
using YamlDotNet.Serialization;

namespace MPiecFramework
{
    /// <summary>
    /// Definition of MP on-device variables for a generalized motion control axis
    /// </summary>
    public class MPiecAxis
    {
        public string AxisName { get; set; }

        // Read Variables
        public MPiecVariable isEnabled { get; set; } = null;
        public MPiecVariable isAtTarget { get; set; } = null;
        public MPiecVariable isMovingToTarget { get; set; } = null;
        public MPiecVariable isHoming { get; set; } = null;
        public MPiecVariable isJogging { get; set; } = null;
        public MPiecVariable isMoving { get; set; } = null;
        public MPiecVariable isAtHome { get; set; } = null;
        public MPiecVariable isAlarmed { get; set; } = null;
        public MPiecVariable CurrentPosition { get; set; } = null;
        public MPiecVariable CurrentSpeed { get; set; } = null;
        public MPiecVariable isHot { get; set; } = null;

  
        // Write Variables
        public MPiecVariable TargetPosition { get; set; } = null;
        public MPiecVariable GoToTarget { get; set; } = null;
        public MPiecVariable GoToHome { get; set; } = null;
        public MPiecVariable JogForward { get; set; } = null;
        public MPiecVariable JogReverse { get; set; } = null;
        public MPiecVariable SetEncoderPosition { get; set; } = null;
        public MPiecVariable ClearAlarm { get; set; } = null;
        public MPiecVariable SetEnabled { get; set; } = null;

        public MPiecVariable StopMotion { get; set; } = null;
        public MPiecVariable SetSpeed { get; set; } = null;

        // Read/Write Variables
        public MPiecVariable jogSpeed { get; set; } = null;


        // Instantaneous State

        /// <summary>
        /// Is the axis enabled? (returns false if unable to determine)
        /// </summary>
        public bool Enabled => isEnabled != null && isEnabled.IsTrue();

        /// <summary>
        /// Is the axis alarmed? (returns false if unable to determine)
        /// </summary>
        public bool Alarmed => isAlarmed != null && isAlarmed.IsTrue();

        /// <summary>
        /// Is the axis at the home position? (returns false if unable to determine)
        /// </summary>
        public bool AtHome => isAtHome != null && isAtHome.IsTrue();

        /// <summary>
        /// Is the axis at the targeted position? (returns false if unable to determine)
        /// </summary>
        public bool AtTarget { get
            {
                if (isAtTarget != null)
                    return isAtTarget.IsTrue();
                else if ((CurrentPosition != null) && (TargetPosition != null))
                    return CurrentPosition.Value == TargetPosition.Value;
                else
                    return false;
                    
            } 
        }

        public bool inMotion => ((isEnabled != null) ? Enabled : true) && ((isAlarmed != null) ? Alarmed : true) && !AtTarget;

        public MPiecAxis(string AxisName)
        {
            this.AxisName = AxisName;
        }

        public MPiecAxis()
        {
            this.AxisName = "New Axis";
        }

        public IEnumerable<MPiecVariable> MonitorVariables
        {
            get
            {
                List<MPiecVariable> Variables = new List<MPiecVariable>();
                if (isEnabled != null)
                    Variables.Add(isEnabled);
                if (isAtTarget != null)
                    Variables.Add(isAtTarget);
                if (isAtHome != null)
                    Variables.Add(isAtHome);
                if (isAlarmed != null)
                    Variables.Add(isAlarmed);
                if (isMovingToTarget != null)
                    Variables.Add(isMovingToTarget);
                if (isMoving != null)
                    Variables.Add(isMoving);
                if (isHoming != null)
                    Variables.Add(isHoming);
                if (isJogging != null)
                    Variables.Add(isJogging);
                if (CurrentPosition != null)
                    Variables.Add(CurrentPosition);
                if (CurrentSpeed != null)
                    Variables.Add(CurrentSpeed);
                if (isHot != null)
                    Variables.Add(isHot);
                return Variables;
            }
        }

    }

    /// <summary>
    /// Definition of MP on-device variables for a generalized motion control program
    /// </summary>
    public class MPiecConfiguration
    {
        public String Name { get; set; }
        public MPiecVariable isEnabled { get; set; }
        public MPiecVariable setEnabled { get; set; }
        public MPiecVariable clearAllAlarms { get; set; }
        public MPiecVariable stopAllMotion { get; set; }
        public List<MPiecAxis> Axes { get; set; } = new List<MPiecAxis>();

        public MPiecVariable jogSpeed { get; set; }
        public MPiecVariable jogAcceleration { get; set; }
        public MPiecVariable jogDeceleration { get; set; }

        static public MPiecConfiguration Load(string filePath)
        {
            using (StreamReader fileReader = File.OpenText(filePath))
            {
                DeserializerBuilder builder = new DeserializerBuilder();
                builder.WithTagMapping("!MPiecConfiguration", typeof(MPiecFramework.MPiecConfiguration));
                return builder.Build().Deserialize(fileReader) as MPiecConfiguration;
            }
        }

        public void Save(string filePath)
        {
            using (StreamWriter fileWriter = File.CreateText(filePath))
            {
                SerializerBuilder builder = new SerializerBuilder();
                builder.EnsureRoundtrip();
                builder.ConfigureDefaultValuesHandling(DefaultValuesHandling.OmitNull);
                builder.WithTagMapping("!MPiecConfiguration", typeof(MPiecFramework.MPiecConfiguration));
                builder.Build().Serialize(fileWriter, this);
            }
        }
    }
}
