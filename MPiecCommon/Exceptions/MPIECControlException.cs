﻿using MPiecFramework.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Exceptions
{
    /// <summary>
    /// General error attempting to control the PLC of the motion controller
    /// </summary>
    public class MPIECControlException : MPIECException
    {
        public MPIECControlException(MPIECControlErrorEventArgs args = null) : base(args)
        { }

        public MPIECControlException(string Message, MPIECControlErrorEventArgs args = null) : base(Message, args)
        { }

        public MPIECControlException(string Message, Exception inner, MPIECControlErrorEventArgs args = null) : base(Message, inner, args)
        { }
    }

}
