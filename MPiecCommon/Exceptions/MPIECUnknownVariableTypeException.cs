﻿using MPiecFramework.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Exceptions
{
    /// <summary>
    /// Unknown variable type when attempting to read or write a variable on the device
    /// </summary>
    public class MPIECUnknownVariableTypeException : MPIECException
    {
        public MPIECUnknownVariableTypeException(MPIECUnknownVariableTypeEventArgs args = null) : base(args)
        { }

        public MPIECUnknownVariableTypeException(string Message, MPIECUnknownVariableTypeEventArgs args = null) : base(Message, args)
        { }

        public MPIECUnknownVariableTypeException(string Message, Exception inner, MPIECUnknownVariableTypeEventArgs args = null) : base(Message, inner, args)
        { }
    }
}
