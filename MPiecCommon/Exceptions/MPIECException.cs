﻿using MPiecFramework.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Exceptions
{
    /// <summary>
    /// General MPiec controller-related exception
    /// </summary>
    public class MPIECException : Exception
    {
        /// <summary>
        /// Arguments associated with the event that caused this exception
        /// </summary>
        public MPIECEventArgs Args { get; private set; }

        public MPIECException(MPIECEventArgs args = null) : base(null, args?.GeneratingException)
        {
            this.Args = args;
        }

        public MPIECException(string Message, MPIECEventArgs args = null) : base(Message, args?.GeneratingException)
        {
            this.Args = args;
        }

        public MPIECException(string Message, Exception inner, MPIECEventArgs args = null) : base(Message, inner)
        { 
            this.Args = args;
        }
    }
}
