﻿using MPiecFramework.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Exceptions
{
    /// <summary>
    /// General error attempting to communicatie with the motion controller
    /// </summary>
    public class MPIECCommunicationException : MPIECException
    {
        public MPIECCommunicationException(MPIECCommunicationErrorEventArgs args = null) : base(args)
        { }

        public MPIECCommunicationException(string Message, MPIECCommunicationErrorEventArgs args = null) : base(Message, args)
        { }

        public MPIECCommunicationException(string Message, Exception inner, MPIECCommunicationErrorEventArgs args = null) : base(Message, inner, args)
        { }
    }
}
