﻿using MPiecFramework.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Exceptions
{
    /// <summary>
    /// Error encountered attempting to write variable values to the device
    /// </summary>
    public class MPIECVariableWriteException : MPIECException
    {
        public MPIECVariableWriteException(MPIECVariableWriteErrorEventArgs args = null) : base(args)
        { }

        public MPIECVariableWriteException(string Message, MPIECVariableWriteErrorEventArgs args = null) : base(Message, args)
        { }

        public MPIECVariableWriteException(string Message, Exception inner, MPIECVariableWriteErrorEventArgs args = null) : base(Message, inner, args)
        { }
    }
}
