﻿using MPiecFramework.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Exceptions
{
    /// <summary>
    /// Setting name not found on device when attempting to read or write a value
    /// </summary>
    public class MPIECSettingNotFoundException : MPIECException
    {
        public MPIECSettingNotFoundException(MPIECSettingNotFoundEventArgs args = null) : base(args)
        { }

        public MPIECSettingNotFoundException(string Message, MPIECSettingNotFoundEventArgs args = null) : base(Message, args)
        { }

        public MPIECSettingNotFoundException(string Message, Exception inner, MPIECSettingNotFoundEventArgs args = null) : base(Message, inner, args)
        { }
    }
}
