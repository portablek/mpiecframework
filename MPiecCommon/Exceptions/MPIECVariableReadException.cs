﻿using MPiecFramework.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Exceptions
{
    /// <summary>
    /// Error encountered attempting to read variable values to the device
    /// </summary>
    public class MPIECVariableReadException : MPIECException
    {
        public MPIECVariableReadException(MPIECVariableReadErrorEventArgs args = null) : base(args)
        { }

        public MPIECVariableReadException(string Message, MPIECVariableReadErrorEventArgs args = null) : base(Message, args)
        { }

        public MPIECVariableReadException(string Message, Exception inner, MPIECVariableReadErrorEventArgs args = null) : base(Message, inner, args)
        { }
    }
}
