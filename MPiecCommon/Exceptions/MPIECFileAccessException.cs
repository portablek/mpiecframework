﻿using MPiecFramework.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Exceptions
{
    /// <summary>
    /// Error encountered attempting to read or write a file on the device
    /// </summary>
    public class MPIECFileAccessException : MPIECException
    {
        public MPIECFileAccessException(MPIECFileEventArgs args = null) : base(args)
        { }

        public MPIECFileAccessException(string Message, MPIECFileEventArgs args = null) : base(Message, args)
        { }

        public MPIECFileAccessException(string Message, Exception inner, MPIECFileEventArgs args = null) : base(Message, inner, args)
        { }
    }
}
