﻿using MPiecFramework.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework.Exceptions
{
    /// <summary>
    /// Subscription id invalid or unknown when attempting to read or write a value
    /// </summary>
    public class MPIECInvalidSubscriptionException : MPIECException
    {
        public MPIECInvalidSubscriptionException(MPIECInvalidSubscriptionEventArgs args = null) : base(args)
        { }

        public MPIECInvalidSubscriptionException(string Message, MPIECInvalidSubscriptionEventArgs args = null) : base(Message, args)
        { }

        public MPIECInvalidSubscriptionException(string Message, Exception inner, MPIECInvalidSubscriptionEventArgs args = null) : base(Message, inner, args)
        { }
    }
}
