﻿using MPiecFramework.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    /// <summary>
    /// General PLCi-based interface to all Yaskawa MPiec eCLR Controllers 
    /// (except for  MP2300Siec, as mentioned in AN.MPIEC.29.pdf)
    /// </summary>
    public interface IMPiec : IDisposable
    {
        #region Public Properties

        /// <summary>
        /// Has a connection successfully been made to this controller? (i.e., is the IPAddress and other parameters correct?)
        /// </summary>
        bool HasConnected { get; }

        /// <summary>
        /// IP Address of currently-connected controller, null if not conected
        /// </summary>
        string IPAddress { get; }

        /// <summary>
        /// TCP port number used to communicate with controller
        /// </summary>
        UInt16 Port { get; } 

        /// <summary>
        /// Controller communication timeout value, is ms
        /// </summary>
        UInt16 Timeout { get; set; }

        #endregion

        #region Error and Event Handling

        /// <summary>
        /// General error encountered attempting to communicate with controller (ethernet timeout/reachability issue, etc)
        /// </summary>
        event EventHandler<MPIECCommunicationErrorEventArgs> CommunicationError;        

        /// <summary>
        /// Setting name not found when attempting to read or write a device setting
        /// </summary>
        event EventHandler<MPIECSettingNotFoundEventArgs> SettingNotFound;
        
        /// <summary>
        /// Invalid or unknown subscription identifier specified when subscribing to or reading values on device
        /// </summary>
        event EventHandler<MPIECInvalidSubscriptionEventArgs> InvalidSubscription;
        
        /// <summary>
        /// Error encountered attempting to read values to variables on device
        /// </summary>
        event EventHandler<MPIECVariableReadErrorEventArgs> ErrorReadingVariables;        

        /// <summary>
        /// Error encountered attempting to write values to variables on device
        /// </summary>
        event EventHandler<MPIECVariableWriteErrorEventArgs> ErrorWritingVariables;        

        /// <summary>
        /// Error attempting to control the PLC on the motion controller
        /// </summary>
        event EventHandler<MPIECControlErrorEventArgs> PLCControlError;        

        /// <summary>
        /// Error attempting to read a file on the device
        /// </summary>
        event EventHandler<MPIECFileEventArgs> FileReadError;        

        /// <summary>
        /// Error attempting to write a file on the device
        /// </summary>
        event EventHandler<MPIECFileEventArgs> FileWriteError;

        #endregion

        #region Lifecycle

        /// <summary>
        /// Attempt to connect to a MPiec controller at a specified IP address
        /// </summary>
        /// <param name="IPAddress">IPv4 dot-notation IP address of controller ("192.168.1.1", etc)</param>
        /// <param name="Timeout">If non-zero, set the communication timeout value to this number of milliseconds</param>
        /// <param name="Port">If non-zero, connect to this TCP port number</param>
        /// <returns>TRUE if controller found and connected, FALSE if unable to connect</returns>
        Task<bool> Connect(string IPAddress, UInt16 Timeout = 0, UInt16 Port = 0);

        /// <summary>
        /// Logically disconnect from the current controller, resetting all state to defaults
        /// </summary>
        /// <returns></returns>
        Task Disconnect();

        #endregion

        #region Device Attribute Getters

        /// <summary>
        /// Get the specific manufacturer of the controller
        /// </summary>
        /// <returns>Manufacuter company name ("Yaskawa Electric America, Inc.", etc)</returns>
        Task<string> GetManufacturer();

        /// <summary>
        /// Get the specific controller model name
        /// </summary>
        /// <returns>Controller model ("MP3300iec", etc)</returns>
        Task<string> GetProductName();

        /// <summary>
        /// Get a unique identity string for the connected device
        /// </summary>
        /// <returns>Unique identity string embedded in the device hardware</returns>
        Task<string> GetDeviceIdentity();

        /// <summary>
        /// Return a string representing the device hardware version
        /// </summary>
        /// <returns>Device hardware version string</returns>
        Task<string> GetHardwareVersion();

        /// <summary>
        /// Return a string representing the device firmware version
        /// </summary>
        /// <returns>Device firmware revision string</returns>
        Task<string> GetFirmwareVersion();

        /// <summary>
        /// Return a string representing the device CLR Runtime Version
        /// </summary>
        /// <returns>Device runtime version string</returns>
        Task<string> GetRuntimeVersion();

        /// <summary>
        /// Get the name of the installed boot image, if any
        /// </summary>
        /// <returns>Name of the boot image</returns>
        Task<string> GetBootImageName();

        /// <summary>
        /// The the time the boot image was compiled, in UTC
        /// </summary>
        /// <returns>Image build time in UTC</returns>
        Task<DateTime> GetImageBuildDate();

        /// <summary>
        /// Is an image installed on the device?
        /// </summary>
        /// <returns>TRUE if device has a valid boot image installed</returns>
        Task<bool> IsImageInstalled();

        /// <summary>
        /// Is the image source installed on the device?
        /// </summary>
        /// <returns>TRUE if the source for the boot image is installed on the device</returns>
        Task<bool> IsSourceInstalled();

        /// <summary>
        /// Get information on device memory usage
        /// </summary>
        /// <returns>Memory max and free sizes</returns>
        Task<MPiecMemory> GetMemoryState();

        /// <summary>
        /// Get the current program state
        /// </summary>
        /// <returns>Current program state on the device</returns>
        Task<ProgramState> GetProgramState();

        /// <summary>
        /// Get the priority level the device supports
        /// </summary>
        /// <returns>Supported threading priority level</returns>
        Task<UInt32> GetThreadingPriorityLevel();

        #endregion

        #region On-Device Variable Subscriptions for bulk/repeated reads

        /// <summary>
        /// Create a subscription group of global variables on the device. A subscirption group can be used to read or write variables in bulk
        /// </summary>
        /// <param name="VariableNames">List of variable names to subscribe to</param>
        /// <param name="SubscriptionID">Unique ID for subscription; one is created if nothing is provided</param>
        /// <returns>SubscriptionID on success.  On any error creating the subscription, if an InvalidSubscription event handler has been set, it will be invoked and string.Empty will
        /// be returned.  If no handler has been set, MPIECInvalidSubscriptionException will be thrown.</returns>
        Task<string> SubscribeToVariables(IEnumerable<MPiecVariable> Variables, string SubscriptionID = null);

        /// <summary>
        /// Create a subscription group of global variables on the device from a list of variable definition strings. 
        /// A subscirption group can be used to read or write variables in bulk. Variable Definition Strings have the format:
        /// 
        /// "VARNAME:VARTYPE" or "VARNAME:VARTYPE"
        /// 
        /// where:
        /// VARNAME: Name of variable on device
        /// VARTYPE: Variable type as defined in MotionWorks
        /// </summary>
        /// <param name="VariableDefinitions">List of variables to subscribe to as Variable Definition Strings</param>
        /// <param name="SubscriptionID">Unique ID for subscription; one is created if nothing is provided</param>
        /// <returns>SubscriptionID on success.  On any error creating the subscription, if an InvalidSubscription event handler has been set, it will be invoked and string.Empty will
        /// be returned.  If no handler has been set, MPIECInvalidSubscriptionException will be thrown.</returns>
        Task<string> SubscribeToGlobalVariables(IEnumerable<string> VariableDefinitions, string SubscriptionID = null);

        /// <summary>
        /// Create a subscription group of local variables on the device from a list of variable definition strings. 
        /// A subscirption group can be used to read or write variables in bulk. Variable Definition Strings have the format:
        /// 
        /// "VARNAME:VARTYPE" or "VARNAME:VARTYPE"
        /// 
        /// where:
        /// VARNAME: Name of variable on device
        /// VARTYPE: Variable type as defined in MotionWorks
        /// </summary>
        /// <param name="VariableDefinitions">List of variables to subscribe to as Variable Definition Strings</param>
        /// <param name="SubscriptionID">Unique ID for subscription; one is created if nothing is provided</param>
        /// <returns>SubscriptionID on success.  On any error creating the subscription, if an InvalidSubscription event handler has been set, it will be invoked and string.Empty will
        /// be returned.  If no handler has been set, MPIECInvalidSubscriptionException will be thrown.</returns>
        Task<string> SubscribeToLocalVariables(IEnumerable<string> VariableDefinitions, string SubscriptionID = null);

        /// <summary>
        /// Remove a subscription group of variables on the dvice.  
        /// On any error removing the subscription, if an InvalidSubscription event handler has been set, it will be invoked and string.Empty will
        /// be returned.  If no handler has been set, MPIECInvalidSubscriptionException will be thrown
        /// </summary>
        /// <param name="SubscriptionID">Unique ID of subscription to remove</param>
        Task Unsubscribe(string SubscriptionID);

        /// <summary>
        /// Remove all subscription groups for variables on the device
        /// On any error removing a subscription, if an InvalidSubscription event handler has been set, it will be invoked and string.Empty will
        /// be returned.  If no handler has been set, MPIECInvalidSubscriptionException will be thrown
        /// </summary>
        Task UnsubscribeAll();

        /// <summary>
        /// Read values for all variables in a subscription from the device
        /// </summary>
        /// <param name="SubscriptionID">Unique ID of subscription to read</param>
        /// <returns>Dictionary with keys of variable names and values of dynamic variable types reflecting current variable values on the device</returns>
        Task<Dictionary<string, dynamic>> ReadSubscriptionValues(string SubscriptionID);

        /// <summary>
        /// Set up a subscription read event to trigger at a regular interval
        /// </summary>
        /// <param name="SubscriptionID">Unique ID of subscription to read</param>
        /// <param name="ReadInterval">Interval in ms for read to occur</param>
        /// <param name="timerHandler">Event handler called when new values have been read</param>
        /// <returns>Timer object to control the reading. Reading can be started and stopped by toggling the Enabled property on the timer object, and interval can be adjusted.
        /// Timer object should not be Disposed, but code should call StopSubscriptionReadTimer() when no longer needed.</returns>
        System.Timers.Timer StartSubscriptionReadTimer(string SubscriptionID, int ReadInterval, EventHandler<MPIECSubscriptionReadEventArgs> timerHandler);

        /// <summary>
        /// Stop a recurring subscription read.  This will Dispose the timer object, which should not be referenced after calling this function.
        /// </summary>
        /// <param name="readTimer">Timer object created by StartSubscriptionReadTimer</param>
        void StopSubscriptionReadTimer(System.Timers.Timer readTimer);

        /// <summary>
        /// Stop all recurring subscription reads
        /// </summary>
        void StopAllSubscriptionReadTimers();

        #endregion

        #region On-Device Variable one-time Reading

        /// <summary>
        /// Read one or more variables on the device
        /// </summary>
        /// <param name="Variables">Collection of variable instances to read</param>
        /// <returns>Dictionary with keys of variable names and values of dynamic variable types reflecting current variable values on the device</returns>
        Task<Dictionary<string, dynamic>> ReadVariables(IEnumerable<MPiecVariable> Variables);

        /// <summary>
        /// Read one or more global variables on the device
        /// </summary>
        /// <param name="VariableDefinitions">Variable names and values to write as variable definition strings. Variable Definition Strings have the format:
        /// 
        /// "VARNAME:VARTYPE"
        /// 
        /// where:
        /// VARNAME: Name of variable on device
        /// VARTYPE: Variable type as defined in MotionWorks
        /// <returns>Dictionary with keys of variable names and values of dynamic variable types reflecting current variable values on the device</returns>
        Task<Dictionary<string, dynamic>> ReadGlobalVariables(IEnumerable<string> VariableDefinitions);

        /// <summary>
        /// Read one or more local variables on the device
        /// </summary>
        /// <param name="VariableDefinitions">Variable names and values to write as variable definition strings. Variable Definition Strings have the format:
        /// 
        /// "VARNAME:VARTYPE"
        /// 
        /// where:
        /// VARNAME: Name of variable on device
        /// VARTYPE: Variable type as defined in MotionWorks
        /// <returns>Dictionary with keys of variable names and values of dynamic variable types reflecting current variable values on the device</returns>
        Task<Dictionary<string, dynamic>> ReadLocalVariables(IEnumerable<string> VariableDefinitions);

        #endregion

        #region On-Device Variable Writing

        /// <summary>
        /// Write to one or more variables on the device
        /// </summary>
        /// <param name="Variables">Collection of variable instances to write. Value in the instance will be written to the on-device variable.</param>
        /// <returns>TRUE if all values are being written to the device, FALSE on any error</returns>
        Task<bool> WriteVariables(IEnumerable<MPiecVariable> Variables);

        /// <summary>
        /// Write to one or more global variables on the device
        /// </summary>
        /// <param name="VariableDefinitions">Variable names and values to write as variable definition strings. Variable Definition Strings have the format:
        /// 
        /// "VARNAME:VARTYPE=VALUE"
        /// 
        /// where:
        /// VARNAME: Name of variable on device
        /// VARTYPE: Variable type as defined in MotionWorks
        /// VALUE: String representation of variable value</param>
        /// <returns>TRUE if all values are being written to the device, FALSE on any error</returns>
        Task<bool> WriteGlobalVariables(IEnumerable<string> VariableDefinitions);

        /// <summary>
        /// Write to one or more local variables on the device
        /// </summary>
        /// <param name="VariableDefinitions">Variable names and values to write as variable definition strings. Variable Definition Strings have the format:
        /// 
        /// "VARNAME:VARTYPE=VALUE"
        /// 
        /// where:
        /// VARNAME: Name of variable on device
        /// VARTYPE: Variable type as defined in MotionWorks
        /// VALUE: String representation of variable value</param>
        /// <returns>TRUE if all values are being written to the device, FALSE on any error</returns>
        Task<bool> WriteLocalVariables(IEnumerable<string> VariableDefinitions);

        /// <summary>
        /// Set a variable on the device to a TRUE or FALSE value
        /// (sets non-BOOL variable types as possible)
        /// </summary>
        /// <param name="Variable">Variable to set</param>
        /// <param name="isTrue">TRUE or FALSE value to set variable to</param>
        /// <returns>TRUE if the value was written to the device, FALSE otherwise</returns>
        Task<bool> SetVariable(MPiecVariable Variable, bool isTrue);

        #endregion

        #region PLC Control

        /// <summary>
        /// Activate the boot program (copy from Flash memory to RAM). Required on new upload and after reset
        /// before starting the program.
        /// </summary>
        /// <returns>TRUE on successful activation, FALSE otherwise</returns>
        Task<bool> Initialize();

        /// <summary>
        /// Warm start the application on the device
        /// </summary>
        /// <returns>TRUE on successful start, FALSE otherwise</returns>
        Task<bool> WarmStart();

        /// <summary>
        /// Cold start the application on the device
        /// </summary>
        /// <returns>TRUE on successful start, FALSE otherwise</returns>
        Task<bool> ColdStart();

        /// <summary>
        /// Hot start the application on the device
        /// </summary>
        /// <returns>TRUE on successful start, FALSE otherwise</returns>
        Task<bool> HotStart();

        /// <summary>
        /// Stop the PLC application on the motion controller.  Motion will stop.
        /// Other controller services (I/O drivers, Mechatrolink network, etc) will continue to operate
        /// </summary>
        /// <returns>TRUE on successful start, FALSE otherwise</returns>
        Task<bool> Stop();

        /// <summary>
        /// Reset the PLC
        /// </summary>
        /// <returns>TRUE on sucessful reset, FALSE otherwise</returns>
        Task<bool> ResetPLC();

        #endregion

        #region File I/O

        /// <summary>
        /// Read data from a file on the device
        /// </summary>
        /// <param name="fileName">Full path on device of file to read ("/flash/user/config/file.xml")</param>
        /// <returns>Data from file on success, null on any error</returns>
        Task<MemoryStream> GetFile(string fileName);

        /// <summary>
        /// Save data to a file on the device
        /// </summary>
        /// <param name="fileName">Full path on device of file to write ("/flash/user/config/file.xml")</param>
        /// <param name="fileData">Data to write to file</param>
        /// <returns>TRUE on successful file save, FALSE on any error</returns>
        Task<bool> PutFile(string fileName, MemoryStream fileData);

        /// <summary>
        /// Remove a file from the device
        /// </summary>
        /// <param name="fileName">Full path on device of file to delete ("/flash/user/config/file.xml")</param>
        /// <returns>TRUE on successful delete, FALSE on any error</returns>
        Task<bool> DeleteFile(string fileName);

        #endregion

    }
}
