﻿using MPiecFramework.Events;
using MPiecFramework.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    /// <summary>
    /// MotionWorks IEC variable type names
    /// </summary>
    public enum MotionWorksType
    {
        BOOL,
        BYTE,
        USINT,
        LREAL,
        REAL,
        DINT,
        SINT,
        INT,
        DWORD,
        TIME,
        UDINT,
        UINT,
        WORD
    }

    public static class MotionWorksTypeExtensions
    {
        /// <summary>
        /// Return the .NET native type corresponding to a MotionWorks IEC varibale type
        /// </summary>
        /// <param name="mwType">MotionWorks IEC variable type</param>
        /// <returns>Corresponding .NET type</returns>
        /// <remarks>Throws an MPIECUnknownVariableTypeException if unable to convert variable type</remarks>
        public static Type GetNativeType(this MotionWorksType mwType)
        {
            switch (mwType)
            {
                case MotionWorksType.BOOL:
                    return typeof(bool);
                case MotionWorksType.BYTE:
                    return typeof(byte);
                case MotionWorksType.USINT:
                    return typeof(byte);
                case MotionWorksType.LREAL:
                    return typeof(double);
                case MotionWorksType.REAL:
                    return typeof(float);
                case MotionWorksType.DINT:
                    return typeof(int);
                case MotionWorksType.SINT:
                    return typeof(sbyte);
                case MotionWorksType.INT:
                    return typeof(short);
                case MotionWorksType.DWORD:
                    return typeof(uint);
                case MotionWorksType.TIME:
                    return typeof(uint);
                case MotionWorksType.UDINT:
                    return typeof(uint);
                case MotionWorksType.UINT:
                    return typeof(ushort);
                case MotionWorksType.WORD:
                    return typeof(ushort);
            }

            throw new MPIECUnknownVariableTypeException(new MPIECUnknownVariableTypeEventArgs(mwType));
        }

        /// <summary>
        /// Convert an object (including a string) to the appropriate MotionWorks type
        /// </summary>
        /// <param name="mwType">MotionWorksType to convert to</param>
        /// <param name="Value">Value to convert</param>
        /// <returns>Converted value, if able.  Throws MPIECUnknownVariableTypeException or other exception on conversion errors.</returns>
        public static dynamic Parse(this MotionWorksType mwType, object Value)
        {
            switch (mwType)
            {
                case MotionWorksType.BOOL:
                    return Convert.ToBoolean(Value);
                case MotionWorksType.BYTE:
                case MotionWorksType.USINT:
                    return Convert.ToByte(Value);
                case MotionWorksType.LREAL:
                    return Convert.ToDouble(Value);
                case MotionWorksType.REAL:
                    return Convert.ToSingle(Value);
                case MotionWorksType.DINT:
                    return Convert.ToInt32(Value);
                case MotionWorksType.SINT:
                    return Convert.ToSByte(Value);
                case MotionWorksType.INT:
                    return Convert.ToInt16(Value);
                case MotionWorksType.DWORD:
                case MotionWorksType.TIME:
                case MotionWorksType.UDINT:
                    return Convert.ToUInt32(Value);
                case MotionWorksType.UINT:
                case MotionWorksType.WORD:
                    return Convert.ToUInt16(Value);
            }

            throw new MPIECUnknownVariableTypeException(new MPIECUnknownVariableTypeEventArgs(mwType));
        }
    }

}
