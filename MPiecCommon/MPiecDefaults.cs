﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public static class MPiecDefaults
    {
        /// <summary>
        /// Defalt IP Address of controller (if E-INIT switch is ON)
        /// </summary>
        public static string IPAddress = "192.168.1.1";

        /// <summary>
        /// Default TCP port number for controller
        /// </summary>
        public static UInt16 Port = 41100;

        /// <summary>
        /// Default communication timeout, in ms
        /// </summary>
        public static UInt16 Timeout = 2000;
    }
}
