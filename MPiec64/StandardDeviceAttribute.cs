﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public enum StandardDeviceAttribute
    {
        Manufacturer = 0,
        ProductName = 1,
        HardwareVersion = 3,
        FirmwareVersion = 4,
        EClrMaxApplicationDataSize = 5,
        EClrMaxApplicationRetainDataSize = 6,
        EClrMaxApplicationProgramSize = 7,
        EClrRuntimeVersion = 11, // 0x0000000B
        EClrThreadingPriorityLevel = 15, // 0x0000000F
        EClrFreeApplicationDataSize = 16384, // 0x00004000
        EClrFreeApplicationProgramSize = 16385, // 0x00004001
        SourceOnPlc = 16387, // 0x00004003
        EClrFreeRetainDataSize = 16389, // 0x00004005
        Identity = 16390, // 0x00004006
        ImageOnPlc = 16392, // 0x00004008
        EClrMetaCrc = 16395, // 0x0000400B
        EClrImageTime = 16397, // 0x0000400D
        EClrBootProjName = 16398, // 0x0000400E
        PlcState = 19993, // 0x00004E19
    }
}
