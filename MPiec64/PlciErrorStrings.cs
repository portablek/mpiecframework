﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    internal static class PlciErrorStrings
    {
        public const string PlciError_0x0 = "no error";
        public const string PlciError_0x1 = "Some function argument is wrong.";
        public const string PlciError_0x2 = "PLC error.";
        public const string PlciError_0x3 = "Overflow of comunication buffer.";
        public const string PlciError_0x100 = "Fatal error, no recovery possible.";
        public const string PlciError_0x101 = "Recoverable error.";
        public const string PlciError_0x102 = "Try to connect to invalid socket.";
        public const string PlciError_0x103 = "The socket is in use and can't be used to connect again.";
        public const string PlciError_0x104 = "Timeout occurred in the reader.";
        public const string PlciError_0x105 = "Timeout occurred in the writer.";
        public const string PlciError_0x106 = "Invalid response received.";
        public const string PlciError_0x107 = "Type ins invalid.";
        public const string PlciError_0x108 = "Type mismatch.";
        public const string PlciError_0x109 = "No memory left.";
        public const string PlciError_0x10A = "The array received is to big.";
        public const string PlciError_0x10B = "Remoting object used is not valid.";
        public const string PlciError_0x10C = "No connection possible.";
        public const string PlciError_0x10D = "Header is not readable.";
        public const string PlciError_0x10E = "Null pointer exception.";
        public const string PlciError_0x10F = "Service requested is not ready or not valid.";
        public const string PlciError_0x110 = "Unexpected exception.";
        public const string PlciError_0x111 = "Function is used for different service, parameters only exist in special service.";
        public const string PlciError_0x112 = "This error is not implemented.";
        public const string PlciError_0x113 = "File error occurred.";
        public const string PlciError_0x114 = "Both, local and global meta data is empty..";
        public const string StdComError_0x40000001 = "Version of the remoting protocol received does not fit to the active protocol on the PLC.";
        public const string StdComError_0x40000002 = "Service handle is not known by the PLC.";
        public const string StdComError_0x40000003 = "Method called is not valid.";
        public const string StdComError_0x40000004 = "The command called is not known on PLC.";
        public const string StdComError_0x40000005 = "The received stream is empty.";
        public const string StdComError_0x40000006 = "Checksum is corrupted.";
        public const string StdComError_0x20000001 = "No memory left on device for PLC.";
        public const string StdComError_0x20000002 = "Invalid handle received.";
        public const string StdComError_0x20000006 = "There is no program downloaded to the PLC.";
        public const string StdComError_0x20000005 = "Another master is active, the requested operation can not be executed.";
        public const string StdComError_0x20000007 = "Error while executing or trying to execute the program.";
        public const string StdComError_0x20000008 = "Tried to write to an readonly object.";
        public const string StdComError_0x20000009 = "The PLC is an in valid state to perform this operation.";
        public const string StdComError_0x40001001 = "Timeout during receive.";
        public const string StdComError_0x40001002 = "Failed to receive data.";
        public const string StdComError_0x40001003 = "Timeout during send.";
        public const string StdComError_0x40001004 = "Failed to send data.";
        public const string StdComError_0x40001005 = "0x40001005";
        public const string StdComError_0x1 = "0x01";
        public const string StdComError_0x2 = "0x02";
        public const string StdComError_0x3 = "Values could not be read.";
        public const string StdComError_0x4 = "Values could not be written.";
        public const string StdComError_0x5 = "Group of variables could not be created.";
        public const string StdComError_0x6 = "Can't access PLC data.";
        public const string FileError_0x0 = "No error, standard value.";
        public const string FileError_0x1 = "No space left on file device";
        public const string FileError_0x2 = "File is write protected.";
        public const string FileError_0x3 = "File trying to access is not found.";
        public const string FileError_0x4 = "No device to use for file methods.";
        public const string FileError_0x5 = "Opening file failed.";
        public const string FileError_0x6 = "Method used is not implemented in the PLC.";
        public const string FileError_0x7 = "Wrong handle used for method.";
        public const string FileError_0x8 = "Methods called in wrong order.";
        public const string FileError_0x9 = "With older PLCs it is only allowed to read one file at same time.";
        public const string NotImplemented = "This error code \"{0}\" is not implemented.";
        public const string ObjectDisposed = "The object instance \"{0}\" was disposed.";
        public const string ObjectNull = "The object instance \"{0}\" was null.";
        public const string PlcDisconnected = "The PLC was disconnected.";
        public const string ServiceNotSupported = "The service is not supported.";
        public const string TypeNotSupported = "Type \"{0}\" is not supported.";
        public const string ExpectedTypeError = "Wrong type \"{0}\" expected type is \"{1}\".";
        public const string NoValidData = "There is no valid data have been found.";
        public const string ValueNullException = "The parameter value was null!";
        public const string ArrayNullException = "The array element was null!";
        public const string ArrayElementNullException = "The array element number {0} was null!";

        public static string GetErrorInformation(ErrorSource source, int code)
        {
            FieldInfo field = typeof(PlciErrorStrings).GetField(string.Format("{0}_0x{1}", (object)source, (object)code.ToString("X")));
            return field != null ? field.GetValue((object)null) as string : (string)null;
        }
    }
}
