﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public enum EncodingType
    {
        Ansi = 1,
        Utf8 = 2,
        Utf16 = 3,
    }
}
