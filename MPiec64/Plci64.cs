﻿using LegacyWrapperClient.Client;
using LegacyWrapperClient.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    /// <summary>
    /// Reverse-engineered Pcli class from Phoenix Contact to be able to call lower-level 32-bit C DLLs from 64-bit .NET processes
    /// </summary>
    public class Plci64 : IDisposable
    {
        #region 32-bit DLL wrapper via LegacyWrapper

        IWrapperConfig configration = null;
        internal IPLC32Wrapper Plci = null;

        internal UInt32 PlcIntPtr { get; set; } = 0;
        internal object LockObject { get; set; }
        public IPAddress Address { get; private set; }
        public ushort Port { get; private set; }
        public ushort Timeout { get; private set; }
        public string ConectionString { get; private set; }

        public Plci64()
        {
            configration = WrapperConfigBuilder.Create()
                .TargetArchitecture(LegacyWrapperClient.Architecture.TargetArchitecture.X86)
                .Build();
            Plci = WrapperProxyFactory<IPLC32Wrapper>.GetInstance(configration);

            ConectionString = string.Empty;
            LockObject = new object();
        }

        #endregion

        #region Plci Methods

        public void Connect(string address, ushort port = (ushort)41100, ushort timeout = (ushort)2000)
        {
            lock (this.LockObject)
            {
                if (!(this.PlcIntPtr == 0))
                    return;
                this.PlcIntPtr = Plci.CPlciCreateInstance();
                if (string.IsNullOrEmpty(address))
                    return;
                this.Address = IPAddress.Parse(address);
                this.Port = port;
                this.Timeout = timeout;
                if (Plci.CPlciConnect(PlcIntPtr, Encoding.UTF8.GetBytes(address), this.Port, this.Timeout))
                    return;
                PlciException.ThrowSpecializedError(this.GetLastError());
            }
        }

        public void Connect(string address, string connectionString)
        {
            lock (this.LockObject)
            {
                if (!(this.PlcIntPtr == 0))
                    return;
                this.PlcIntPtr = Plci.CPlciCreateInstance();
                if (!string.IsNullOrEmpty(connectionString))
                    return;
                this.ConectionString = connectionString;
                if (Plci.CPlciConnectStr(PlcIntPtr, this.ConectionString))
                    return;
                PlciException.ThrowSpecializedError(this.GetLastError());
            }
        }

        public void Disconnect()
        {
            lock (this.LockObject)
            {
                if (!(this.PlcIntPtr != 0))
                    return;
                UInt32 plcIntPtr = this.PlcIntPtr;
                this.PlcIntPtr = 0;
                Plci.CPlciDisconnect(plcIntPtr);
                Plci.CPlciDisposeInstance(plcIntPtr);
            }
        }

        public T GetService<T>()
        {
            lock (this.LockObject)
            {
                object obj = (object)null;
                if (this.PlcIntPtr != 0)
                {
                    try
                    {
                        var types = Assembly.GetCallingAssembly().GetTypes();
                        obj = Activator.CreateInstance(Assembly.GetCallingAssembly().GetTypes().First(c => c.IsClass && c.GetInterfaces().Contains(typeof(T))));
                        if (obj != null)
                        {
                            if (obj is IService service)
                                service.InitService(Plci, this);
                            else
                                obj = (object)null;
                        }
                    }
                    catch
                    {
                        obj = (object)null;
                    }
                }
                return (T)obj;
            }
        }

        internal PlciExceptionData GetLastError()
        {
            PlciExceptionData lastError = new PlciExceptionData();
            Plci.CPlciGetLastError(this.PlcIntPtr, ref lastError);
            return lastError;
        }

        internal void ResetLastError()
        {
            Plci.CPlciResetLastError(this.PlcIntPtr);
        }

        internal void CheckAndThrowError()
        {
            if (this.GetLastError().Code == 0)
                return;
            PlciException.ThrowSpecializedError(this.GetLastError());
        }

        #endregion

        #region IDisposable Interface

        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    if (Plci != null)
                        Plci.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                if (this.PlcIntPtr != 0)
                    this.Disconnect();

                disposedValue = true;
            }
        }

        ~Plci64()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
