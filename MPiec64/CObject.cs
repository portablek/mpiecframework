﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public class CObject : IDisposable
    {
        public IPLC32Wrapper Plci { get; private set; } = null;
        private UInt32 objectIntPtr = 0;
        private bool tempIntPtr;
        private bool disposed;

        public CObject(IPLC32Wrapper wrapper)
        {
            this.Plci = wrapper;
            this.ObjectIntPtr = Plci.CObjectCreateInstance();
            this.tempIntPtr = false;
        }

        public CObject(IPLC32Wrapper wrapper, UInt32 cObjectPtr)
        {
            this.Plci = wrapper;
            this.ObjectIntPtr = cObjectPtr;
            this.tempIntPtr = true;
        }

        ~CObject()
        {
            this.Dispose(false);
        }

        public UInt32 ObjectIntPtr
        {
            get
            {
                return this.objectIntPtr;
            }
            set
            {
                if (this.ObjectIntPtr != 0 && !this.tempIntPtr)
                {
                    Plci.CObjectDisposeInstance(this.ObjectIntPtr);
                    this.objectIntPtr = value;
                }
                else
                    this.objectIntPtr = value;
            }
        }

        public void SetTempObjectIntPtr(UInt32 pointer)
        {
            this.ObjectIntPtr = pointer;
            this.tempIntPtr = true;
        }

        public bool IsElementary()
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CObject));
            return Plci.CObjectIsElementary(this.ObjectIntPtr);
        }

        public bool IsString()
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CObject));
            return Plci.CObjectIsString(this.ObjectIntPtr);
        }

        public bool IsValueType()
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CObject));
            return Plci.CObjectIsValueType(this.ObjectIntPtr);
        }

        public bool IsArray()
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CObject));
            return Plci.CObjectIsArray(this.ObjectIntPtr);
        }

        public bool IsElementaryArray()
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CObject));
            return Plci.CObjectIsElementaryArray(this.ObjectIntPtr);
        }

        public bool IsStringArray()
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CObject));
            return Plci.CObjectIsStringArray(this.ObjectIntPtr);
        }

        public bool IsStringPtrArray()
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CObject));
            return Plci.CObjectIsStringPtrArray(this.ObjectIntPtr);
        }

        public MetaElementType GetMetaElementType()
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CObject));
            return (MetaElementType)Plci.CObjectGetType(this.ObjectIntPtr);
        }

        public MetaElementType GetMetaElementArrayType()
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CObject));
            return (MetaElementType)Plci.CObjectGetArrayType(this.ObjectIntPtr);
        }

        public T GetValue<T>()
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CObject));
            object obj = (object)null;
            switch (typeof(T).Name)
            {
                case "Boolean":
                    obj = (object)Plci.CObjectGetValueBool(this.ObjectIntPtr);
                    break;
                case "Char":
                    obj = (object)Plci.CObjectGetValueChar(this.ObjectIntPtr);
                    break;
                case "Byte":
                    obj = (object)Plci.CObjectGetValueByte(this.ObjectIntPtr);
                    break;
                case "SByte":
                    obj = (object)Plci.CObjectGetValueInt8(this.ObjectIntPtr);
                    break;
                case "Int16":
                    obj = (object)Plci.CObjectGetValueInt16(this.ObjectIntPtr);
                    break;
                case "UInt16":
                    obj = (object)Plci.CObjectGetValueUInt16(this.ObjectIntPtr);
                    break;
                case "Int32":
                    obj = (object)Plci.CObjectGetValueInt32(this.ObjectIntPtr);
                    break;
                case "UInt32":
                    obj = (object)Plci.CObjectGetValueUInt32(this.ObjectIntPtr);
                    break;
                case "Int64":
                    long num1 = 0;
                    Plci.CObjectGetValueInt64(this.ObjectIntPtr, ref num1);
                    obj = (object)num1;
                    break;
                case "UInt64":
                    ulong num2 = 0;
                    Plci.CObjectGetValueUInt64(this.ObjectIntPtr, ref num2);
                    obj = (object)num2;
                    break;
                case "Single":
                    float num3 = 0.0f;
                    Plci.CObjectGetValueFloat(this.ObjectIntPtr, ref num3);
                    obj = (object)num3;
                    break;
                case "Double":
                    double num4 = 0.0;
                    Plci.CObjectGetValueDouble(this.ObjectIntPtr, ref num4);
                    obj = (object)num4;
                    break;
                case "String":
                    EncodingType encoding = this.GetEncoding();
                    string stringUni;
                    if (encoding == EncodingType.Utf16)
                    {
                        UInt32 ptr;
                        Plci.CObjectGetString(this.ObjectIntPtr, out stringUni);
                        //stringUni = Marshal.PtrToStringUni((IntPtr)ptr);
                    }
                    else
                    {
                        IntPtr ptr;
                        int length;
                        Plci.CObjectGetByteString(this.ObjectIntPtr, out ptr, out length);
                        stringUni = this.GetString(ptr, length, encoding);
                    }
                    obj = (object)this.FormatStringEnd(stringUni);
                    break;
                case nameof(CObject):
                    obj = (object)new CObject(Plci, ObjectIntPtr);
                    break;
            }
            return (T)Convert.ChangeType(obj, typeof(T), (IFormatProvider)NumberFormatInfo.InvariantInfo);
        }

        public EncodingType GetEncoding()
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CObject));
            return (EncodingType)Enum.Parse(typeof(EncodingType), Plci.CObjectGetEncoding(this.ObjectIntPtr).ToString((IFormatProvider)CultureInfo.InvariantCulture), false);
        }

        private string GetString(IntPtr ptr, int length, EncodingType encoding)
        {
            string str = string.Empty;
            if (ptr != IntPtr.Zero && length > 0)
            {
                byte[] numArray = new byte[length];
                Marshal.Copy((IntPtr)ptr, numArray, 0, length);
                str = encoding != EncodingType.Utf8 ? Encoding.UTF8.GetString(numArray, 0, length) : Encoding.UTF8.GetString(numArray, 0, length);
            }
            return str;
        }

        private string FormatStringEnd(string data)
        {
            return !string.IsNullOrEmpty(data) && data.Contains("\0") ? data.Substring(0, data.IndexOf(char.MinValue)) : data;
        }

        public void SetValue(object value)
        {
            this.SetValue(value, EncodingType.Utf8);
        }

        public void SetValue(object value, EncodingType encoding)
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CObject));
            if (value == null)
                throw new RecoverablePlciException("The parameter value was null!");
            switch (value.GetType().Name)
            {
                case "Boolean":
                    Plci.CObjectSetValueBool(this.ObjectIntPtr, (bool)value);
                    break;
                case "Char":
                    Plci.CObjectSetValueChar(this.ObjectIntPtr, (char)value);
                    break;
                case "Byte":
                    Plci.CObjectSetValueByte(this.ObjectIntPtr, (byte)value);
                    break;
                case "SByte":
                    Plci.CObjectSetValueInt8(this.ObjectIntPtr, (sbyte)value);
                    break;
                case "Int16":
                    Plci.CObjectSetValueInt16(this.ObjectIntPtr, (short)value);
                    break;
                case "UInt16":
                    Plci.CObjectSetValueUInt16(this.ObjectIntPtr, (ushort)value);
                    break;
                case "Int32":
                    Plci.CObjectSetValueInt32(this.ObjectIntPtr, (int)value);
                    break;
                case "UInt32":
                    Plci.CObjectSetValueUInt32(this.ObjectIntPtr, (uint)value);
                    break;
                case "Int64":
                    Plci.CObjectSetValueInt64(this.ObjectIntPtr, (long)value);
                    break;
                case "UInt64":
                    Plci.CObjectSetValueUInt64(this.ObjectIntPtr, (ulong)value);
                    break;
                case "Single":
                    Plci.CObjectSetValueFloat(this.ObjectIntPtr, (float)value);
                    break;
                case "Double":
                    Plci.CObjectSetValueDouble(this.ObjectIntPtr, (double)value);
                    break;
                case "String":
                    this.SetValue((string)value, encoding);
                    break;
                case "Object":
                    CObject cobject = value.ObjectToCObject(Plci);
                    if (cobject == null)
                        break;
                    this.SetTempObjectIntPtr(cobject.ObjectIntPtr);
                    break;
                default:
                    throw new PlciException(string.Format("Type \"{0}\" is not supported.", (object)value.GetType().Name));
            }
        }

        private void SetValue(string value, EncodingType encoding)
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CObject));
            if (encoding == EncodingType.Utf16)
                Plci.CObjectSetString(this.ObjectIntPtr, value);
            else if (encoding == EncodingType.Utf8)
            {
                byte[] bytes = Encoding.UTF8.GetBytes(value);
                Plci.CObjectSetByteString(this.ObjectIntPtr, bytes, bytes.Length, (int)encoding);
            }
            else
            {
                byte[] bytes = Encoding.UTF8.GetBytes(value);
                Plci.CObjectSetByteString(this.ObjectIntPtr, bytes, bytes.Length, (int)encoding);
            }
        }

        public int GetArrayLength()
        {
            return Plci.CObjectGetArrayLength(this.ObjectIntPtr);
        }

        public void SetArray(object data)
        {
            this.SetArray(data, EncodingType.Utf8);
        }

        internal void SetArray(object data, EncodingType encoding)
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CObject));
            if (data == null)
                throw new RecoverablePlciException("There is no valid data have been found.");
            string name;
            if (data.GetType().IsArray)
            {
                name = data.GetType().GetElementType().Name;
            }
            else
            {
                if (!(data is IList))
                    throw new PlciException(string.Format("Type \"{0}\" is not supported.", (object)data.GetType().Name));
                name = data.GetType().GetProperty("Item").PropertyType.Name;
            }
            switch (name)
            {
                case "Boolean":
                    Plci.CObjectSetArrayBool(this.ObjectIntPtr, data.GenericListToCArray<bool>(Plci).ObjectIntPtr);
                    break;
                case "Char":
                    Plci.CObjectSetArrayChar(this.ObjectIntPtr, data.GenericListToCArray<char>(Plci).ObjectIntPtr);
                    break;
                case "Byte":
                    Plci.CObjectSetArrayByte(this.ObjectIntPtr, data.GenericListToCArray<byte>(Plci).ObjectIntPtr);
                    break;
                case "SByte":
                    Plci.CObjectSetArrayInt8(this.ObjectIntPtr, data.GenericListToCArray<sbyte>(Plci).ObjectIntPtr);
                    break;
                case "Int16":
                    Plci.CObjectSetArrayInt16(this.ObjectIntPtr, data.GenericListToCArray<short>(Plci).ObjectIntPtr);
                    break;
                case "UInt16":
                    Plci.CObjectSetArrayUInt16(this.ObjectIntPtr, data.GenericListToCArray<ushort>(Plci).ObjectIntPtr);
                    break;
                case "Int32":
                    Plci.CObjectSetArrayInt32(this.ObjectIntPtr, data.GenericListToCArray<int>(Plci).ObjectIntPtr);
                    break;
                case "UInt32":
                    Plci.CObjectSetArrayUInt32(this.ObjectIntPtr, data.GenericListToCArray<uint>(Plci).ObjectIntPtr);
                    break;
                case "Int64":
                    Plci.CObjectSetArrayInt64(this.ObjectIntPtr, data.GenericListToCArray<long>(Plci).ObjectIntPtr);
                    break;
                case "UInt64":
                    Plci.CObjectSetArrayUInt64(this.ObjectIntPtr, data.GenericListToCArray<ulong>(Plci).ObjectIntPtr);
                    break;
                case "Single":
                    Plci.CObjectSetArrayFloat(this.ObjectIntPtr, data.GenericListToCArray<float>(Plci).ObjectIntPtr);
                    break;
                case "Double":
                    Plci.CObjectSetArrayDouble(this.ObjectIntPtr, data.GenericListToCArray<double>(Plci).ObjectIntPtr);
                    break;
                case "String":
                    Plci.CObjectSetArrayString(this.ObjectIntPtr, data.GenericListToCArray<string>(Plci).ObjectIntPtr);
                    break;
                case nameof(CObject):
                    Plci.CObjectSetArrayCObject(this.ObjectIntPtr, data.GenericListToCArray<CObject>(Plci).ObjectIntPtr);
                    break;
                default:
                    throw new PlciException(string.Format("Type \"{0}\" is not supported.", (object)name));
            }
        }

        public IList<T> GetArray<T>()
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CObject));
            UInt32 cArrayPtr;
            switch (typeof(T).Name)
            {
                case "Boolean":
                    cArrayPtr = Plci.CObjectGetArrayBool(this.ObjectIntPtr);
                    break;
                case "Byte":
                    cArrayPtr = Plci.CObjectGetArrayByte(this.ObjectIntPtr);
                    break;
                case "SByte":
                    cArrayPtr = Plci.CObjectGetArrayInt8(this.ObjectIntPtr);
                    break;
                case "Int16":
                    cArrayPtr = Plci.CObjectGetArrayInt16(this.ObjectIntPtr);
                    break;
                case "UInt16":
                    cArrayPtr = Plci.CObjectGetArrayUInt16(this.ObjectIntPtr);
                    break;
                case "Int32":
                    cArrayPtr = Plci.CObjectGetArrayInt32(this.ObjectIntPtr);
                    break;
                case "UInt32":
                    cArrayPtr = Plci.CObjectGetArrayUInt32(this.ObjectIntPtr);
                    break;
                case "Int64":
                    cArrayPtr = Plci.CObjectGetArrayInt64(this.ObjectIntPtr);
                    break;
                case "UInt64":
                    cArrayPtr = Plci.CObjectGetArrayUInt64(this.ObjectIntPtr);
                    break;
                case "Single":
                    cArrayPtr = Plci.CObjectGetArrayFloat(this.ObjectIntPtr);
                    break;
                case "Double":
                    cArrayPtr = Plci.CObjectGetArrayDouble(this.ObjectIntPtr);
                    break;
                case "String":
                    cArrayPtr = Plci.CObjectGetArrayString(this.ObjectIntPtr);
                    break;
                case nameof(CObject):
                    cArrayPtr = Plci.CObjectGetArrayCObject(this.ObjectIntPtr);
                    break;
                default:
                    throw new PlciException(string.Format("Type \"{0}\" is not supported.", (object)typeof(T).Name));
            }
            return new CArray<T>(cArrayPtr, true).CArrayToIList();
        }

        public override string ToString()
        {
            return this.GetMetaElementType().ToString();
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
                return;
            int num = disposing ? 1 : 0;
            this.ObjectIntPtr = 0;
            this.disposed = true;
        }
    }
}
