﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public class CString : IDisposable, IComparable
    {
        public IPLC32Wrapper Plci { get; private set; } = null;
        private bool tempIntPtr;
        private UInt32 objectIntPtr;
        private bool disposed;

        public CString(IPLC32Wrapper wrapper)
        {
            this.Plci = wrapper;
            this.objectIntPtr = Plci.CStringCreateInstance();
        }

        public CString(IPLC32Wrapper wrapper, string value) : this(wrapper)
        {
            this.SetString(value);
        }

        public CString(IPLC32Wrapper wrapper, string value, EncodingType encoding) : this(wrapper)
        {
            this.SetString(value, encoding);
        }

        public CString(IPLC32Wrapper wrapper, UInt32 value)
        {
            this.Plci = wrapper;
            this.objectIntPtr = value;
            this.tempIntPtr = true;
        }

        ~CString()
        {
            this.Dispose(false);
        }

        public UInt32 ObjectIntPtr
        {
            get
            {
                return this.objectIntPtr;
            }
            set
            {
                if (this.ObjectIntPtr != 0 && !this.tempIntPtr)
                {
                    Plci.CStringDisposeInstance(this.objectIntPtr);
                    this.objectIntPtr = value;
                }
                else
                    this.objectIntPtr = value;
            }
        }

        public string GetString()
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CString));
            UInt32 source;
            int length;
            if (!Plci.CStringGetValue(this.ObjectIntPtr, out source, out length))
                return (string)null;
            byte[] numArray = new byte[length];
            Marshal.Copy((IntPtr)source, numArray, 0, length);
            switch (this.GetEncodingCString())
            {
                case EncodingType.Ansi:
                    return Encoding.UTF8.GetString(numArray, 0, length);
                case EncodingType.Utf8:
                    return Encoding.UTF8.GetString(numArray, 0, length);
                case EncodingType.Utf16:
                    return Encoding.Unicode.GetString(numArray, 0, length);
                default:
                    return string.Empty;
            }
        }

        public EncodingType GetEncodingCString()
        {
            this.ObjectIntPtr.ThrowExceptionIfPointerZero(nameof(CString));
            return (EncodingType)Enum.Parse(typeof(EncodingType), Plci.CStringGetEncoding(this.ObjectIntPtr).ToString((IFormatProvider)CultureInfo.InvariantCulture), false);
        }

        public bool SetString(string value)
        {
            return this.SetString(value, EncodingType.Ansi);
        }

        public bool SetString(string value, EncodingType encodingType)
        {
            if (this.ObjectIntPtr != 0)
            {
                switch (encodingType)
                {
                    case EncodingType.Ansi:
                        byte[] bytes1 = Encoding.UTF8.GetBytes(value);
                        return Plci.CStringSetValue(this.ObjectIntPtr, bytes1, bytes1.Length);
                    case EncodingType.Utf8:
                        byte[] bytes2 = Encoding.UTF8.GetBytes(value);
                        return Plci.CStringSetValue(this.ObjectIntPtr, bytes2, bytes2.Length);
                    case EncodingType.Utf16:
                        byte[] bytes3 = Encoding.Unicode.GetBytes(value);
                        return Plci.CStringSetValue(this.ObjectIntPtr, bytes3, bytes3.Length);
                }
            }
            return false;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
                return;
            int num = disposing ? 1 : 0;
            this.ObjectIntPtr = 0;
            this.disposed = true;
        }

        public int CompareTo(object obj)
        {
            return !(obj is CString cstring) ? 1 : string.Compare(this.GetString(), cstring.GetString(), StringComparison.Ordinal);
        }

        public override bool Equals(object obj)
        {
            return this.CompareTo(obj) == 0;
        }

        public override int GetHashCode()
        {
            return this.GetString().GetHashCode();
        }

        public override string ToString()
        {
            return this.GetString();
        }
    }
}