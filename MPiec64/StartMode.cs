﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public enum StartMode
    {
        ColdStart = 1,
        WarmStart = 2,
        HotStart = 3,
    }
}
