﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public class RecoverableFileException : RecoverablePlciException
    {
        private string localMessage;

        internal RecoverableFileException(string message)
          : base(message)
        {
            this.localMessage = base.Message;
        }

        internal RecoverableFileException(int code, int addCode)
          : base(code, addCode)
        {
            this.localMessage = base.Message;
        }

        internal RecoverableFileException(PlciExceptionData data)
          : base(data)
        {
            this.localMessage = base.Message;
            if (data.AddCode <= 0)
                return;
            string errorInformation = PlciErrorStrings.GetErrorInformation(ErrorSource.FileError, data.AddCode);
            if (string.IsNullOrEmpty(errorInformation))
                return;
            this.localMessage = errorInformation;
        }

        internal RecoverableFileException(string message, System.Exception innerException)
          : base(message, innerException)
        {
            this.localMessage = base.Message;
        }

        public override string Message
        {
            get
            {
                return this.localMessage;
            }
        }
    }
}
