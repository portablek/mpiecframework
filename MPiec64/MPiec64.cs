﻿using MPiecFramework.Events;
using MPiecFramework.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    /// <summary>
    /// 64-bit (AMD64) PLCi-based interface class to all Yaskawa MPiec eCLR Controllers 
    /// (except for  MP2300Siec, as mentioned in AN.MPIEC.29.pdf)
    /// </summary>
    public class MPiec64 : IMPiec
    {
        #region Defaults and other statics

        /// <summary>
        /// Defalt IP Address of controller (if E-INIT switch is ON)
        /// </summary>
        public static string DefaultIPAddress = "192.168.1.1";

        /// <summary>
        /// Default TCP port number for controller
        /// </summary>
        public static UInt16 DefaultPort = 41100;

        /// <summary>
        /// Default communication timeout, in ms
        /// </summary>
        public static UInt16 DefaultTimeout = 2000;

        #endregion

        #region Internal variables

        private Plci64 Controller = new Plci64();

        private IDataAccessService DataAccess = null;
        private IDeviceAttributeService DeviceAttributes = null;
        private IPlcControlService ControlService = null;
        private IFileService FileService = null;

        private Dictionary<string, Subscription> Subscriptions = new Dictionary<string, Subscription>();
        private Dictionary<string, IEnumerable<MPiecVariable>> SubscriptionVariables = new Dictionary<string, IEnumerable<MPiecVariable>>();

        private List<System.Timers.Timer> ReadTimers = new List<System.Timers.Timer>();

        #endregion

        #region Public Properties

        /// <summary>
        /// Has a connection successfully been made to this controller? (i.e., is the IPAddress and other parameters correct?)
        /// </summary>
        public bool HasConnected { get { return !String.IsNullOrEmpty(IPAddress); } }

        /// <summary>
        /// IP Address of currently-connected controller, null if not conected
        /// </summary>
        public string IPAddress { get; private set; } = null;

        /// <summary>
        /// TCP port number used to communicate with controller
        /// </summary>
        public UInt16 Port { get; private set; } = DefaultPort;

        /// <summary>
        /// Controller communication timeout value, is ms
        /// </summary>
        public UInt16 Timeout { get; set; } = DefaultTimeout;

        #endregion

        #region Error and Event Handling

        /// <summary>
        /// General error encountered attempting to communicate with controller (ethernet timeout/reachability issue, etc)
        /// </summary>
        public event EventHandler<MPIECCommunicationErrorEventArgs> CommunicationError;
        protected virtual void OnCommunicationError(MPIECCommunicationErrorEventArgs e)
        {
            if (CommunicationError != null)
                CommunicationError.Invoke(this, e);
            else
                throw new MPIECCommunicationException(e);
        }

        /// <summary>
        /// Setting name not found when attempting to read or write a device setting
        /// </summary>
        public event EventHandler<MPIECSettingNotFoundEventArgs> SettingNotFound;
        protected virtual void OnSettingNotFound(MPIECSettingNotFoundEventArgs e)
        {
            if (SettingNotFound != null)
                SettingNotFound.Invoke(this, e);
            else
                throw new MPIECSettingNotFoundException(e);
        }

        /// <summary>
        /// Invalid or unknown subscription identifier specified when subscribing to or reading values on device
        /// </summary>
        public event EventHandler<MPIECInvalidSubscriptionEventArgs> InvalidSubscription;
        protected virtual void OnInvalidSubscription(MPIECInvalidSubscriptionEventArgs e)
        {
            if (InvalidSubscription != null)
                InvalidSubscription.Invoke(this, e);
            else
                throw new MPIECInvalidSubscriptionException(e);
        }

        /// <summary>
        /// Error encountered attempting to read values to variables on device
        /// </summary>
        public event EventHandler<MPIECVariableReadErrorEventArgs> ErrorReadingVariables;
        protected virtual void OnErrorReadingVariables(MPIECVariableReadErrorEventArgs e)
        {
            if (ErrorReadingVariables != null)
                ErrorReadingVariables.Invoke(this, e);
            else
                throw new MPIECVariableReadException(e);
        }

        /// <summary>
        /// Error encountered attempting to write values to variables on device
        /// </summary>
        public event EventHandler<MPIECVariableWriteErrorEventArgs> ErrorWritingVariables;
        protected virtual void OnErrorWritingVariables(MPIECVariableWriteErrorEventArgs e)
        {
            if (ErrorWritingVariables != null)
                ErrorWritingVariables.Invoke(this, e);
            else
                throw new MPIECVariableWriteException(e);
        }

        /// <summary>
        /// Error attempting to control the PLC on the motion controller
        /// </summary>
        public event EventHandler<MPIECControlErrorEventArgs> PLCControlError;
        protected virtual void OnControlError(MPIECControlErrorEventArgs e)
        {
            if (PLCControlError != null)
                PLCControlError.Invoke(this, e);
            else
                throw new MPIECControlException(e);
        }

        /// <summary>
        /// Error attempting to read a file on the device
        /// </summary>
        public event EventHandler<MPIECFileEventArgs> FileReadError;
        protected virtual void OnFileReadError(MPIECFileEventArgs e)
        {
            if (FileReadError != null)
                FileReadError.Invoke(this, e);
            else
                throw new MPIECFileAccessException(e);
        }

        /// <summary>
        /// Error attempting to write a file on the device
        /// </summary>
        public event EventHandler<MPIECFileEventArgs> FileWriteError;
        protected virtual void OnFileWriteError(MPIECFileEventArgs e)
        {
            if (FileWriteError != null)
                FileWriteError.Invoke(this, e);
            else
                throw new MPIECFileAccessException(e);
        }

        #endregion

        #region Lifecycle

        /// <summary>
        /// Attempt to connect to a MPiec controller at a specified IP address
        /// </summary>
        /// <param name="IPAddress">IPv4 dot-notation IP address of controller ("192.168.1.1", etc)</param>
        /// <param name="Timeout">If non-zero, set the communication timeout value to this number of milliseconds</param>
        /// <param name="Port">If non-zero, connect to this TCP port number</param>
        /// <returns>TRUE if controller found and connected, FALSE if unable to connect</returns>
        public async Task<bool> Connect(string IPAddress, UInt16 Timeout = 0, UInt16 Port = 0)
        {
            if (Controller == null)
                Controller = new Plci64();

            await Disconnect();
            return await Task.Run(() =>
            {
                if (Timeout > 0)
                    this.Timeout = Timeout;
                if (Port > 0)
                    this.Port = Port;

                try
                {
                    Controller.Connect(IPAddress, this.Port, this.Timeout);
                    DataAccess = Controller.GetService<IDataAccessService>();
                    DeviceAttributes = Controller.GetService<IDeviceAttributeService>();
                    ControlService = Controller.GetService<IPlcControlService>();
                    FileService = Controller.GetService<IFileService>();
                    this.IPAddress = IPAddress;
                    return true;
                }
                catch (FatalPlciException ex)
                {
                    OnCommunicationError(new MPIECCommunicationErrorEventArgs(ex));
                }
                catch (BadAllocPlciException)
                {
                    throw new InsufficientMemoryException();
                }
                return false;
            });
        }

        /// <summary>
        /// Logically disconnect from the current controller, resetting all state to defaults
        /// </summary>
        /// <returns></returns>
        public async Task Disconnect()
        {
            await Task.Run(async () =>
            {
                StopAllSubscriptionReadTimers();
                await this.UnsubscribeAll();

                if (DataAccess != null)
                    DataAccess.Dispose();
                if (DeviceAttributes != null)
                    DeviceAttributes.Dispose();
                if (ControlService != null)
                    ControlService.Dispose();
                if (FileService != null)
                    FileService.Dispose();

                DataAccess = null;
                DeviceAttributes = null;
                ControlService = null;

                if (Controller != null)
                    Controller.Disconnect();

                IPAddress = null;
                Port = DefaultPort;
                Timeout = DefaultTimeout;
            });
        }

        #region IDisposable Interface

        private bool isDisposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (isDisposed)
                return;

            if (disposing)
            {
                // Free Managed Resources Here
                _ = Disconnect();
            }

            // Free native (un-managed) resources here

            isDisposed = true;
        }

        ~MPiec64()
        {
            Dispose(false);
        }

        #endregion

        #endregion

        #region Device Attribute Helper Functions

        /// <summary>
        /// Helper function to get a device attribute with error handling
        /// </summary>
        private bool GetAttribute<T>(StandardDeviceAttribute Attribute, ref T Value)
        {
            if (!HasConnected)
                return false;
            try
            {
                Value = DeviceAttributes.GetAttribute<T>(Attribute);
                return true;
            }
            catch (RecoverablePlciException ex)
            {
                // Usually this indicates the specified attribute was not found on the device
                OnSettingNotFound(new MPIECSettingNotFoundEventArgs(Attribute.ToString(), ex));
                return false;
            }
            catch (FatalPlciException ex)
            {
                OnCommunicationError(new MPIECCommunicationErrorEventArgs(ex));
            }
            catch (BadAllocPlciException)
            {
                throw new InsufficientMemoryException();
            }
            return false;
        }


        /// <summary>
        /// Helper method to return a device attribute as a string
        /// </summary>
        private async Task<string> GetAttributeString(StandardDeviceAttribute Attribute)
        {
            return await Task.Run(() =>
            {
                string val = string.Empty;
                if (GetAttribute<string>(Attribute, ref val))
                    return val;
                else
                    return string.Empty;
            });
        }

        /// <summary>
        /// Helper function to return a device attribute as a bool
        /// </summary>
        private async Task<bool> GetAttributeBool(StandardDeviceAttribute Attribute)
        {
            return await Task.Run(() =>
            {
                bool val = false;
                if (GetAttribute<bool>(Attribute, ref val))
                    return val;
                else
                    return false;
            });
        }

        /// <summary>
        /// Helper function to retun a device attribute as a UInt32
        /// </summary>
        private async Task<UInt32> GetAttributeUInt32(StandardDeviceAttribute Attribute)
        {
            return await Task.Run(() =>
            {
                UInt32 val = 0;
                if (GetAttribute<UInt32>(Attribute, ref val))
                    return val;
                else
                    return (UInt32)0;
            });
        }

        /// <summary>
        /// Helper function to retun a device attribute as an Int32
        /// </summary>
        private async Task<Int32> GetAttributeInt32(StandardDeviceAttribute Attribute)
        {
            return await Task.Run(() =>
            {
                Int32 val = 0;
                if (GetAttribute<Int32>(Attribute, ref val))
                    return val;
                else
                    return (Int32)0;
            });
        }

        /// <summary>
        /// Helper function to return a device attribute as a .NET DateTime value
        /// </summary>
        private async Task<DateTime> GetAttributeDateTime(StandardDeviceAttribute Attribute)
        {
            UInt32 Timestamp = await GetAttributeUInt32(Attribute);
            DateTime BaseEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return BaseEpoch.AddSeconds(Timestamp);
        }

        #endregion

        #region Device Attribute Getters

        /// <summary>
        /// Get the specific manufacturer of the controller
        /// </summary>
        /// <returns>Manufacuter company name ("Yaskawa Electric America, Inc.", etc)</returns>
        public async Task<string> GetManufacturer()
        {
            return await GetAttributeString(StandardDeviceAttribute.Manufacturer);
        }

        /// <summary>
        /// Get the specific controller model name
        /// </summary>
        /// <returns>Controller model ("MP3300iec", etc)</returns>
        public async Task<string> GetProductName()
        {
            return await GetAttributeString(StandardDeviceAttribute.ProductName);
        }

        /// <summary>
        /// Get a unique identity string for the connected device
        /// </summary>
        /// <returns>Unique identity string embedded in the device hardware</returns>
        public async Task<string> GetDeviceIdentity()
        {
            return await GetAttributeString(StandardDeviceAttribute.Identity);
        }

        /// <summary>
        /// Return a string representing the device hardware version
        /// </summary>
        /// <returns>Device hardware version string</returns>
        public async Task<string> GetHardwareVersion()
        {
            return await GetAttributeString(StandardDeviceAttribute.HardwareVersion);
        }

        /// <summary>
        /// Return a string representing the device firmware version
        /// </summary>
        /// <returns>Device firmware revision string</returns>
        public async Task<string> GetFirmwareVersion()
        {
            return await GetAttributeString(StandardDeviceAttribute.FirmwareVersion);
        }

        /// <summary>
        /// Return a string representing the device CLR Runtime Version
        /// </summary>
        /// <returns>Device runtime version string</returns>
        public async Task<string> GetRuntimeVersion()
        {
            return await GetAttributeString(StandardDeviceAttribute.EClrRuntimeVersion);
        }

        /// <summary>
        /// Get the name of the installed boot image, if any
        /// </summary>
        /// <returns>Name of the boot image</returns>
        public async Task<string> GetBootImageName()
        {
            return await GetAttributeString(StandardDeviceAttribute.EClrBootProjName);
        }

        /// <summary>
        /// The the time the boot image was compiled, in UTC
        /// </summary>
        /// <returns>Image build time in UTC</returns>
        public async Task<DateTime> GetImageBuildDate()
        {
            return await GetAttributeDateTime(StandardDeviceAttribute.EClrImageTime);
        }

        /// <summary>
        /// Is an image installed on the device?
        /// </summary>
        /// <returns>TRUE if device has a valid boot image installed</returns>
        public async Task<bool> IsImageInstalled()
        {
            return await GetAttributeBool(StandardDeviceAttribute.ImageOnPlc);
        }

        /// <summary>
        /// Is the image source installed on the device?
        /// </summary>
        /// <returns>TRUE if the source for the boot image is installed on the device</returns>
        public async Task<bool> IsSourceInstalled()
        {
            return await GetAttributeBool(StandardDeviceAttribute.SourceOnPlc);
        }

        /// <summary>
        /// Get information on device memory usage
        /// </summary>
        /// <returns>Memory max and free sizes</returns>
        public async Task<MPiecMemory> GetMemoryState()
        {
            return await Task.Run(async () =>
            {
                MPiecMemory Memory = new MPiecMemory();

                Memory.ApplicationFree = Convert.ToUInt32(await GetAttributeInt32(StandardDeviceAttribute.EClrFreeApplicationProgramSize));
                Memory.ApplicationSize = await GetAttributeUInt32(StandardDeviceAttribute.EClrMaxApplicationProgramSize);

                Memory.DataFree = Convert.ToUInt32(await GetAttributeInt32(StandardDeviceAttribute.EClrFreeApplicationDataSize));
                Memory.DataSize = await GetAttributeUInt32(StandardDeviceAttribute.EClrMaxApplicationDataSize);

                Memory.RetainFree = Convert.ToUInt32(await GetAttributeInt32(StandardDeviceAttribute.EClrFreeRetainDataSize));
                Memory.RetainSize = await GetAttributeUInt32(StandardDeviceAttribute.EClrMaxApplicationRetainDataSize);

                return Memory;
            });
        }

        /// <summary>
        /// Get the current program state
        /// </summary>
        /// <returns>Current program state on the device</returns>
        public async Task<ProgramState> GetProgramState()
        {
            UInt32 StateVal = await GetAttributeUInt32(StandardDeviceAttribute.PlcState);
            return (ProgramState)StateVal;
        }

        /// <summary>
        /// Get the priority level the device supports
        /// </summary>
        /// <returns>Supported threading priority level</returns>
        public async Task<UInt32> GetThreadingPriorityLevel()
        {
            return await GetAttributeUInt32(StandardDeviceAttribute.EClrThreadingPriorityLevel);
        }

        #endregion

        #region On-Device Variable Subscriptions for bulk/repeated reads

        /// <summary>
        /// Create a subscription group of global variables on the device. A subscirption group can be used to read or write variables in bulk
        /// </summary>
        /// <param name="VariableNames">List of variable names to subscribe to</param>
        /// <param name="SubscriptionID">Unique ID for subscription; one is created if nothing is provided</param>
        /// <returns>SubscriptionID on success.  On any error creating the subscription, if an InvalidSubscription event handler has been set, it will be invoked and string.Empty will
        /// be returned.  If no handler has been set, MPIECInvalidSubscriptionException will be thrown.</returns>
        public async Task<string> SubscribeToVariables(IEnumerable<MPiecVariable> Variables, string SubscriptionID = null)
        {
            if (string.IsNullOrEmpty(SubscriptionID))
                SubscriptionID = Guid.NewGuid().ToString();
            else if (Subscriptions.Keys.Contains(SubscriptionID))
            {
                OnInvalidSubscription(new MPIECInvalidSubscriptionEventArgs(SubscriptionID, new Exception("Subscription ID already exists; can not recreate without unsubscripting existing subscription.")));
                return string.Empty;
            }

            return await Task.Run(() =>
            {
                try
                {
                    List<string> VarNames = new List<string>(Variables.Count());
                    foreach (MPiecVariable Variable in Variables)
                        VarNames.Add(Variable.DeviceName);

                    Subscription Handle = DataAccess.CreateSubscription(VarNames);
                    Subscriptions.Add(SubscriptionID, Handle);
                    SubscriptionVariables.Add(SubscriptionID, Variables);
                    return SubscriptionID;
                }
                catch (Exception ex)
                {
                    OnInvalidSubscription(new MPIECInvalidSubscriptionEventArgs(SubscriptionID, ex));
                    return string.Empty;
                }
            });
        }

        /// <summary>
        /// Create a subscription group of global variables on the device from a list of variable definition strings. 
        /// A subscirption group can be used to read or write variables in bulk. Variable Definition Strings have the format:
        /// 
        /// "VARNAME:VARTYPE" or "VARNAME:VARTYPE"
        /// 
        /// where:
        /// VARNAME: Name of variable on device
        /// VARTYPE: Variable type as defined in MotionWorks
        /// </summary>
        /// <param name="VariableDefinitions">List of variables to subscribe to as Variable Definition Strings</param>
        /// <param name="SubscriptionID">Unique ID for subscription; one is created if nothing is provided</param>
        /// <returns>SubscriptionID on success.  On any error creating the subscription, if an InvalidSubscription event handler has been set, it will be invoked and string.Empty will
        /// be returned.  If no handler has been set, MPIECInvalidSubscriptionException will be thrown.</returns>
        public async Task<string> SubscribeToGlobalVariables(IEnumerable<string> VariableDefinitions, string SubscriptionID = null)
        {
            List<MPiecVariable> Variables = new List<MPiecVariable>();
            foreach (string Definition in VariableDefinitions)
                Variables.Add(new MPiecVariable(Definition));
            return await SubscribeToVariables(Variables, SubscriptionID);
        }

        /// <summary>
        /// Create a subscription group of local variables on the device from a list of variable definition strings. 
        /// A subscirption group can be used to read or write variables in bulk. Variable Definition Strings have the format:
        /// 
        /// "VARNAME:VARTYPE" or "VARNAME:VARTYPE"
        /// 
        /// where:
        /// VARNAME: Name of variable on device
        /// VARTYPE: Variable type as defined in MotionWorks
        /// </summary>
        /// <param name="VariableDefinitions">List of variables to subscribe to as Variable Definition Strings</param>
        /// <param name="SubscriptionID">Unique ID for subscription; one is created if nothing is provided</param>
        /// <returns>SubscriptionID on success.  On any error creating the subscription, if an InvalidSubscription event handler has been set, it will be invoked and string.Empty will
        /// be returned.  If no handler has been set, MPIECInvalidSubscriptionException will be thrown.</returns>
        public async Task<string> SubscribeToLocalVariables(IEnumerable<string> VariableDefinitions, string SubscriptionID = null)
        {
            List<MPiecVariable> Variables = new List<MPiecVariable>();
            foreach (string Definition in VariableDefinitions)
                Variables.Add(new MPiecVariable(Definition, MPiecVariable.Instance));
            return await SubscribeToVariables(Variables, SubscriptionID);
        }

        /// <summary>
        /// Remove a subscription group of variables on the dvice.  
        /// On any error removing the subscription, if an InvalidSubscription event handler has been set, it will be invoked and string.Empty will
        /// be returned.  If no handler has been set, MPIECInvalidSubscriptionException will be thrown
        /// </summary>
        /// <param name="SubscriptionID">Unique ID of subscription to remove</param>
        public async Task Unsubscribe(string SubscriptionID)
        {
            await Task.Run(() =>
            {
                try
                {
                    Subscription Handle = Subscriptions[SubscriptionID];
                    DataAccess.DestroySubscription(Handle);
                    Subscriptions.Remove(SubscriptionID);
                }
                catch (Exception ex)
                {
                    OnInvalidSubscription(new MPIECInvalidSubscriptionEventArgs(SubscriptionID, ex));
                }
            });
        }

        /// <summary>
        /// Remove all subscription groups for variables on the device
        /// On any error removing a subscription, if an InvalidSubscription event handler has been set, it will be invoked and string.Empty will
        /// be returned.  If no handler has been set, MPIECInvalidSubscriptionException will be thrown
        /// </summary>
        public async Task UnsubscribeAll()
        {
            List<string> SubIDs = new List<string>(Subscriptions.Keys);
            foreach (string SubscriptionID in SubIDs)
                await Unsubscribe(SubscriptionID);
        }

        /// <summary>
        /// Read values for all variables in a subscription from the device
        /// </summary>
        /// <param name="SubscriptionID">Unique ID of subscription to read</param>
        /// <returns>Dictionary with keys of variable names and values of dynamic variable types reflecting current variable values on the device</returns>
        public async Task<Dictionary<string, dynamic>> ReadSubscriptionValues(string SubscriptionID)
        {
            Dictionary<string, dynamic> Values = new Dictionary<string, dynamic>();
            if (!Subscriptions.ContainsKey(SubscriptionID))
                OnInvalidSubscription(new MPIECInvalidSubscriptionEventArgs(SubscriptionID, new Exception("Unknown subscription ID: " + SubscriptionID)));
            else
            {
                Subscription Handle = Subscriptions[SubscriptionID];
                IEnumerable<MPiecVariable> Variables = SubscriptionVariables[SubscriptionID];
                await Task.Run(() =>
                {
                    IList<object> varValues = DataAccess.GetResponse(Handle);
                    if (varValues.Count != Variables.Count())
                        OnInvalidSubscription(new MPIECInvalidSubscriptionEventArgs(SubscriptionID, new Exception(varValues.Count + " varaibles read (expected " + Variables.Count() + ")")));
                    int index = 0;
                    foreach (MPiecVariable Variable in Variables)
                        Values.Add(Variable.Name, varValues[index++]);
                });
            }
            return Values;
        }

        /// <summary>
        /// Set up a subscription read event to trigger at a regular interval
        /// </summary>
        /// <param name="SubscriptionID">Unique ID of subscription to read</param>
        /// <param name="ReadInterval">Interval in ms for read to occur</param>
        /// <param name="timerHandler">Event handler called when new values have been read</param>
        /// <returns>Timer object to control the reading. Reading can be started and stopped by toggling the Enabled property on the timer object, and interval can be adjusted.
        /// Timer object should not be Disposed, but code should call StopSubscriptionReadTimer() when no longer needed.</returns>
        public System.Timers.Timer StartSubscriptionReadTimer(string SubscriptionID, int ReadInterval, EventHandler<MPIECSubscriptionReadEventArgs> timerHandler)
        {
            System.Timers.Timer readTimer = new System.Timers.Timer((double)ReadInterval);
            readTimer.Elapsed += (object sender, System.Timers.ElapsedEventArgs e) =>
            {
                timerHandler.Invoke(this, new MPIECSubscriptionReadEventArgs(ReadSubscriptionValues(SubscriptionID).Result));
            };
            readTimer.AutoReset = true;
            readTimer.Enabled = true;
            ReadTimers.Add(readTimer);

            return readTimer;
        }

        /// <summary>
        /// Stop a recurring subscription read.  This will Dispose the timer object, which should not be referenced after calling this function.
        /// </summary>
        /// <param name="readTimer">Timer object created by StartSubscriptionReadTimer</param>
        public void StopSubscriptionReadTimer(System.Timers.Timer readTimer)
        {
            try
            {
                if (readTimer != null)
                {
                    readTimer.Enabled = false;
                    readTimer.Dispose();
                }
            }
            catch
            {
                // Ignore any error clearing this timer
            }
        }

        /// <summary>
        /// Stop all recurring subscription reads
        /// </summary>
        public void StopAllSubscriptionReadTimers()
        {
            foreach (System.Timers.Timer readTimer in ReadTimers)
                StopSubscriptionReadTimer(readTimer);
        }

        #endregion

        #region On-Device Variable one-time Reading

        /// <summary>
        /// Read one or more variables on the device
        /// </summary>
        /// <param name="Variables">Collection of variable instances to read</param>
        /// <returns>Dictionary with keys of variable names and values of dynamic variable types reflecting current variable values on the device</returns>
        public async Task<Dictionary<string, dynamic>> ReadVariables(IEnumerable<MPiecVariable> Variables)
        {
            Dictionary<string, dynamic> Values = new Dictionary<string, dynamic>();
            await Task.Run(() =>
            {
                try
                {
                    IList<string> VarNames = Variables.Select(v => v.DeviceName).ToList();
                    IList<object> VarValues = DataAccess.ReadVariables(VarNames);

                    int index = 0;
                    foreach (MPiecVariable Variable in Variables)
                        Values.Add(Variable.Name, VarValues[index++]);
                }
                catch (Exception ex)
                {
                    ErrorReadingVariables?.Invoke(this, new MPIECVariableReadErrorEventArgs(Variables.ToList(), ex));
                }
            });
            return Values;
        }

        /// <summary>
        /// Read one or more global variables on the device
        /// </summary>
        /// <param name="VariableDefinitions">Variable names and values to write as variable definition strings. Variable Definition Strings have the format:
        /// 
        /// "VARNAME:VARTYPE"
        /// 
        /// where:
        /// VARNAME: Name of variable on device
        /// VARTYPE: Variable type as defined in MotionWorks
        /// <returns>Dictionary with keys of variable names and values of dynamic variable types reflecting current variable values on the device</returns>
        public async Task<Dictionary<string, dynamic>> ReadGlobalVariables(IEnumerable<string> VariableDefinitions)
        {
            List<MPiecVariable> Variables = new List<MPiecVariable>();
            foreach (string Definition in VariableDefinitions)
                Variables.Add(new MPiecVariable(Definition));

            return await ReadVariables(Variables);
        }

        /// <summary>
        /// Read one or more local variables on the device
        /// </summary>
        /// <param name="VariableDefinitions">Variable names and values to write as variable definition strings. Variable Definition Strings have the format:
        /// 
        /// "VARNAME:VARTYPE"
        /// 
        /// where:
        /// VARNAME: Name of variable on device
        /// VARTYPE: Variable type as defined in MotionWorks
        /// <returns>Dictionary with keys of variable names and values of dynamic variable types reflecting current variable values on the device</returns>
        public async Task<Dictionary<string, dynamic>> ReadLocalVariables(IEnumerable<string> VariableDefinitions)
        {
            List<MPiecVariable> Variables = new List<MPiecVariable>();
            foreach (string Definition in VariableDefinitions)
                Variables.Add(new MPiecVariable(Definition, MPiecVariable.Instance));

            return await ReadVariables(Variables);
        }

        #endregion

        #region On-Device Variable Writing

        /// <summary>
        /// Write to one or more variables on the device
        /// </summary>
        /// <param name="Variables">Collection of variable instances to write. Value in the instance will be written to the on-device variable.</param>
        /// <returns>TRUE if all values are being written to the device, FALSE on any error</returns>
        public async Task<bool> WriteVariables(IEnumerable<MPiecVariable> Variables)
        {
            return await Task.Run(() =>
            {
                try
                {
                    IList<string> VarNames = Variables.Select(v => v.DeviceName).ToList();
                    IList<dynamic> VarValues = Variables.Select(v => v.Value).ToList();
                    DataAccess.WriteVariables(VarNames, VarValues);
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            });
        }

        /// <summary>
        /// Write to one or more global variables on the device
        /// </summary>
        /// <param name="VariableDefinitions">Variable names and values to write as variable definition strings. Variable Definition Strings have the format:
        /// 
        /// "VARNAME:VARTYPE=VALUE"
        /// 
        /// where:
        /// VARNAME: Name of variable on device
        /// VARTYPE: Variable type as defined in MotionWorks
        /// VALUE: String representation of variable value</param>
        /// <returns>TRUE if all values are being written to the device, FALSE on any error</returns>
        public async Task<bool> WriteGlobalVariables(IEnumerable<string> VariableDefinitions)
        {
            List<MPiecVariable> Variables = new List<MPiecVariable>();
            foreach (string Definition in VariableDefinitions)
                Variables.Add(new MPiecVariable(Definition));

            return await WriteVariables(Variables);
        }

        /// <summary>
        /// Write to one or more local variables on the device
        /// </summary>
        /// <param name="VariableDefinitions">Variable names and values to write as variable definition strings. Variable Definition Strings have the format:
        /// 
        /// "VARNAME:VARTYPE=VALUE"
        /// 
        /// where:
        /// VARNAME: Name of variable on device
        /// VARTYPE: Variable type as defined in MotionWorks
        /// VALUE: String representation of variable value</param>
        /// <returns>TRUE if all values are being written to the device, FALSE on any error</returns>
        public async Task<bool> WriteLocalVariables(IEnumerable<string> VariableDefinitions)
        {
            List<MPiecVariable> Variables = new List<MPiecVariable>();
            foreach (string Definition in VariableDefinitions)
                Variables.Add(new MPiecVariable(Definition, MPiecVariable.Instance));

            return await WriteVariables(Variables);
        }

        /// <summary>
        /// Set a variable on the device to a TRUE or FALSE value
        /// (sets non-BOOL variable types as possible)
        /// </summary>
        /// <param name="Variable">Variable to set</param>
        /// <param name="isTrue">TRUE or FALSE value to set variable to</param>
        /// <returns>TRUE if the value was written to the device, FALSE otherwise</returns>
        public async Task<bool> SetVariable(MPiecVariable Variable, bool isTrue)
        {
            Variable.Value = Variable.VariableType.Parse(isTrue);
            return await WriteVariables(new List<MPiecVariable>() { Variable });
        }

        #endregion

        #region PLC Control

        /// <summary>
        /// Activate the boot program (copy from Flash memory to RAM). Required on new upload and after reset
        /// before starting the program.
        /// </summary>
        /// <returns>TRUE on successful activation, FALSE otherwise</returns>
        public async Task<bool> Initialize()
        {
            return await Task.Run(() =>
            {
                try
                {
                    ControlService.ActivateBootProject();
                }
                catch (Exception ex)
                {
                    OnControlError(new MPIECControlErrorEventArgs(ex));
                    return false;
                }
                return true;
            });
        }

        private async Task<bool> Start(StartMode mode = StartMode.WarmStart)
        {
            return await Task.Run(() =>
            {
                try
                {
                    ControlService.StartPlc(mode);
                }
                catch (Exception ex)
                {
                    OnControlError(new MPIECControlErrorEventArgs(ex));
                    return false;
                }
                return true;
            });
        }

        /// <summary>
        /// Warm start the application on the device
        /// </summary>
        /// <returns>TRUE on successful start, FALSE otherwise</returns>
        public async Task<bool> WarmStart()
        {
            return await Start(StartMode.WarmStart);
        }

        /// <summary>
        /// Cold start the application on the device
        /// </summary>
        /// <returns>TRUE on successful start, FALSE otherwise</returns>
        public async Task<bool> ColdStart()
        {
            return await Start(StartMode.ColdStart);
        }

        /// <summary>
        /// Hot start the application on the device
        /// </summary>
        /// <returns>TRUE on successful start, FALSE otherwise</returns>
        public async Task<bool> HotStart()
        {
            return await Start(StartMode.HotStart);
        }

        /// <summary>
        /// Stop the PLC application on the motion controller.  Motion will stop.
        /// Other controller services (I/O drivers, Mechatrolink network, etc) will continue to operate
        /// </summary>
        /// <returns>TRUE on successful start, FALSE otherwise</returns>
        public async Task<bool> Stop()
        {
            return await Task.Run(() =>
            {
                try
                {
                    ControlService.StopPlc();
                }
                catch (Exception ex)
                {
                    OnControlError(new MPIECControlErrorEventArgs(ex));
                    return false;
                }
                return true;
            });
        }

        /// <summary>
        /// Reset the PLC
        /// </summary>
        /// <returns>TRUE on sucessful reset, FALSE otherwise</returns>
        public async Task<bool> ResetPLC()
        {
            return await Task.Run(() =>
            {
                try
                {
                    ControlService.Reset();
                }
                catch (Exception ex)
                {
                    OnControlError(new MPIECControlErrorEventArgs(ex));
                    return false;
                }
                return true;
            });
        }

        #endregion

        #region File I/O

        /// <summary>
        /// Read data from a file on the device
        /// </summary>
        /// <param name="fileName">Full path on device of file to read ("/flash/user/config/file.xml")</param>
        /// <returns>Data from file on success, null on any error</returns>
        public async Task<MemoryStream> GetFile(string fileName)
        {
            return await Task.Run(() =>
            {
                try
                {
                    int blockSize = (int)FileService.GetTransferPaketSize();
                    int ReadHandle = FileService.OpenRead(fileName);

                    MemoryStream memoryStream = new MemoryStream();
                    byte[] DataBuffer = new byte[blockSize];
                    while (FileService.Read(ReadHandle, out DataBuffer, blockSize))
                        memoryStream.Write(DataBuffer, 0, DataBuffer.Length);
                    FileService.Close(ReadHandle);

                    return memoryStream;
                }
                catch (Exception ex)
                {
                    OnFileReadError(new MPIECFileEventArgs(fileName, false, ex));
                }
                return null;
            });
        }

        /// <summary>
        /// Save data to a file on the device
        /// </summary>
        /// <param name="fileName">Full path on device of file to write ("/flash/user/config/file.xml")</param>
        /// <param name="fileData">Data to write to file</param>
        /// <returns>TRUE on successful file save, FALSE on any error</returns>
        public async Task<bool> PutFile(string fileName, MemoryStream fileData)
        {
            return await Task.Run(() =>
            {
                try
                {
                    int WriteHandle = FileService.OpenWrite(fileName);
                    FileService.Write(WriteHandle, fileData.ToArray());
                    FileService.Close(WriteHandle);
                }
                catch (Exception ex)
                {
                    OnFileWriteError(new MPIECFileEventArgs(fileName, true, ex));
                    return false;
                }
                return true;
            });
        }

        /// <summary>
        /// Remove a file from the device
        /// </summary>
        /// <param name="fileName">Full path on device of file to delete ("/flash/user/config/file.xml")</param>
        /// <returns>TRUE on successful delete, FALSE on any error</returns>
        public async Task<bool> DeleteFile(string fileName)
        {
            return await Task.Run(() =>
            {
                try
                {
                    FileService.RemoveFile(fileName);
                }
                catch (Exception ex)
                {
                    OnFileWriteError(new MPIECFileEventArgs(fileName, true, ex));
                    return false;
                }
                return true;
            });
        }

        #endregion
    }
}

