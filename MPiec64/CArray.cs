﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public class CArray<T> : IDisposable
    {
        public IPLC32Wrapper Plci { get; private set; }  = null;
        private readonly IList<object> myEnumerable;
        private bool tempIntPtr;
        private UInt32 objectIntPtr;
        private bool disposed;

        public CArray(IPLC32Wrapper wrapper)
        {
            this.Plci = wrapper;
            this.myEnumerable = new List<object>();
            this.objectIntPtr = GetObjectPointer(Plci);
        }

        public CArray(IPLC32Wrapper wrapper, IEnumerable<T> array) : this(wrapper)
        {
            foreach (T obj in array)
                this.Add((object)obj);
        }

        public CArray(IPLC32Wrapper wrapper, IEnumerable<string> array) : this(wrapper)
        {
            foreach (string str in array)
                this.Add((object)new CString(Plci, str));
        }

        public CArray(UInt32 cArrayPtr, bool ownAndFreeIntPtr)
        {
            this.myEnumerable = (IList<object>)new List<object>();
            this.objectIntPtr = cArrayPtr;
            this.tempIntPtr = !ownAndFreeIntPtr;
        }

        ~CArray()
        {
            this.Dispose(false);
        }

        public UInt32 ObjectIntPtr
        {
            get
            {
                return this.objectIntPtr;
            }
            set
            {
                if (this.ObjectIntPtr != 0 && !this.tempIntPtr)
                {
                    this.DisposeNativeResources();
                    this.objectIntPtr = value;
                }
                else
                    this.objectIntPtr = value;
            }
        }

        public int Size
        {
            get
            {
                return this.CArrayGetSizeCObject();
            }
        }

        public bool IsEmpty
        {
            get
            {
                return this.Size == 0;
            }
        }

        internal void SetTempObjectIntPtr(UInt32 pointer)
        {
            this.objectIntPtr = pointer;
            this.tempIntPtr = true;
        }

        public bool Add(object value)
        {
            if (object.ReferenceEquals(value, (object)null))
                throw new RecoverablePlciException(string.Format("The array element was null!", (object)(this.Size + 1)));
            if (value.GetType() != typeof(T))
                throw new PlciException(string.Format("Wrong type \"{0}\" expected type is \"{1}\".", (object)value.GetType().Name, (object)typeof(T).Name));
            bool flag1 = false;
            switch (value.GetType().Name)
            {
                case "Boolean":
                    bool flag2 = (bool)value;
                    flag1 = Plci.CArrayPushBackBoolean(this.ObjectIntPtr, ref flag2);
                    break;
                case "Char":
                    char ch = (char)value;
                    flag1 = Plci.CArrayPushBackChar(this.ObjectIntPtr, ref ch);
                    break;
                case "Byte":
                    byte num1 = (byte)value;
                    flag1 = Plci.CArrayPushBackByte(this.ObjectIntPtr, ref num1);
                    break;
                case "SByte":
                    sbyte num2 = (sbyte)value;
                    flag1 = Plci.CArrayPushBackSByte(this.ObjectIntPtr, ref num2);
                    break;
                case "Int16":
                    short num3 = (short)value;
                    flag1 = Plci.CArrayPushBackInt16(this.ObjectIntPtr, ref num3);
                    break;
                case "UInt16":
                    ushort num4 = (ushort)value;
                    flag1 = Plci.CArrayPushBackUInt16(this.ObjectIntPtr, ref num4);
                    break;
                case "Int32":
                    int num5 = (int)value;
                    flag1 = Plci.CArrayPushBackInt32(this.ObjectIntPtr, ref num5);
                    break;
                case "UInt32":
                    uint num6 = (uint)value;
                    flag1 = Plci.CArrayPushBackUInt32(this.ObjectIntPtr, ref num6);
                    break;
                case "Int64":
                    long num7 = (long)value;
                    flag1 = Plci.CArrayPushBackInt64(this.ObjectIntPtr, ref num7);
                    break;
                case "UInt64":
                    ulong num8 = (ulong)value;
                    flag1 = Plci.CArrayPushBackUInt64(this.ObjectIntPtr, ref num8);
                    break;
                case "Single":
                    float num9 = (float)value;
                    flag1 = Plci.CArrayPushBackFloat(this.ObjectIntPtr, ref num9);
                    break;
                case "Double":
                    double num10 = (double)value;
                    flag1 = Plci.CArrayPushBackDouble(this.ObjectIntPtr, ref num10);
                    break;
                case "String":
                    flag1 = Plci.CArrayPushBackCString(this.ObjectIntPtr, value.ToString().StringToCString(Plci).ObjectIntPtr);
                    break;
                case "CObject":
                    if (value is CObject cobject)
                    {
                        flag1 = Plci.CArrayPushBackCObject(this.ObjectIntPtr, cobject.ObjectIntPtr);
                        break;
                    }
                    break;
            }
            return flag1;
        }

        public bool Add(string value, EncodingType encoding)
        {
            bool flag = false;
            if (value == null)
                throw new RecoverablePlciException(string.Format("The array element was null!", (object)(this.Size + 1)));
            if (typeof(T).Name == "String")
                flag = Plci.CArrayPushBackCString(this.ObjectIntPtr, value.StringToCString(Plci).ObjectIntPtr);
            return flag;
        }

        public T GetElement(int position)
        {
            object obj = (object)null;
            switch (typeof(T).Name)
            {
                case "Boolean":
                    obj = (object)Plci.CArrayGetElementBoolean(this.ObjectIntPtr, (uint)position);
                    break;
                case "Char":
                    obj = (object)Plci.CArrayGetElementChar(this.ObjectIntPtr, (uint)position);
                    break;
                case "Byte":
                    obj = (object)Plci.CArrayGetElementByte(this.ObjectIntPtr, (uint)position);
                    break;
                case "SByte":
                    obj = (object)Plci.CArrayGetElementSByte(this.ObjectIntPtr, (uint)position);
                    break;
                case "Int16":
                    obj = (object)Plci.CArrayGetElementInt16(this.ObjectIntPtr, (uint)position);
                    break;
                case "UInt16":
                    obj = (object)Plci.CArrayGetElementUInt16(this.ObjectIntPtr, (uint)position);
                    break;
                case "Int32":
                    obj = (object)Plci.CArrayGetElementInt32(this.ObjectIntPtr, (uint)position);
                    break;
                case "UInt32":
                    obj = (object)Plci.CArrayGetElementUInt32(this.ObjectIntPtr, (uint)position);
                    break;
                case "Int64":
                    long num1 = 0;
                    Plci.CArrayGetElementInt64(this.ObjectIntPtr, (uint)position, ref num1);
                    obj = (object)num1;
                    break;
                case "UInt64":
                    ulong num2 = 0;
                    Plci.CArrayGetElementUInt64(this.ObjectIntPtr, (uint)position, ref num2);
                    obj = (object)num2;
                    break;
                case "Single":
                    float num3 = 0.0f;
                    Plci.CArrayGetElementFloat(this.ObjectIntPtr, (uint)position, ref num3);
                    obj = (object)num3;
                    break;
                case "Double":
                    double num4 = 0.0;
                    Plci.CArrayGetElementDouble(this.ObjectIntPtr, (uint)position, ref num4);
                    obj = (object)num4;
                    break;
                case "String":
                    CString cstring = new CString(Plci);
                    UInt32 objectIntPtr1 = cstring.ObjectIntPtr;
                    Plci.CArrayGetElementCString(this.ObjectIntPtr, (uint)position, objectIntPtr1);
                    obj = (object)cstring.GetString();
                    break;
                case "CObject":
                    CObject cobject = new CObject(Plci);
                    UInt32 objectIntPtr2 = cobject.ObjectIntPtr;
                    Plci.CArrayGetElementCObject(this.ObjectIntPtr, (uint)position, objectIntPtr2);
                    obj = (object)cobject;
                    break;
            }
            return (T)Convert.ChangeType(obj, typeof(T), (IFormatProvider)NumberFormatInfo.InvariantInfo);
        }

        public IList<T> CArrayToIList()
        {
            List<T> objList = new List<T>(this.Size);
            for (int position = 0; position < this.Size; ++position)
                objList.Add(this.GetElement(position));
            return (IList<T>)objList;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int position = 0; position < this.Size; ++position)
            {
                object element = (object)this.GetElement(position);
                if (element != null)
                {
                    if (stringBuilder.Length > 0)
                        stringBuilder.Append(", ");
                    stringBuilder.Append(element);
                }
            }
            return stringBuilder.ToString();
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
                return;
            int num = disposing ? 1 : 0;
            this.ObjectIntPtr = 0;
            this.disposed = true;
        }

        private static UInt32 GetObjectPointer(IPLC32Wrapper Plci)
        {
            switch (typeof(T).Name)
            {
                case "Boolean":
                    return Plci.CArrayCreateInstanceBoolean();
                case "Char":
                    return Plci.CArrayCreateInstanceChar();
                case "Byte":
                    return Plci.CArrayCreateInstanceByte();
                case "SByte":
                    return Plci.CArrayCreateInstanceSByte();
                case "Int16":
                    return Plci.CArrayCreateInstanceInt16();
                case "UInt16":
                    return Plci.CArrayCreateInstanceUInt16();
                case "Int32":
                    return Plci.CArrayCreateInstanceInt32();
                case "UInt32":
                    return Plci.CArrayCreateInstanceUInt32();
                case "Int64":
                    return Plci.CArrayCreateInstanceInt64();
                case "UInt64":
                    return Plci.CArrayCreateInstanceUInt64();
                case "Single":
                    return Plci.CArrayCreateInstanceFloat();
                case "Double":
                    return Plci.CArrayCreateInstanceDouble();
                case "String":
                    return Plci.CArrayCreateInstanceCString();
                case "CObject":
                    return Plci.CArrayCreateInstanceCObject();
                default:
                    return 0;
            }
        }

        private int CArrayGetSizeCObject()
        {
            switch (typeof(T).Name)
            {
                case "Boolean":
                    return (int)Plci.CArrayGetSizeBoolean(this.ObjectIntPtr);
                case "Char":
                    return (int)Plci.CArrayGetSizeChar(this.ObjectIntPtr);
                case "Byte":
                    return (int)Plci.CArrayGetSizeByte(this.ObjectIntPtr);
                case "SByte":
                    return (int)Plci.CArrayGetSizeSByte(this.ObjectIntPtr);
                case "Int16":
                    return (int)Plci.CArrayGetSizeInt16(this.ObjectIntPtr);
                case "UInt16":
                    return (int)Plci.CArrayGetSizeUInt16(this.ObjectIntPtr);
                case "Int32":
                    return (int)Plci.CArrayGetSizeInt32(this.ObjectIntPtr);
                case "UInt32":
                    return (int)Plci.CArrayGetSizeUInt32(this.ObjectIntPtr);
                case "Int64":
                    return (int)Plci.CArrayGetSizeInt64(this.ObjectIntPtr);
                case "UInt64":
                    return (int)Plci.CArrayGetSizeUInt64(this.ObjectIntPtr);
                case "Single":
                    return (int)Plci.CArrayGetSizeFloat(this.ObjectIntPtr);
                case "Double":
                    return (int)Plci.CArrayGetSizeDouble(this.ObjectIntPtr);
                case "String":
                    return (int)Plci.CArrayGetSizeCString(this.ObjectIntPtr);
                case "CObject":
                    return (int)Plci.CArrayGetSizeCObject(this.ObjectIntPtr);
                default:
                    return 0;
            }
        }

        private void DisposeNativeResources()
        {
            switch (typeof(T).Name)
            {
                case "Boolean":
                    Plci.CArrayDisposeInstanceBoolean(this.ObjectIntPtr);
                    break;
                case "Char":
                    Plci.CArrayDisposeInstanceChar(this.ObjectIntPtr);
                    break;
                case "Byte":
                    Plci.CArrayDisposeInstanceByte(this.ObjectIntPtr);
                    break;
                case "SByte":
                    Plci.CArrayDisposeInstanceSByte(this.ObjectIntPtr);
                    break;
                case "Int16":
                    Plci.CArrayDisposeInstanceInt16(this.ObjectIntPtr);
                    break;
                case "UInt16":
                    Plci.CArrayDisposeInstanceUInt16(this.ObjectIntPtr);
                    break;
                case "Int32":
                    Plci.CArrayDisposeInstanceInt32(this.ObjectIntPtr);
                    break;
                case "UInt32":
                    Plci.CArrayDisposeInstanceUInt32(this.ObjectIntPtr);
                    break;
                case "Int64":
                    Plci.CArrayDisposeInstanceInt64(this.ObjectIntPtr);
                    break;
                case "UInt64":
                    Plci.CArrayDisposeInstanceUInt64(this.ObjectIntPtr);
                    break;
                case "Single":
                    Plci.CArrayDisposeInstanceFloat(this.ObjectIntPtr);
                    break;
                case "Double":
                    Plci.CArrayDisposeInstanceDouble(this.ObjectIntPtr);
                    break;
                case "String":
                    Plci.CArrayDisposeInstanceCString(this.ObjectIntPtr);
                    break;
                case "CObject":
                    Plci.CArrayDisposeInstanceCObject(this.ObjectIntPtr);
                    break;
            }
        }
    }
}
