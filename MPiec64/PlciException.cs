﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public class PlciException : System.Exception
    {
        private readonly object extAccess = new object();
        protected const int NoException = 0;
        protected const int NormalException = 1;
        protected const int RecoverableException = 2;
        protected const int FatalException = 3;
        protected const int BadAllocException = 4;
        protected const int RecoverableFileException = 7;

        internal PlciException(string message)
          : base(message)
        {
            this.DateTimeReceived = DateTime.Now;
        }

        internal PlciException(int code, int additionalCode)
          : base(PlciException.GetMessage(code))
        {
            this.Code = code & (int)ushort.MaxValue;
            this.AdditionalCode = additionalCode;
            this.DateTimeReceived = DateTime.Now;
        }

        internal PlciException(PlciExceptionData data)
          : base(PlciException.GetMessage(data.Code))
        {
            this.Code = data.Code;
            this.AdditionalCode = data.AddCode;
            this.DateTimeReceived = DateTime.Now;
        }

        internal PlciException(string message, System.Exception innerException)
          : base(message, innerException)
        {
            lock (this.extAccess)
                this.DateTimeReceived = DateTime.Now;
        }

        public DateTime DateTimeReceived { get; private set; }

        public int Code { get; private set; }

        public int AdditionalCode { get; set; }

        protected static string GetMessage(int code)
        {
            string str = PlciErrorStrings.GetErrorInformation(ErrorSource.PlciError, code);
            if (string.IsNullOrEmpty(str))
                str = string.Format("This error code \"{0}\" is not implemented.", (object)code);
            return str;
        }

        internal static void ThrowSpecializedError(PlciExceptionData exception)
        {
            switch (exception.ExceptionClassType)
            {
                case 0:
                    break;
                case 2:
                    throw new RecoverablePlciException(exception);
                case 3:
                    throw new FatalPlciException(exception);
                case 4:
                    throw new BadAllocPlciException(exception);
                case 7:
                    throw new RecoverableFileException(exception);
                default:
                    throw new PlciException(exception);
            }
        }
    }
}
