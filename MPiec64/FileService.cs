﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public class FileService : IService, IFileService, IDisposable
    {
        public IPLC32Wrapper Plci { get; private set; } = null;
        private UInt32 plcFileServicePtr = 0;
        private Plci64 plcConnection;
        private bool disposed;

        public FileService()
        {
            this.Plci = null;
            this.plcConnection = (Plci64)null;
        }

        ~FileService()
        {
            this.Dispose(false);
        }

        public bool InitService(IPLC32Wrapper wrapper, Plci64 plcHandle)
        {
            this.Plci = wrapper;
            this.plcConnection = plcHandle;
            return !(this.plcConnection.PlcIntPtr == 0) && Plci.FileServiceGetService(this.plcConnection.PlcIntPtr, out this.plcFileServicePtr);
        }

        public int OpenRead(string fileName)
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                this.plcFileServicePtr.ThrowExceptionIfPointerZero("PlcFileService");
                this.plcConnection.ResetLastError();
                int num = Plci.FileServiceOpenRead(this.plcConnection.PlcIntPtr, this.plcFileServicePtr, Encoding.UTF8.GetBytes(fileName));
                this.plcConnection.CheckAndThrowError();
                return num;
            }
        }

        public int OpenWrite(string fileName)
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                this.plcFileServicePtr.ThrowExceptionIfPointerZero("PlcFileService");
                this.plcConnection.ResetLastError();
                int num = Plci.FileServiceOpenWrite(this.plcConnection.PlcIntPtr, this.plcFileServicePtr, Encoding.UTF8.GetBytes(fileName));
                this.plcConnection.CheckAndThrowError();
                return num;
            }
        }

        public bool Read(int fileHandle, out byte[] data, int lengthToRead)
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                CArray<byte> carray = new CArray<byte>(Plci);
                UInt32 objectIntPtr = carray.ObjectIntPtr;
                this.plcFileServicePtr.ThrowExceptionIfPointerZero("PlcFileService");
                this.plcConnection.ResetLastError();
                bool flag = Plci.FileServiceRead(this.plcConnection.PlcIntPtr, this.plcFileServicePtr, fileHandle, ref objectIntPtr, lengthToRead);
                data = new byte[carray.Size];
                for (int position = 0; position < carray.Size; ++position)
                    data[position] = carray.GetElement(position);
                this.plcConnection.CheckAndThrowError();
                return flag;
            }
        }

        public void Write(int fileHandle, byte[] data)
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                this.plcFileServicePtr.ThrowExceptionIfPointerZero("PlcFileService");
                this.plcConnection.ResetLastError();
                Plci.FileServiceWrite(this.plcConnection.PlcIntPtr, this.plcFileServicePtr, fileHandle, data, data.Length);
                this.plcConnection.CheckAndThrowError();
            }
        }

        public void Close(int fileHandle)
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                this.plcFileServicePtr.ThrowExceptionIfPointerZero("PlcFileService");
                this.plcConnection.ResetLastError();
                Plci.FileServiceClose(this.plcConnection.PlcIntPtr, this.plcFileServicePtr, fileHandle);
                this.plcConnection.CheckAndThrowError();
            }
        }

        public void RemoveFile(string fileName)
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                this.plcFileServicePtr.ThrowExceptionIfPointerZero("PlcFileService");
                this.plcConnection.ResetLastError();
                Plci.FileServiceRemoveFile(this.plcConnection.PlcIntPtr, this.plcFileServicePtr, Encoding.UTF8.GetBytes(fileName));
                this.plcConnection.CheckAndThrowError();
            }
        }

        public uint GetTransferPaketSize()
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                this.plcFileServicePtr.ThrowExceptionIfPointerZero("PlcFileService");
                this.plcConnection.ResetLastError();
                uint transferPaketSize = Plci.FileServiceGetTransferPaketSize(this.plcConnection.PlcIntPtr, this.plcFileServicePtr);
                this.plcConnection.CheckAndThrowError();
                return transferPaketSize;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
                return;
            int num = disposing ? 1 : 0;
            if (this.plcFileServicePtr != 0)
            {
                Plci.FileServiceDelete(this.plcFileServicePtr);
                this.plcFileServicePtr = 0;
            }
            this.disposed = true;
        }
    }
}
