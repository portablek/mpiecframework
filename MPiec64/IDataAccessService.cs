﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public interface IDataAccessService : IDisposable
    {
        Subscription CreateSubscription(IList<string> variableNames);
        void DestroySubscription(Subscription subscriptionHandle);
        IList<object> GetResponse(Subscription subscriptionHandle);
        IList<object> ReadVariables(IList<string> variableNames);
        void WriteVariables(IList<string> variableNames, IList<object> values);
    }
}
