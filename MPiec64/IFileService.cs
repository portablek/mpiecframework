﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public interface IFileService : IDisposable
    {
        int OpenRead(string fileName);

        int OpenWrite(string fileName);

        bool Read(int fileHandle, out byte[] data, int lengthToRead);

        void Write(int fileHandle, byte[] data);

        void Close(int fileHandle);

        void RemoveFile(string fileName);

        uint GetTransferPaketSize();
    }
}
