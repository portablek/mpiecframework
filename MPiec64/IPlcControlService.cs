﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public interface IPlcControlService : IDisposable
    {
        void StopPlc();

        void StartPlc(StartMode mode);

        void Reset();

        void ActivateBootProject();

        void DeleteBootProject();
    }
}
