﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public static class PLCICommon
    {
        public const CallingConvention CallingConvSetting = CallingConvention.Cdecl;
        public const string Boolean = "Boolean";
        public const string Char = "Char";
        public const string Byte = "Byte";
        public const string SByte = "SByte";
        public const string Int16 = "Int16";
        public const string UInt16 = "UInt16";
        public const string Int32 = "Int32";
        public const string UInt32 = "UInt32";
        public const string Int64 = "Int64";
        public const string UInt64 = "UInt64";
        public const string Single = "Single";
        public const string Double = "Double";
        public const string String = "String";
        public const string ClrInt64 = "ClrInt64";
        public const string Object = "Object";
        public const string CObject = "CObject";
        public const string CString = "CString";
        public const string Plc = "PLC";
        public const string PlcControlService = "PlcControlService";
        public const string DeviceAttributeService = "DeviceAttributeService";
        public const string PlcFileService = "PlcFileService";
        public const string DataAccessService = "DataAccessService";

        public static void CheckPlciNotNull(Plci64 plciConnection)
        {
            if (plciConnection == null)
                throw new FatalPlciException(string.Format("The object instance \"{0}\" was null.", (object)"PLC"));
            if (plciConnection.PlcIntPtr == 0)
                throw new FatalPlciException("The PLC was disconnected.");
        }

        public static void ThrowExceptionIfPointerZero(this UInt32 pointer, string className)
        {
            if (!(pointer == 0))
                return;
            if (className == "PLC")
                throw new FatalPlciException("The PLC was disconnected.");
            throw new FatalPlciException(string.Format("The object instance \"{0}\" was disposed.", (object)className));
        }

        public static object CObjectToObject(this CObject data)
        {
            object obj = (object)null;
            if (data != null)
            {
                switch (data.GetMetaElementType())
                {
                    case MetaElementType.TypeUnknown:
                    case MetaElementType.TypeVoid:
                        break;
                    case MetaElementType.TypeBool:
                        obj = (object)data.GetValue<bool>();
                        break;
                    case MetaElementType.TypeChar:
                        obj = (object)data.GetValue<char>();
                        break;
                    case MetaElementType.TypeInt8:
                        obj = (object)data.GetValue<sbyte>();
                        break;
                    case MetaElementType.TypeUint8:
                        obj = (object)data.GetValue<byte>();
                        break;
                    case MetaElementType.TypeInt16:
                        obj = (object)data.GetValue<short>();
                        break;
                    case MetaElementType.TypeUint16:
                        obj = (object)data.GetValue<ushort>();
                        break;
                    case MetaElementType.TypeInt32:
                        obj = (object)data.GetValue<int>();
                        break;
                    case MetaElementType.TypeUint32:
                        obj = (object)data.GetValue<uint>();
                        break;
                    case MetaElementType.TypeInt64:
                        obj = (object)data.GetValue<long>();
                        break;
                    case MetaElementType.TypeUint64:
                        obj = (object)data.GetValue<ulong>();
                        break;
                    case MetaElementType.TypeReal32:
                        obj = (object)data.GetValue<float>();
                        break;
                    case MetaElementType.TypeReal64:
                        obj = (object)data.GetValue<double>();
                        break;
                    case MetaElementType.TypeString:
                        obj = (object)data.GetValue<string>();
                        break;
                    case MetaElementType.TypeArray:
                        obj = GetArrayElementsFromCObject(data);
                        break;
                    default:
                        throw new PlciException("This CObject meta element type is not supported");
                }
            }
            return obj;
        }

        private static object GetArrayElementsFromCObject(CObject data)
        {
            object obj = (object)null;
            if (data.IsArray() || data.IsElementaryArray())
            {
                switch (data.GetMetaElementArrayType())
                {
                    case MetaElementType.TypeBool:
                        obj = (object)data.GetArray<bool>();
                        break;
                    case MetaElementType.TypeChar:
                        obj = (object)data.GetArray<char>();
                        break;
                    case MetaElementType.TypeInt8:
                        obj = (object)data.GetArray<sbyte>();
                        break;
                    case MetaElementType.TypeUint8:
                        obj = (object)data.GetArray<byte>();
                        break;
                    case MetaElementType.TypeInt16:
                        obj = (object)data.GetArray<short>();
                        break;
                    case MetaElementType.TypeUint16:
                        obj = (object)data.GetArray<ushort>();
                        break;
                    case MetaElementType.TypeInt32:
                        obj = (object)data.GetArray<int>();
                        break;
                    case MetaElementType.TypeUint32:
                        obj = (object)data.GetArray<uint>();
                        break;
                    case MetaElementType.TypeInt64:
                        obj = (object)data.GetArray<long>();
                        break;
                    case MetaElementType.TypeUint64:
                        obj = (object)data.GetArray<ulong>();
                        break;
                    case MetaElementType.TypeReal32:
                        obj = (object)data.GetArray<float>();
                        break;
                    case MetaElementType.TypeReal64:
                        obj = (object)data.GetArray<double>();
                        break;
                    case MetaElementType.TypeString:
                        obj = (object)data.GetArray<string>();
                        break;
                    case MetaElementType.TypeObject:
                        IList<CObject> array = data.GetArray<CObject>();
                        IList<object> objectList = (IList<object>)new List<object>();
                        foreach (CObject data1 in array)
                            objectList.Add(data1.CObjectToObject());
                        obj = (object)objectList;
                        break;
                    default:
                        throw new PlciException("This array meta element type is not supported");
                }
            }
            return obj;
        }

        public static IList<object> CObjectArrayToObjectList(this CArray<CObject> cArrayData)
        {
            IList<object> objectList = (IList<object>)new List<object>();
            for (int position = 0; position < cArrayData.Size; ++position)
            {
                CObject element = cArrayData.GetElement(position);
                if (element != null)
                    objectList.Add(element.CObjectToObject());
            }
            return objectList;
        }

        public static CString StringToCString(this string data, IPLC32Wrapper Plci)
        {
            return new CString(Plci, data);
        }

        public static CObject ObjectToCObject(this object data, IPLC32Wrapper Plci)
        {
            return data.ObjectToCObject(Plci, EncodingType.Utf8);
        }

        public static CObject ObjectToCObject(this object o, IPLC32Wrapper Plci, EncodingType encoding)
        {
            CObject cobject = new CObject(Plci);
            if (o is IList && o.GetType().IsGenericType)
                cobject.SetArray(o, encoding);
            else
                cobject.SetValue(o, encoding);
            return cobject;
        }

        public static CArray<CObject> ObjectListToCArrayOfCObject(this IList<object> objectList, IPLC32Wrapper Plci)
        {
            CArray<CObject> carray = new CArray<CObject>(Plci);
            int num = 0;
            foreach (object data in (IEnumerable<object>)objectList)
            {
                CObject cobject = data.ObjectToCObject(Plci);
                if (cobject == null)
                    throw new RecoverablePlciException(string.Format("The array element was null!", (object)num));
                carray.Add((object)cobject);
                ++num;
            }
            return carray;
        }

        public static CArray<T> GenericListToCArray<T>(this object data, IPLC32Wrapper Plci)
        {
            CArray<T> carray = new CArray<T>(Plci);
            if (data is IList list)
            {
                foreach (object obj in (IEnumerable)list)
                    carray.Add(obj);
            }
            return carray;
        }
    }
}
