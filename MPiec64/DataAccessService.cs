﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public class DataAccessService : IService, IDataAccessService, IDisposable
    {
        public IPLC32Wrapper Plci { get; private set; } = null;
        private UInt32 dataAccessServicePtr = 0;
        private Plci64 plcConnection;
        private bool disposed;

        public DataAccessService()
        {
            this.Plci = null;
            this.plcConnection = null;
            this.ServicePossible = false;
        }

        ~DataAccessService()
        {
            this.Dispose(false);
        }

        public bool ServicePossible { get; private set; }

        public bool InitService(IPLC32Wrapper wrapper, Plci64 plcHandle)
        {
            this.Plci = wrapper;
            this.plcConnection = plcHandle;
            plcConnection.PlcIntPtr.ThrowExceptionIfPointerZero(nameof(DataAccessService));
            if (!Plci.DataAccessServiceGetService(this.plcConnection.PlcIntPtr, out this.dataAccessServicePtr))
                PlciException.ThrowSpecializedError(this.plcConnection.GetLastError());
            return true;
        }

        public IList<object> ReadVariables(IList<string> variableNames)
        {
            lock (this.plcConnection.LockObject)
            {
                this.dataAccessServicePtr.ThrowExceptionIfPointerZero("PlcFileService");
                PLCICommon.CheckPlciNotNull(this.plcConnection);
                this.plcConnection.ResetLastError();
                CArray<CObject> cArrayData = new CArray<CObject>(Plci);
                UInt32 objectIntPtr = cArrayData.ObjectIntPtr;
                UInt32 varList = Plci.DataAccessServiceCreateVarList(this.plcConnection.PlcIntPtr, this.dataAccessServicePtr);
                foreach (string variableName in (IEnumerable<string>)variableNames)
                    Plci.DataAccessServiceAddItemToVarList(Encoding.UTF8.GetBytes(variableName), varList);
                Plci.DataAccessServiceReadVariables(this.plcConnection.PlcIntPtr, this.dataAccessServicePtr, varList, ref objectIntPtr);
                Plci.DataAccessServiceDeleteVarList(varList);
                this.plcConnection.CheckAndThrowError();
                return cArrayData.CObjectArrayToObjectList();
            }
        }

        public void WriteVariables(IList<string> variableNames, IList<object> values)
        {
            lock (this.plcConnection.LockObject)
            {
                this.dataAccessServicePtr.ThrowExceptionIfPointerZero("PlcFileService");
                PLCICommon.CheckPlciNotNull(this.plcConnection);
                this.plcConnection.ResetLastError();
                UInt32 varList = Plci.DataAccessServiceCreateVarList(this.plcConnection.PlcIntPtr, this.dataAccessServicePtr);
                foreach (string variableName in (IEnumerable<string>)variableNames)
                    Plci.DataAccessServiceAddItemToVarList(Encoding.UTF8.GetBytes(variableName), varList);
                UInt32 objectIntPtr = values.ObjectListToCArrayOfCObject(Plci).ObjectIntPtr;
                Plci.DataAccessServiceWriteVariables(this.plcConnection.PlcIntPtr, this.dataAccessServicePtr, varList, ref objectIntPtr);
                Plci.DataAccessServiceDeleteVarList(varList);
                this.plcConnection.CheckAndThrowError();
            }
        }

        public Subscription CreateSubscription(IList<string> variableNames)
        {
            lock (this.plcConnection.LockObject)
            {
                this.dataAccessServicePtr.ThrowExceptionIfPointerZero("PlcFileService");
                PLCICommon.CheckPlciNotNull(this.plcConnection);
                this.plcConnection.ResetLastError();
                UInt32 varList = Plci.DataAccessServiceCreateVarList(this.plcConnection.PlcIntPtr, this.dataAccessServicePtr);
                foreach (string variableName in (IEnumerable<string>)variableNames)
                    Plci.DataAccessServiceAddItemToVarList(Encoding.UTF8.GetBytes(variableName), varList);
                UInt32 subscription1 = Plci.DataAccessServiceCreateSubscription(this.plcConnection.PlcIntPtr, this.dataAccessServicePtr, varList);
                Subscription subscription2 = (Subscription)null;
                if (subscription1 != 0)
                    subscription2 = new Subscription(subscription1);
                Plci.DataAccessServiceDeleteVarList(varList);
                return subscription2;
            }
        }

        public IList<object> GetResponse(Subscription subscriptionHandle)
        {
            lock (this.plcConnection.LockObject)
            {
                this.dataAccessServicePtr.ThrowExceptionIfPointerZero("PlcFileService");
                PLCICommon.CheckPlciNotNull(this.plcConnection);
                this.plcConnection.ResetLastError();
                CArray<CObject> cArrayData = new CArray<CObject>(Plci);
                UInt32 objectIntPtr = cArrayData.ObjectIntPtr;
                Plci.DataAccessServiceGetResponse(this.plcConnection.PlcIntPtr, subscriptionHandle.SubscriptionIntPtr, ref objectIntPtr);
                this.plcConnection.CheckAndThrowError();
                return cArrayData.CObjectArrayToObjectList();
            }
        }

        public void DestroySubscription(Subscription subscriptionHandle)
        {
            lock (this.plcConnection.LockObject)
            {
                this.dataAccessServicePtr.ThrowExceptionIfPointerZero("PlcFileService");
                PLCICommon.CheckPlciNotNull(this.plcConnection);
                this.plcConnection.ResetLastError();
                Plci.DataAccessServiceDestroySubscription(this.plcConnection.PlcIntPtr, subscriptionHandle.SubscriptionIntPtr);
                this.plcConnection.CheckAndThrowError();
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
                return;
            int num = disposing ? 1 : 0;
            if (this.dataAccessServicePtr != 0)
            {
                Plci.DataAccessServiceDeleteService(this.dataAccessServicePtr);
                this.dataAccessServicePtr = 0;
            }
            this.disposed = true;
        }
    }
}
