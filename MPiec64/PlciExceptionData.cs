﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public struct PlciExceptionData
    {
        [MarshalAs(UnmanagedType.I4)]
        public int ExceptionClassType;

        [MarshalAs(UnmanagedType.I4)]
        public int Code;

        [MarshalAs(UnmanagedType.I4)]
        public int AddCode;
    }
}
