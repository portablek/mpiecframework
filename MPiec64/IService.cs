﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    internal interface IService
    {
        bool InitService(IPLC32Wrapper wrapper, Plci64 plcConnection);
    }
}
