﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public enum MetaElementType : byte
    {
        TypeNull = 0,
        TypeUnknown = 0,
        TypeVoid = 1,
        TypeBool = 2,
        TypeChar = 3,
        TypeInt8 = 4,
        TypeUint8 = 5,
        TypeInt16 = 6,
        TypeUint16 = 7,
        TypeInt32 = 8,
        TypeUint32 = 9,
        TypeInt64 = 10, // 0x0A
        TypeUint64 = 11, // 0x0B
        TypeReal32 = 12, // 0x0C
        TypeReal64 = 13, // 0x0D
        TypeString = 14, // 0x0E
        TypePtr = 15, // 0x0F
        TypeStruct = 17, // 0x11
        TypeArray = 20, // 0x14
        TypeObject = 28, // 0x1C
        TypeMaxRange = 255, // 0xFF
    }
}
