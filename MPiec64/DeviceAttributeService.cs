﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public class DeviceAttributeService : IService, IDeviceAttributeService, IDisposable
    {
        public IPLC32Wrapper Plci { get; private set; } = null;
        private UInt32 deviceAttributeServicePtr = 0;
        private Plci64 plcConnection;
        private bool disposed;

        public DeviceAttributeService()
        {
            this.Plci = null;
            this.plcConnection = null;
        }

        ~DeviceAttributeService()
        {
            this.Dispose(false);
        }

        public bool InitService(IPLC32Wrapper wrapper, Plci64 plcHandle)
        {
            this.Plci = wrapper;
            this.plcConnection = plcHandle;
            bool havePLC = !(this.plcConnection.PlcIntPtr == 0);
            bool gotService = Plci.AttributeServiceGetService(this.plcConnection.PlcIntPtr, out deviceAttributeServicePtr);
            return havePLC && gotService;
        }

        public object GetAttribute(StandardDeviceAttribute attributeId)
        {
            return this.CObjectToObject(this.GetAttributeCObject(attributeId));
        }

        public T GetAttribute<T>(StandardDeviceAttribute attributeId)
        {
            return (T)this.GetAttribute(attributeId);
        }

        public IList<object> GetAttributes(IList<short> attributes)
        {
            CArray<CObject> attributesCobject = this.GetAttributesCObject((IEnumerable<short>)attributes);
            IList<object> objectList = (IList<object>)new List<object>();
            for (int position = 0; position < attributesCobject.Size; ++position)
            {
                CObject element = attributesCobject.GetElement(position);
                if (element != null)
                {
                    switch (element.GetMetaElementType())
                    {
                        case MetaElementType.TypeBool:
                            objectList.Add((object)element.GetValue<bool>());
                            continue;
                        case MetaElementType.TypeChar:
                            objectList.Add((object)element.GetValue<char>());
                            continue;
                        case MetaElementType.TypeInt8:
                            objectList.Add((object)element.GetValue<short>());
                            continue;
                        case MetaElementType.TypeUint8:
                            objectList.Add((object)element.GetValue<ushort>());
                            continue;
                        case MetaElementType.TypeInt16:
                            objectList.Add((object)element.GetValue<short>());
                            continue;
                        case MetaElementType.TypeUint16:
                            objectList.Add((object)element.GetValue<ushort>());
                            continue;
                        case MetaElementType.TypeInt32:
                            objectList.Add((object)element.GetValue<int>());
                            continue;
                        case MetaElementType.TypeUint32:
                            objectList.Add((object)element.GetValue<uint>());
                            continue;
                        case MetaElementType.TypeInt64:
                            objectList.Add((object)element.GetValue<long>());
                            continue;
                        case MetaElementType.TypeUint64:
                            objectList.Add((object)element.GetValue<ulong>());
                            continue;
                        case MetaElementType.TypeReal32:
                            objectList.Add((object)element.GetValue<float>());
                            continue;
                        case MetaElementType.TypeReal64:
                            objectList.Add((object)element.GetValue<double>());
                            continue;
                        case MetaElementType.TypeString:
                            objectList.Add((object)element.GetValue<string>());
                            continue;
                        default:
                            continue;
                    }
                }
            }
            return objectList;
        }

        private CObject GetAttributeCObject(StandardDeviceAttribute attributeId)
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                CObject cobject = new CObject(Plci);
                UInt32 objectIntPtr = cobject.ObjectIntPtr;
                this.deviceAttributeServicePtr.ThrowExceptionIfPointerZero(nameof(DeviceAttributeService));
                if (!Plci.AttributeServiceGetAttribute(this.plcConnection.PlcIntPtr, this.deviceAttributeServicePtr, (int)attributeId, ref objectIntPtr))
                    PlciException.ThrowSpecializedError(this.plcConnection.GetLastError());
                return cobject;
            }
        }

        private CArray<CObject> GetAttributesCObject(IEnumerable<short> attributeIds)
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                UInt32 objectIntPtr1 = new CArray<short>(Plci, attributeIds).ObjectIntPtr;
                CArray<CObject> carray = new CArray<CObject>(Plci);
                UInt32 objectIntPtr2 = carray.ObjectIntPtr;
                this.deviceAttributeServicePtr.ThrowExceptionIfPointerZero(nameof(DeviceAttributeService));
                if (!Plci.AttributeServiceGetAttributes(this.plcConnection.PlcIntPtr, this.deviceAttributeServicePtr, ref objectIntPtr1, ref objectIntPtr2))
                    PlciException.ThrowSpecializedError(this.plcConnection.GetLastError());
                return carray;
            }
        }

        private object CObjectToObject(CObject obj)
        {
            switch (obj.GetMetaElementType())
            {
                case MetaElementType.TypeUnknown:
                    return new object();
                case MetaElementType.TypeBool:
                    return (object)obj.GetValue<bool>();
                case MetaElementType.TypeChar:
                    return (object)obj.GetValue<char>();
                case MetaElementType.TypeInt8:
                    return (object)obj.GetValue<short>();
                case MetaElementType.TypeUint8:
                    return (object)obj.GetValue<ushort>();
                case MetaElementType.TypeInt16:
                    return (object)obj.GetValue<short>();
                case MetaElementType.TypeUint16:
                    return (object)obj.GetValue<ushort>();
                case MetaElementType.TypeInt32:
                    return (object)obj.GetValue<int>();
                case MetaElementType.TypeUint32:
                    return (object)obj.GetValue<uint>();
                case MetaElementType.TypeInt64:
                    return (object)obj.GetValue<long>();
                case MetaElementType.TypeUint64:
                    return (object)obj.GetValue<ulong>();
                case MetaElementType.TypeReal32:
                    return (object)obj.GetValue<float>();
                case MetaElementType.TypeReal64:
                    return (object)obj.GetValue<double>();
                case MetaElementType.TypeString:
                    return (object)obj.GetValue<string>();
                case MetaElementType.TypeArray:
                    throw new PlciException(string.Format("Type \"{0}\" is not supported.", (object)MetaElementType.TypeArray));
                default:
                    return (object)null;
            }
        }

        public void SetAttribute<T>(
          StandardDeviceAttribute attributeId,
          T value,
          EncodingType encoding)
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                CObject cobject = ((object)value).ObjectToCObject(Plci, encoding);
                this.deviceAttributeServicePtr.ThrowExceptionIfPointerZero(nameof(DeviceAttributeService));
                if (Plci.AttributeServiceSetAttribute(this.plcConnection.PlcIntPtr, this.deviceAttributeServicePtr, (int)attributeId, cobject.ObjectIntPtr))
                    return;
                PlciException.ThrowSpecializedError(this.plcConnection.GetLastError());
            }
        }

        public void SetAttribute<T>(StandardDeviceAttribute attributeId, T value)
        {
            this.SetAttribute<T>(attributeId, value, EncodingType.Utf16);
        }

        public void SetAttributes(IList<short> attributeIds, IList<object> values)
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                UInt32 objectIntPtr1 = new CArray<short>(Plci, attributeIds).ObjectIntPtr;
                UInt32 objectIntPtr2 = values.ObjectListToCArrayOfCObject(Plci).ObjectIntPtr;
                this.deviceAttributeServicePtr.ThrowExceptionIfPointerZero(nameof(DeviceAttributeService));
                if (Plci.AttributeServiceSetAttributes(this.plcConnection.PlcIntPtr, this.deviceAttributeServicePtr, ref objectIntPtr1, ref objectIntPtr2))
                    return;
                PlciException.ThrowSpecializedError(this.plcConnection.GetLastError());
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
                return;
            int num = disposing ? 1 : 0;
            if (this.deviceAttributeServicePtr != 0)
            {
                Plci.AttributeServiceDelete(this.deviceAttributeServicePtr);
                this.deviceAttributeServicePtr = 0;
            }
            this.disposed = true;
        }
    }
}
