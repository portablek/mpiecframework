﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public class Subscription : IDisposable
    {
        private bool disposed;

        public Subscription(UInt32 inSubscriptionPtr)
        {
            this.SubscriptionIntPtr = inSubscriptionPtr;
        }

        internal UInt32 SubscriptionIntPtr { get; private set; }

        ~Subscription()
        {
            this.Dispose(false);
        }

        public override string ToString()
        {
            return this.SubscriptionIntPtr.ToString("X8");
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
                return;
            int num = disposing ? 1 : 0;
            this.SubscriptionIntPtr = 0;
            this.disposed = true;
        }
    }
}
