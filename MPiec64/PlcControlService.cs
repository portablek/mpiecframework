﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public class PlcControlService : IService, IPlcControlService, IDisposable
    {
        public IPLC32Wrapper Plci { get; private set; } = null;
        private UInt32 plcControlServicePtr = 0;
        private Plci64 plcConnection;
        private bool disposed;

        public PlcControlService()
        {
            this.Plci = null;
            this.plcConnection = null;
        }

        ~PlcControlService()
        {
            this.Dispose(false);
        }

        public bool InitService(IPLC32Wrapper wrapper, Plci64 plcHandle)
        {
            this.Plci = wrapper;
            this.plcConnection = plcHandle;
            return !(this.plcConnection.PlcIntPtr == 0) && Plci.PlcControlServiceGetService(this.plcConnection.PlcIntPtr, out this.plcControlServicePtr);
        }

        public void StopPlc()
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                this.plcControlServicePtr.ThrowExceptionIfPointerZero(nameof(PlcControlService));
                this.plcConnection.ResetLastError();
                Plci.PlcControlServiceStopPlc(this.plcConnection.PlcIntPtr, this.plcControlServicePtr);
                this.plcConnection.CheckAndThrowError();
            }
        }

        public void StartPlc(StartMode mode)
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                this.plcControlServicePtr.ThrowExceptionIfPointerZero(nameof(PlcControlService));
                this.plcConnection.ResetLastError();
                Plci.PlcControlServiceStartPlc(this.plcConnection.PlcIntPtr, this.plcControlServicePtr, (int)mode);
                this.plcConnection.CheckAndThrowError();
            }
        }

        public void Reset()
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                this.plcControlServicePtr.ThrowExceptionIfPointerZero(nameof(PlcControlService));
                this.plcConnection.ResetLastError();
                Plci.PlcControlServiceReset(this.plcConnection.PlcIntPtr, this.plcControlServicePtr);
                this.plcConnection.CheckAndThrowError();
            }
        }

        public void ActivateBootProject()
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                this.plcControlServicePtr.ThrowExceptionIfPointerZero(nameof(PlcControlService));
                this.plcConnection.ResetLastError();
                Plci.PlcControlServiceActivateBootProject(this.plcConnection.PlcIntPtr, this.plcControlServicePtr);
                this.plcConnection.CheckAndThrowError();
            }
        }

        public void DeleteBootProject()
        {
            PLCICommon.CheckPlciNotNull(this.plcConnection);
            lock (this.plcConnection.LockObject)
            {
                this.plcControlServicePtr.ThrowExceptionIfPointerZero(nameof(PlcControlService));
                this.plcConnection.ResetLastError();
                Plci.PlcControlServiceDeleteBootProject(this.plcConnection.PlcIntPtr, this.plcControlServicePtr);
                this.plcConnection.CheckAndThrowError();
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
                return;
            int num = disposing ? 1 : 0;
            if (this.plcControlServicePtr != 0)
            {
                Plci.PlcControlServiceDelete(this.plcControlServicePtr);
                this.plcControlServicePtr = 0;
            }
            this.disposed = true;
        }
    }
}
