﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public class BadAllocPlciException : PlciException
    {
        internal BadAllocPlciException(string message)
          : base(message)
        {
        }

        internal BadAllocPlciException(int code, int addCode)
          : base(code, addCode)
        {
        }

        internal BadAllocPlciException(PlciExceptionData data)
          : base(data)
        {
        }

        internal BadAllocPlciException(string message, System.Exception innerException)
          : base(message, innerException)
        {
        }
    }
}
