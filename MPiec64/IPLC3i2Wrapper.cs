﻿using LegacyWrapper.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    /// <summary>
    /// Interface for wrapper around 32-bit PLCi control DLL for 64-bit processes
    /// </summary>
    [LegacyDllImport(@"PlciBridge.dll")]
    public interface IPLC32Wrapper : IDisposable
    {
        /* 
         * Controller Methods 
         */

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CPlciCreateInstance();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CPlciDisposeInstance(UInt32 plcHandle);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool CPlciConnect(UInt32 plcHandle, byte[] address, ushort port, ushort timeout);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool CPlciConnectStr(UInt32 plcHandle, string connectionString);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CPlciGetLastError(UInt32 plcHandle, [MarshalAs(UnmanagedType.Struct)] ref PlciExceptionData lastError);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CPlciResetLastError(UInt32 plcHandle);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CPlciDisconnect(UInt32 plcHandle);

        /* 
         * Data Access Service Methods 
         */

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool DataAccessServiceGetService(UInt32 plcHandle, out UInt32 serviceHandle);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        int DataAccessServiceDeleteService(UInt32 serviceHandle);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void DataAccessServiceDeleteVarList(UInt32 variableList);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 DataAccessServiceCreateVarList(UInt32 plcHandle, UInt32 serviceHandle);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void DataAccessServiceAddItemToVarList(byte[] name, UInt32 variableList);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void DataAccessServiceReadVariables(UInt32 plcHandle, UInt32 serviceHandle, UInt32 variableList, ref UInt32 values);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void DataAccessServiceWriteVariables(UInt32 plcHandle, UInt32 serviceHandle, UInt32 variableList, ref UInt32 values);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 DataAccessServiceCreateSubscription(UInt32 plcHandle, UInt32 serviceHandle, UInt32 variableList);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void DataAccessServiceGetResponse(UInt32 plcHandle, UInt32 subscription, ref UInt32 values);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void DataAccessServiceDestroySubscription(UInt32 plcHandle, UInt32 subscription);

        /*
         * Device Attribute Service Methods
         */


        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool AttributeServiceGetService(UInt32 plcHandle, out UInt32 serviceHandle);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void AttributeServiceDelete(UInt32 serviceHandle);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool AttributeServiceGetAttribute(UInt32 plcHandle, UInt32 serviceHandle, int attributeId, ref UInt32 cObjectPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool AttributeServiceGetAttributes(UInt32 plcHandle, UInt32 serviceHandle, ref UInt32 attributeIds, ref UInt32 values);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool AttributeServiceSetAttribute(UInt32 plcHandle, UInt32 serviceHandle, int attributeId, UInt32 cObjectPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool AttributeServiceSetAttributes(UInt32 plcHandle, UInt32 serviceHandle, ref UInt32 attributeIds, ref UInt32 values);

        /*
         * PLC Control Service Methods
         */

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool PlcControlServiceGetService(UInt32 plcHandle, out UInt32 serviceHandle);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        int PlcControlServiceDelete(UInt32 serviceHandle);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool PlcControlServiceStartPlc(UInt32 plcHandle, UInt32 serviceHandle, int startmode);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool PlcControlServiceStopPlc(UInt32 plcHandle, UInt32 serviceHandle);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool PlcControlServiceReset(UInt32 plcHandle, UInt32 serviceHandle);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool PlcControlServiceActivateBootProject(UInt32 plcHandle, UInt32 serviceHandle);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool PlcControlServiceDeleteBootProject(UInt32 plcHandle,  UInt32 serviceHandle);

        /*
         * File Service Methods
         */


        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool FileServiceGetService(UInt32 plcHandle, out UInt32 serviceHandle);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void FileServiceDelete(UInt32 serviceHandle);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        int FileServiceOpenRead(UInt32 plcHandle, UInt32 serviceHandle, byte[] fileName);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        int FileServiceOpenWrite(UInt32 plcHandle, UInt32 serviceHandle, byte[] fileName);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool FileServiceRead(UInt32 plcHandle, UInt32 serviceHandle, int fileHandle, ref UInt32 data, int lengthToRead);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void FileServiceWrite(UInt32 plcHandle, UInt32 serviceHandle, int fileHandle, byte[] data, int lenght);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void FileServiceClose(UInt32 plcHandle, UInt32 serviceHandle, int fileHandle);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void FileServiceRemoveFile(UInt32 plcHandle, UInt32 serviceHandle, byte[] fileName);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        uint FileServiceGetTransferPaketSize(UInt32 plcHandle, UInt32 serviceHandle);

        /* 
         * CObject Methods
         */

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectCreateInstance();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectDisposeInstance(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool CObjectIsElementary(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool CObjectIsString(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool CObjectIsValueType(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool CObjectIsArray(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool CObjectIsElementaryArray(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool CObjectIsStringArray(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool CObjectIsStringPtrArray(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        int CObjectGetType(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        int CObjectGetArrayType(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectSetValueBool(UInt32 objPtr, bool value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectSetValueChar(UInt32 objPtr, char value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectSetValueByte(UInt32 objPtr, byte value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectSetValueInt8(UInt32 objPtr, sbyte value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectSetValueInt16(UInt32 objPtr, short value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectSetValueUInt16(UInt32 objPtr, ushort value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectSetValueInt32(UInt32 objPtr, int value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectSetValueUInt32(UInt32 objPtr, uint value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectSetValueInt64(UInt32 objPtr, long value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectSetValueUInt64(UInt32 objPtr, ulong value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectSetValueFloat(UInt32 objPtr, float value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectSetValueDouble(UInt32 objPtr, double value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectSetByteString(UInt32 objPtr, byte[] value, int length, int encodingType);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectSetString(UInt32 objPtr, [MarshalAs(UnmanagedType.LPWStr)] string value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool CObjectGetValueBool(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        char CObjectGetValueChar(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        byte CObjectGetValueByte(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        sbyte CObjectGetValueInt8(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        short CObjectGetValueInt16(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        ushort CObjectGetValueUInt16(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        int CObjectGetValueInt32(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        uint CObjectGetValueUInt32(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectGetValueInt64(UInt32 objPtr, ref long value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectGetValueUInt64(UInt32 objPtr, ref ulong value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectGetValueFloat(UInt32 objPtr, ref float value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectGetValueDouble(UInt32 objPtr, ref double value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CObjectGetByteString(UInt32 objPtr, out IntPtr value, out int length);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
        void CObjectGetString(UInt32 objPtr, out string value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        int CObjectGetEncoding(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        int CObjectGetArrayLength(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectGetArrayBool(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectGetArrayByte(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectGetArrayInt8(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectGetArrayInt16(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectGetArrayUInt16(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectGetArrayInt32(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectGetArrayUInt32(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectGetArrayInt64(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectGetArrayUInt64(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectGetArrayFloat(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectGetArrayDouble(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectGetArrayString(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectGetArrayCObject(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectSetArrayBool(UInt32 objPtr, UInt32 data);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
        UInt32 CObjectSetArrayChar(UInt32 objPtr, UInt32 data);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectSetArrayByte(UInt32 objPtr, UInt32 data);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectSetArrayInt8(UInt32 objPtr, UInt32 data);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectSetArrayInt16(UInt32 objPtr, UInt32 data);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectSetArrayUInt16(UInt32 objPtr, UInt32 data);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectSetArrayInt32(UInt32 objPtr, UInt32 data);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectSetArrayUInt32(UInt32 objPtr, UInt32 data);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectSetArrayInt64(UInt32 objPtr, UInt32 data);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectSetArrayUInt64(UInt32 objPtr, UInt32 data);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectSetArrayFloat(UInt32 objPtr, UInt32 data);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectSetArrayDouble(UInt32 objPtr, UInt32 data);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectSetArrayString(UInt32 objPtr, UInt32 data);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CObjectSetArrayCObject(UInt32 objPtr, UInt32 data);

        /*
         * CArray Methods
         */

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CArrayCreateInstanceBoolean();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CArrayDisposeInstanceBoolean(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        uint CArrayGetSizeBoolean(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayPushBackBoolean(UInt32 objPtr, ref bool value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool CArrayGetElementBoolean(UInt32 objPtr, uint position);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CArrayCreateInstanceChar();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CArrayDisposeInstanceChar(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        uint CArrayGetSizeChar(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayPushBackChar(UInt32 objPtr, ref char value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        char CArrayGetElementChar(UInt32 objPtr, uint position);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CArrayCreateInstanceByte();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CArrayDisposeInstanceByte(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        uint CArrayGetSizeByte(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayPushBackByte(UInt32 objPtr, ref byte value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.U1)]
        byte CArrayGetElementByte(UInt32 objPtr, uint position);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CArrayCreateInstanceSByte();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CArrayDisposeInstanceSByte(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        uint CArrayGetSizeSByte(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayPushBackSByte(UInt32 objPtr, ref sbyte value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        sbyte CArrayGetElementSByte(UInt32 objPtr, uint position);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CArrayCreateInstanceInt16();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CArrayDisposeInstanceInt16(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        uint CArrayGetSizeInt16(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayPushBackInt16(UInt32 objPtr, ref short value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I2)]
        short CArrayGetElementInt16(UInt32 objPtr, uint position);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CArrayCreateInstanceUInt16();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CArrayDisposeInstanceUInt16(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        uint CArrayGetSizeUInt16(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayPushBackUInt16(UInt32 objPtr, ref ushort value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.U2)]
        ushort CArrayGetElementUInt16(UInt32 objPtr, uint position);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CArrayCreateInstanceInt32();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CArrayDisposeInstanceInt32(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        uint CArrayGetSizeInt32(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayPushBackInt32(UInt32 objPtr, ref int value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        int CArrayGetElementInt32(UInt32 objPtr, uint position);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CArrayCreateInstanceUInt32();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CArrayDisposeInstanceUInt32(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        uint CArrayGetSizeUInt32(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayPushBackUInt32(UInt32 objPtr, ref uint value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.U4)]
        uint CArrayGetElementUInt32(UInt32 objPtr, uint position);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CArrayCreateInstanceInt64();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CArrayDisposeInstanceInt64(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        uint CArrayGetSizeInt64(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayPushBackInt64(UInt32 objPtr, ref long value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I8)]
        void CArrayGetElementInt64(UInt32 objPtr, uint position, ref long value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CArrayCreateInstanceUInt64();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CArrayDisposeInstanceUInt64(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        uint CArrayGetSizeUInt64(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayPushBackUInt64(UInt32 objPtr, ref ulong value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.U8)]
        void CArrayGetElementUInt64(UInt32 objPtr, uint position, ref ulong value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CArrayCreateInstanceFloat();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CArrayDisposeInstanceFloat(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        uint CArrayGetSizeFloat(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayPushBackFloat(UInt32 objPtr, ref float value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.R4)]
        void CArrayGetElementFloat(UInt32 objPtr, uint position, ref float value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CArrayCreateInstanceDouble();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CArrayDisposeInstanceDouble(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        uint CArrayGetSizeDouble(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayPushBackDouble(UInt32 objPtr, ref double value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.R8)]
        void CArrayGetElementDouble(UInt32 objPtr, uint position, ref double value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CArrayCreateInstanceCString();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CArrayDisposeInstanceCString(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        uint CArrayGetSizeCString(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayPushBackCString(UInt32 objPtr, UInt32 value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayGetElementCString(UInt32 objPtr, uint position, UInt32 value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CArrayCreateInstanceCObject();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CArrayDisposeInstanceCObject(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        uint CArrayGetSizeCObject(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayPushBackCObject(UInt32 objPtr, UInt32 value);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        bool CArrayGetElementCObject(UInt32 objPtr, uint position, UInt32 value);

        /*
         * CString Methods
         */

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        UInt32 CStringCreateInstance();

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        void CStringDisposeInstance(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool CStringGetValue(UInt32 objPtr, out UInt32 value, out int length);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        int CStringGetEncoding(UInt32 objPtr);

        [LegacyDllMethod(CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        bool CStringSetValue(UInt32 objPtr, byte[] value, int lenght);



    }



}