﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public interface IDeviceAttributeService : IDisposable
    {
        object GetAttribute(StandardDeviceAttribute attributeId);
        T GetAttribute<T>(StandardDeviceAttribute attributeId);
        IList<object> GetAttributes(IList<short> attributes);
        void SetAttribute<T>(StandardDeviceAttribute attributeId, T value, EncodingType encoding);
        void SetAttribute<T>(StandardDeviceAttribute attributeId, T value);
        void SetAttributes(IList<short> attributeIds, IList<object> values);
    }
}
