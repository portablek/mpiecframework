﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ZaXa.Application.Licensing.Interface;
using ZaXa.Hardware.Motion.Manager;
using ZaXa.Hardware.Motion.Plugins;
using ZaXa.Plugins.Interfaces;

namespace YaskawaPlugin
{
    public class YaskawaPlugin : ZxMcPlugin
    {
        private static readonly IZxLicenseFeatureSet YaskawaFeatureSet = new YaskawaPluginFeatureSet();

        protected override IZxLicenseFeatureSet CreateLicenseFeatureSet()
        {
            return YaskawaFeatureSet;
        }

        protected override IZxPluginInformation CreatePluginInformation(
          Assembly pluginAssembly)
        {
            return new ZxMcPluginInformation(pluginAssembly);
        }

        public override string FriendlyName
        {
            get
            {
                return "Yaskawa Motion Control";
            }
        }

        public override string UniqueKey
        {
            get
            {
                return "Yaskawa";
            }
        }

        protected override IZxMcPluginControllerSet CreateControllerSet(
          IZxMcManager controllerManager)
        {
            return (IZxMcPluginControllerSet)new YaskawaControllerSet(controllerManager);
        }
    }
}