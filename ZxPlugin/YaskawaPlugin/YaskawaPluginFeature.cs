﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZaXa.Application.Licensing.Interface;

namespace YaskawaPlugin
{
    class YaskawaPluginFeature : IZxLicenseFeature
    {
        internal static readonly Guid LicenseFeatureId = new Guid("FA3B8B8E-0EB2-4FC5-B51B-2F9191D2E9A7");
        internal const int LicenseFeatureKey = 1550;

        public string Name
        {
            get
            {
                return "Yaskawa Motion Controller Plugin";
            }
        }

        public string Description
        {
            get
            {
                return "A motion controll plugin supporting the Yaskawa motion controller.";
            }
        }

        public Guid Id => LicenseFeatureId;
        public int FeatureKey => LicenseFeatureKey;
    }
}
