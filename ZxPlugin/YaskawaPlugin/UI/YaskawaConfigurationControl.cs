﻿using MPiecFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YaskawaPlugin;
using ZaXa.Hardware.Motion.Controller.Configuration;

namespace YaskawaPlugin
{
    public partial class YaskawaConfigurationControl : ZxMcControllerConfigurationControl
    {
        private readonly YaskawaController _controller;
        private readonly YaskawaControllerConfiguration _configuration;

        public YaskawaConfigurationControl()
        {
            this.InitializeComponent();
        }

        public YaskawaConfigurationControl(YaskawaController motionController)
          : this()
        {
            this._controller = motionController;
            this._configuration = motionController.Configuration as YaskawaControllerConfiguration;
        }

        public override void StoreToController()
        {
            _configuration.IPAddress = txtIPAddress.Text;
            _configuration.ConfigFile = txtConfigFile.Text;

            base.StoreToController();
        }

        public override void LoadFromController()
        {
            txtIPAddress.Text = _configuration.IPAddress;
            txtConfigFile.Text = _configuration.ConfigFile;

            base.LoadFromController();
        }

        private void btnFindConfig_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtConfigFile.Text = openFileDialog1.FileName;
            }
        }

        private void btnWarmRestart_Click(object sender, EventArgs e)
        {
            if (_controller != null)
            {
                if (MessageBox.Show("Perform a warm restart on the controller?  This will reset all settings to power-on values.",
                    "Restart Yaskawa Controller", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    _controller.WarmRestart();
                }
            }
        }
    }
}
