﻿using System.Windows.Forms;

namespace YaskawaPlugin
{
    public partial class YaskawaAxisConfigurationControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnResetEncoderPosition = new System.Windows.Forms.Button();
            this.txtShortName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLongName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gbOtherSettings = new System.Windows.Forms.GroupBox();
            this.chkAlertOnMovement = new System.Windows.Forms.CheckBox();
            this.chkAllowZero = new System.Windows.Forms.CheckBox();
            this.chkVisible = new System.Windows.Forms.CheckBox();
            this.chkEnabled = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.udConversion = new System.Windows.Forms.NumericUpDown();
            this.cbConversionUnit = new System.Windows.Forms.ComboBox();
            this.gbHoming = new System.Windows.Forms.GroupBox();
            this.cbResetPositionUnit = new System.Windows.Forms.ComboBox();
            this.udResetPositionValue = new System.Windows.Forms.NumericUpDown();
            this.chkResetPositionOnHome = new System.Windows.Forms.CheckBox();
            this.cbHomingMethod = new System.Windows.Forms.ComboBox();
            this.TtMain = new System.Windows.Forms.ToolTip(this.components);
            this.lblMotorType = new System.Windows.Forms.Label();
            this.cbAxisType = new System.Windows.Forms.ComboBox();
            this.udMaxSpeed = new System.Windows.Forms.NumericUpDown();
            this.udAccelleration = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.gbOtherSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udConversion)).BeginInit();
            this.gbHoming.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udResetPositionValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udMaxSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udAccelleration)).BeginInit();
            this.SuspendLayout();
            // 
            // btnResetEncoderPosition
            // 
            this.btnResetEncoderPosition.Location = new System.Drawing.Point(8, 213);
            this.btnResetEncoderPosition.Name = "btnResetEncoderPosition";
            this.btnResetEncoderPosition.Size = new System.Drawing.Size(141, 21);
            this.btnResetEncoderPosition.TabIndex = 26;
            this.btnResetEncoderPosition.Text = "Reset Encoder Position";
            this.btnResetEncoderPosition.UseVisualStyleBackColor = true;
            // 
            // txtShortName
            // 
            this.txtShortName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtShortName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShortName.Location = new System.Drawing.Point(76, 2);
            this.txtShortName.Margin = new System.Windows.Forms.Padding(4);
            this.txtShortName.Name = "txtShortName";
            this.txtShortName.Size = new System.Drawing.Size(73, 20);
            this.txtShortName.TabIndex = 25;
            this.TtMain.SetToolTip(this.txtShortName, "Short Axis Name (3 letters max)");
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 2);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 20);
            this.label1.TabIndex = 24;
            this.label1.Text = "Short Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLongName
            // 
            this.txtLongName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLongName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLongName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLongName.Location = new System.Drawing.Point(241, 2);
            this.txtLongName.Margin = new System.Windows.Forms.Padding(4);
            this.txtLongName.Name = "txtLongName";
            this.txtLongName.Size = new System.Drawing.Size(216, 20);
            this.txtLongName.TabIndex = 28;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(169, 2);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 20);
            this.label2.TabIndex = 27;
            this.label2.Text = "Long Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbOtherSettings
            // 
            this.gbOtherSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbOtherSettings.Controls.Add(this.chkAlertOnMovement);
            this.gbOtherSettings.Controls.Add(this.chkAllowZero);
            this.gbOtherSettings.Controls.Add(this.chkVisible);
            this.gbOtherSettings.Controls.Add(this.chkEnabled);
            this.gbOtherSettings.Location = new System.Drawing.Point(8, 163);
            this.gbOtherSettings.Name = "gbOtherSettings";
            this.gbOtherSettings.Size = new System.Drawing.Size(449, 49);
            this.gbOtherSettings.TabIndex = 29;
            this.gbOtherSettings.TabStop = false;
            this.gbOtherSettings.Text = "Other Settings";
            // 
            // chkAlertOnMovement
            // 
            this.chkAlertOnMovement.AutoSize = true;
            this.chkAlertOnMovement.Location = new System.Drawing.Point(224, 21);
            this.chkAlertOnMovement.Name = "chkAlertOnMovement";
            this.chkAlertOnMovement.Size = new System.Drawing.Size(139, 17);
            this.chkAlertOnMovement.TabIndex = 3;
            this.chkAlertOnMovement.Text = "Alert On Axis Movement";
            this.chkAlertOnMovement.UseVisualStyleBackColor = true;
            // 
            // chkAllowZero
            // 
            this.chkAllowZero.AutoSize = true;
            this.chkAllowZero.Location = new System.Drawing.Point(142, 21);
            this.chkAllowZero.Name = "chkAllowZero";
            this.chkAllowZero.Size = new System.Drawing.Size(76, 17);
            this.chkAllowZero.TabIndex = 2;
            this.chkAllowZero.Text = "Allow Zero";
            this.chkAllowZero.UseVisualStyleBackColor = true;
            // 
            // chkVisible
            // 
            this.chkVisible.AutoSize = true;
            this.chkVisible.Location = new System.Drawing.Point(78, 21);
            this.chkVisible.Name = "chkVisible";
            this.chkVisible.Size = new System.Drawing.Size(56, 17);
            this.chkVisible.TabIndex = 1;
            this.chkVisible.Text = "Visible";
            this.chkVisible.UseVisualStyleBackColor = true;
            // 
            // chkEnabled
            // 
            this.chkEnabled.AutoSize = true;
            this.chkEnabled.Location = new System.Drawing.Point(7, 21);
            this.chkEnabled.Name = "chkEnabled";
            this.chkEnabled.Size = new System.Drawing.Size(65, 17);
            this.chkEnabled.TabIndex = 0;
            this.chkEnabled.Text = "Enabled";
            this.chkEnabled.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, 30);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 20);
            this.label3.TabIndex = 30;
            this.label3.Text = "Conversion";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // udConversion
            // 
            this.udConversion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.udConversion.DecimalPlaces = 14;
            this.udConversion.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.udConversion.Location = new System.Drawing.Point(76, 30);
            this.udConversion.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udConversion.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.udConversion.Name = "udConversion";
            this.udConversion.Size = new System.Drawing.Size(254, 20);
            this.udConversion.TabIndex = 31;
            this.TtMain.SetToolTip(this.udConversion, "Conversion from controller unit to engineering unit.  (Conversion Unit / Yaskawa " +
        "Unit)");
            // 
            // cbConversionUnit
            // 
            this.cbConversionUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbConversionUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConversionUnit.FormattingEnabled = true;
            this.cbConversionUnit.Location = new System.Drawing.Point(336, 30);
            this.cbConversionUnit.Name = "cbConversionUnit";
            this.cbConversionUnit.Size = new System.Drawing.Size(121, 21);
            this.cbConversionUnit.TabIndex = 32;
            // 
            // gbHoming
            // 
            this.gbHoming.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbHoming.Controls.Add(this.cbResetPositionUnit);
            this.gbHoming.Controls.Add(this.udResetPositionValue);
            this.gbHoming.Controls.Add(this.chkResetPositionOnHome);
            this.gbHoming.Controls.Add(this.cbHomingMethod);
            this.gbHoming.Location = new System.Drawing.Point(278, 57);
            this.gbHoming.Name = "gbHoming";
            this.gbHoming.Size = new System.Drawing.Size(179, 100);
            this.gbHoming.TabIndex = 33;
            this.gbHoming.TabStop = false;
            this.gbHoming.Text = "Homing";
            // 
            // cbResetPositionUnit
            // 
            this.cbResetPositionUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbResetPositionUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbResetPositionUnit.FormattingEnabled = true;
            this.cbResetPositionUnit.Location = new System.Drawing.Point(109, 69);
            this.cbResetPositionUnit.Name = "cbResetPositionUnit";
            this.cbResetPositionUnit.Size = new System.Drawing.Size(64, 21);
            this.cbResetPositionUnit.TabIndex = 37;
            this.TtMain.SetToolTip(this.cbResetPositionUnit, "Unit of the reset position");
            // 
            // udResetPositionValue
            // 
            this.udResetPositionValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.udResetPositionValue.DecimalPlaces = 2;
            this.udResetPositionValue.Location = new System.Drawing.Point(6, 70);
            this.udResetPositionValue.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.udResetPositionValue.Minimum = new decimal(new int[] {
            10000000,
            0,
            0,
            -2147483648});
            this.udResetPositionValue.Name = "udResetPositionValue";
            this.udResetPositionValue.Size = new System.Drawing.Size(97, 20);
            this.udResetPositionValue.TabIndex = 36;
            // 
            // chkResetPositionOnHome
            // 
            this.chkResetPositionOnHome.AutoSize = true;
            this.chkResetPositionOnHome.Location = new System.Drawing.Point(16, 47);
            this.chkResetPositionOnHome.Name = "chkResetPositionOnHome";
            this.chkResetPositionOnHome.Size = new System.Drawing.Size(142, 17);
            this.chkResetPositionOnHome.TabIndex = 35;
            this.chkResetPositionOnHome.Text = "Reset Position On Home";
            this.TtMain.SetToolTip(this.chkResetPositionOnHome, "Reset position on home.");
            this.chkResetPositionOnHome.UseVisualStyleBackColor = true;
            // 
            // cbHomingMethod
            // 
            this.cbHomingMethod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbHomingMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbHomingMethod.FormattingEnabled = true;
            this.cbHomingMethod.Items.AddRange(new object[] {
            "None",
            "Seek Zero",
            "Built-In"});
            this.cbHomingMethod.Location = new System.Drawing.Point(6, 19);
            this.cbHomingMethod.Name = "cbHomingMethod";
            this.cbHomingMethod.Size = new System.Drawing.Size(167, 21);
            this.cbHomingMethod.TabIndex = 34;
            this.TtMain.SetToolTip(this.cbHomingMethod, "Homing method.");
            this.cbHomingMethod.SelectedIndexChanged += new System.EventHandler(this.cbHomingMethod_SelectedIndexChanged);
            // 
            // lblMotorType
            // 
            this.lblMotorType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMotorType.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblMotorType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMotorType.Location = new System.Drawing.Point(76, 57);
            this.lblMotorType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMotorType.Name = "lblMotorType";
            this.lblMotorType.Size = new System.Drawing.Size(195, 20);
            this.lblMotorType.TabIndex = 35;
            this.lblMotorType.Text = "Unknown";
            this.lblMotorType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TtMain.SetToolTip(this.lblMotorType, "The type of motor (if known)");
            // 
            // cbAxisType
            // 
            this.cbAxisType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAxisType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAxisType.FormattingEnabled = true;
            this.cbAxisType.Items.AddRange(new object[] {
            "Rotary",
            "Linear",
            "Other"});
            this.cbAxisType.Location = new System.Drawing.Point(76, 87);
            this.cbAxisType.Name = "cbAxisType";
            this.cbAxisType.Size = new System.Drawing.Size(195, 21);
            this.cbAxisType.TabIndex = 37;
            this.TtMain.SetToolTip(this.cbAxisType, "The type of axis.  Affects avaliable conversion units and homing.");
            this.cbAxisType.SelectedIndexChanged += new System.EventHandler(this.cbAxisType_SelectedIndexChanged);
            // 
            // udMaxSpeed
            // 
            this.udMaxSpeed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.udMaxSpeed.Location = new System.Drawing.Point(76, 111);
            this.udMaxSpeed.Name = "udMaxSpeed";
            this.udMaxSpeed.Size = new System.Drawing.Size(195, 20);
            this.udMaxSpeed.TabIndex = 39;
            this.TtMain.SetToolTip(this.udMaxSpeed, "Maximum speed in steps/second");
            // 
            // udAccelleration
            // 
            this.udAccelleration.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.udAccelleration.Location = new System.Drawing.Point(76, 137);
            this.udAccelleration.Name = "udAccelleration";
            this.udAccelleration.Size = new System.Drawing.Size(195, 20);
            this.udAccelleration.TabIndex = 41;
            this.TtMain.SetToolTip(this.udAccelleration, "Maximum accelleration and decelleration in steps/second/second");
            this.udAccelleration.Visible = false;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(5, 57);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 20);
            this.label4.TabIndex = 34;
            this.label4.Text = "Motor Type";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(4, 87);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 20);
            this.label5.TabIndex = 36;
            this.label5.Text = "Axis Type";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(5, 111);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 20);
            this.label6.TabIndex = 38;
            this.label6.Text = "Max Speed";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(5, 137);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 20);
            this.label7.TabIndex = 40;
            this.label7.Text = "Acceleration";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Visible = false;
            // 
            // YaskawaAxisConfigurationControl
            // 
            this.Controls.Add(this.cbAxisType);
            this.Controls.Add(this.udAccelleration);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.udMaxSpeed);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnResetEncoderPosition);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblMotorType);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gbHoming);
            this.Controls.Add(this.cbConversionUnit);
            this.Controls.Add(this.udConversion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.gbOtherSettings);
            this.Controls.Add(this.txtLongName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtShortName);
            this.Controls.Add(this.label1);
            this.Name = "YaskawaAxisConfigurationControl";
            this.Size = new System.Drawing.Size(461, 234);
            this.gbOtherSettings.ResumeLayout(false);
            this.gbOtherSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udConversion)).EndInit();
            this.gbHoming.ResumeLayout(false);
            this.gbHoming.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udResetPositionValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udMaxSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udAccelleration)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnResetEncoderPosition;
        private System.Windows.Forms.TextBox txtShortName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtLongName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gbOtherSettings;
        private System.Windows.Forms.CheckBox chkAlertOnMovement;
        private System.Windows.Forms.CheckBox chkAllowZero;
        private System.Windows.Forms.CheckBox chkVisible;
        private System.Windows.Forms.CheckBox chkEnabled;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown udConversion;
        private System.Windows.Forms.ComboBox cbConversionUnit;
        private System.Windows.Forms.GroupBox gbHoming;
        private System.Windows.Forms.NumericUpDown udResetPositionValue;
        private System.Windows.Forms.CheckBox chkResetPositionOnHome;
        private System.Windows.Forms.ComboBox cbHomingMethod;
        private System.Windows.Forms.ComboBox cbResetPositionUnit;
        private System.Windows.Forms.ToolTip TtMain;
        private Label label4;
        private Label lblMotorType;
        private Label label5;
        private ComboBox cbAxisType;
        private Label label6;
        private NumericUpDown udMaxSpeed;
        private NumericUpDown udAccelleration;
        private Label label7;
    }
}
