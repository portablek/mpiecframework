﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZaXa.Hardware.Motion.Axis;
using ZaXa.Hardware.Motion.Axis.Configuration;
using ZaXa.Logging.Global;

namespace YaskawaPlugin
{
    public partial class YaskawaAxisConfigurationControl : ZxMcAxisConfigurationControl
    {
        private readonly YaskawaAxis _axis;
        private readonly YaskawaAxisConfiguration _configuration;
        private readonly List<HomingMethodItem> HomingItems = new List<HomingMethodItem>()
        {
            new HomingMethodItem("None", YaskawaHomingMethod.None),
            new HomingMethodItem("Seek Zero", YaskawaHomingMethod.SeekZero),
            new HomingMethodItem("Built-In", YaskawaHomingMethod.Builtin)
        };
        private readonly List<string> AxisTypes = new List<string>()
        {
            "Rotary", "Linear", "Other"
        };

        public YaskawaAxisConfigurationControl()
        {
            this.InitializeComponent();

            udMaxSpeed.Minimum = Decimal.Zero;

            cbAxisType.Items.Clear();
            cbAxisType.Items.AddRange(AxisTypes.ToArray());

            cbHomingMethod.Items.Clear();
            cbHomingMethod.Items.AddRange(HomingItems.Select(i => i.Name).ToArray());
        }

        public YaskawaAxisConfigurationControl(YaskawaAxis axis)
          : base((IZxMcAxis)axis)
        {
            this.InitializeComponent();

            udMaxSpeed.Minimum = Decimal.Zero;

            _axis = axis;
            _configuration = axis.Configuration as YaskawaAxisConfiguration;
        }

        public override void StoreToAxis()
        {
            _axis.NativeConfiguration.ShortName = txtShortName.Text;
            _axis.NativeConfiguration.LongName = txtLongName.Text;

            ZxMcAxisType axisType;
            Enum.TryParse<ZxMcAxisType>(cbAxisType.SelectedItem.ToString(), out axisType);
            _axis.NativeConfiguration.AxisType = axisType;

            HomingMethodItem homeMethod = HomingItems.Where(i => i.Name.Equals(cbHomingMethod.SelectedItem)).FirstOrDefault();            
            _configuration.HomingMethod = (homeMethod != null) ? homeMethod.Method : YaskawaHomingMethod.None;
            
            _configuration.ConversionValue = (double)udConversion.Value;
            _configuration.ConversionValueUnit = GetSelectedComboboxConversionUnit(cbConversionUnit);
            _configuration.ResetOnHome = chkResetPositionOnHome.Checked;
            _configuration.ResetOnHomeUnit = GetSelectedComboboxConversionUnit(cbResetPositionUnit);
            _configuration.Speed = (ulong)udMaxSpeed.Value;
            _configuration.Acceleration = (ulong)udAccelleration.Value;

            _configuration.Enabled = chkEnabled.Checked;
            _configuration.Visible = chkVisible.Checked;
            _configuration.AllowZero = chkAllowZero.Checked;
            _configuration.AlertOnMovement = chkAlertOnMovement.Checked;
        }

        public override void LoadFromAxis()
        {
            txtShortName.Text = _axis.NativeConfiguration.ShortName;
            txtLongName.Text = _axis.NativeConfiguration.LongName;
            lblMotorType.Text = _axis.MotorType.ToString();
            ZxMcAxisType aType = _axis.AxisType;
            cbAxisType.SelectedIndex = (int)_axis.AxisType;
            udConversion.Value = (Decimal)_axis.Units.ConversionUnit.Multiplier;
            PopulateComboboxConversionUnits(cbConversionUnit, _axis.AxisType, _axis.ConversionUnit);
            HomingMethodItem homing = HomingItems.Where(i => i.Method == _axis.HomingMethod).FirstOrDefault();
            cbHomingMethod.SelectedItem = homing == null ? HomingItems[0].Name : homing.Name;
            chkResetPositionOnHome.Checked = _axis.ConfiguredResetOnHome;
            udResetPositionValue.Value = (Decimal)_axis.ResetOnHomePosition.Value;
            PopulateComboboxConversionUnits(cbResetPositionUnit, _axis.AxisType, _axis.ConfiguredResetOnHomeUnit);

            switch (_axis.MotorType)
            {
                case ZxMcMotorType.Servo:
                    udMaxSpeed.Maximum = (Decimal)12000000;
                    break;
                case ZxMcMotorType.Stepper:
                case ZxMcMotorType.Other:
                case ZxMcMotorType.Unknown:
                    udMaxSpeed.Maximum = (Decimal)3000000;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
                }

            Decimal configuredMaximumSpeed = (Decimal)_axis.ConfiguredMaximumSpeed;
            Decimal num1 = configuredMaximumSpeed < udMaxSpeed.Minimum ? udMaxSpeed.Minimum : configuredMaximumSpeed;
            this.udMaxSpeed.Value = num1 > udMaxSpeed.Maximum ? udMaxSpeed.Maximum : num1;
            Decimal maximumAcceleration = (Decimal)_axis.ConfiguredMaximumAcceleration;
            Decimal num2 = maximumAcceleration < udAccelleration.Minimum ? udAccelleration.Minimum : maximumAcceleration;
            this.udAccelleration.Value = num2 > udAccelleration.Maximum ? udAccelleration.Maximum : num2;

            this.chkEnabled.Checked = _axis.Enabled;
            this.chkVisible.Checked = _axis.Visible;
            this.chkAllowZero.Checked = _axis.ConfiguredAllowZero;
            this.chkAlertOnMovement.Checked =_axis.AlertOnMovement;
        }

        private void cbHomingMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cbHomingMethod.SelectedItem is HomingMethodItem selectedItem && selectedItem.Method == YaskawaHomingMethod.Builtin)
            {
                chkResetPositionOnHome.Enabled = true;
                udResetPositionValue.Enabled = true;
                cbResetPositionUnit.Enabled = true;
            }
            else
            {
                chkResetPositionOnHome.Enabled = false;
                udResetPositionValue.Enabled = false;
                cbResetPositionUnit.Enabled = false;
            }
        }

        private void cbAxisType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._axis == null)
                return;
            ZxMcAxisType result;
            Enum.TryParse<ZxMcAxisType>(cbAxisType.SelectedItem.ToString(), out result);
            PopulateComboboxConversionUnits(cbConversionUnit, result, _axis.ConversionUnit);
            PopulateComboboxConversionUnits(cbResetPositionUnit, result, _axis.ConfiguredResetOnHomeUnit);
        }
    }

    class HomingMethodItem
    {
        public string Name { get; private set; }
        public YaskawaHomingMethod Method { get; private set; }

        public HomingMethodItem(string name, YaskawaHomingMethod method)
        {
            this.Name = name;
            this.Method = method;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
