﻿namespace YaskawaPlugin
{
    public partial class YaskawaConfigurationControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtIPAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtConfigFile = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnFindConfig = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnWarmRestart = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtIPAddress
            // 
            this.txtIPAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIPAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIPAddress.Location = new System.Drawing.Point(115, 6);
            this.txtIPAddress.Margin = new System.Windows.Forms.Padding(4);
            this.txtIPAddress.Name = "txtIPAddress";
            this.txtIPAddress.Size = new System.Drawing.Size(236, 32);
            this.txtIPAddress.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 20);
            this.label1.TabIndex = 19;
            this.label1.Text = "IP Address";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtConfigFile
            // 
            this.txtConfigFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConfigFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfigFile.Location = new System.Drawing.Point(115, 34);
            this.txtConfigFile.Margin = new System.Windows.Forms.Padding(4);
            this.txtConfigFile.Name = "txtConfigFile";
            this.txtConfigFile.Size = new System.Drawing.Size(236, 32);
            this.txtConfigFile.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 36);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 20);
            this.label2.TabIndex = 21;
            this.label2.Text = "MP Configuration File";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnFindConfig
            // 
            this.btnFindConfig.Location = new System.Drawing.Point(367, 32);
            this.btnFindConfig.Name = "btnFindConfig";
            this.btnFindConfig.Size = new System.Drawing.Size(80, 21);
            this.btnFindConfig.TabIndex = 23;
            this.btnFindConfig.Text = "Browse...";
            this.btnFindConfig.UseVisualStyleBackColor = true;
            this.btnFindConfig.Click += new System.EventHandler(this.btnFindConfig_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "mpc";
            this.openFileDialog1.Filter = "Controller Configuration (*.mpc)|*.mpc|All Files (*.*)|*.*";
            this.openFileDialog1.Title = "Load Controller Configuration From...";
            // 
            // btnWarmRestart
            // 
            this.btnWarmRestart.Location = new System.Drawing.Point(12, 195);
            this.btnWarmRestart.Name = "btnWarmRestart";
            this.btnWarmRestart.Size = new System.Drawing.Size(162, 25);
            this.btnWarmRestart.TabIndex = 24;
            this.btnWarmRestart.Text = "Restart Controller...";
            this.btnWarmRestart.UseVisualStyleBackColor = true;
            this.btnWarmRestart.Click += new System.EventHandler(this.btnWarmRestart_Click);
            // 
            // YaskawaConfigurationControl
            // 
            this.Controls.Add(this.btnWarmRestart);
            this.Controls.Add(this.btnFindConfig);
            this.Controls.Add(this.txtConfigFile);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtIPAddress);
            this.Controls.Add(this.label1);
            this.Name = "YaskawaConfigurationControl";
            this.Size = new System.Drawing.Size(461, 234);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtIPAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtConfigFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnFindConfig;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnWarmRestart;
    }
}
