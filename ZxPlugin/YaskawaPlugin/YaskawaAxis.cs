﻿using MPiecFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ZaXa.Hardware.Motion;
using ZaXa.Hardware.Motion.Axis;
using ZaXa.Hardware.Motion.Axis.Configuration;
using ZaXa.Hardware.Motion.Units.Base;
using ZaXa.Hardware.Motion.Units.Cache;
using ZaXa.Hardware.Motion.Units.Core;
using ZaXa.Logging.Global;

namespace YaskawaPlugin
{
    public class YaskawaAxis : ZxMcAxis<YaskawaController>
    {
        private const string Cat = "PK:PLG:YKCTRL:AX";

        private const float JogEpsilon = 0.0005f;
        private readonly YaskawaAxisConfiguration _configuration;
        private readonly YaskawaAxisTechnique _technique;
        private YaskawaController _controller;
        private MPiecAxis _axisConfiguration;
        private double LastTargetPosition;
        public bool LastTargetNearlyZero => (LastTargetPosition > -1E-9) && (LastTargetPosition < 1E-9);


        public YaskawaAxis(YaskawaController controller, ZxMcAxisId id, ZxMcConfigurations configuration)
          : base(id, controller)
        {
            this._controller = controller;
            this._configuration = new YaskawaAxisConfiguration(this, configuration.Configuration);
            this._technique = new YaskawaAxisTechnique(this, configuration.Technique);
            SetAxisConfiguration(controller == null ? null : controller.GetAxisConfig(id));
        }

        public void SetAxisConfiguration(MPiecAxis AxisConfiguration)
        {
            _axisConfiguration = AxisConfiguration;
            _configuration.LongName = (_axisConfiguration == null) ? "" : _axisConfiguration.AxisName;
        }

        public void ClearForMovement()
        {
            _controller.SetVariables(new List<MPiecVariable>() { 
                _axisConfiguration.StopMotion, _axisConfiguration.GoToTarget, _axisConfiguration.GoToHome, _axisConfiguration.JogForward, _axisConfiguration.JogReverse
                }, false);
        }


        public override bool Operable
        {
            get
            {
                return this.NativeParentController.Operable;
            }
        }

        public override bool Enabled
        {
            get
            {
                if (_controller.IsVirtual)
                    return true;

                if (_axisConfiguration.isEnabled != null)
                    return _controller.ReadBoolVariable(_axisConfiguration.isEnabled, "isEnabled");

                return base.Enabled;
            }
            protected set
            {
                if (_axisConfiguration.SetEnabled != null)
                    _controller.WriteVariable(_axisConfiguration.SetEnabled, value);

                base.Enabled = value;
            }
        }

        public override bool Commandable
        {
            get
            {
                return this.Operable && this.Enabled;
            }
        }

        public override bool SafelyCommandable
        {
            get
            {
                return this.Commandable && !this.Aborted && !this.IsHot;
            }
        }

        public override bool Aborted
        {
            get
            {
                return this.NativeParentController.Aborted;
            }
        }

        public YaskawaAxisConfiguration NativeConfiguration
        {
            get
            {
                return this._configuration;
            }
        }
        public bool AlertOnMovement { get; internal set; }

        public YaskawaHomingMethod HomingMethod { get; internal set; }

        public bool isAtTarget()
        {
            if (!this.Operable)
                return false;

            double curPosition = _controller.ReadVariable(_axisConfiguration.CurrentPosition, "currentPosition");
            //bool atTarget = _controller.ReadBoolVariable(_axisConfiguration.isAtTarget, "isAtTarget");
            bool atTarget = Math.Abs(curPosition - LastTargetPosition) <= JogEpsilon;
            if (atTarget)
            {
                if (_axisConfiguration.GoToTarget != null)
                    _controller.WriteVariable(_axisConfiguration.GoToTarget, false, "GoToTarget");
            }

            return atTarget;
        }

        public bool isAtHome()
        {
            if (!this.Operable)
                return false;

            switch (this.HomingMethod)
            {
                case YaskawaHomingMethod.None:
                    return false;
                case YaskawaHomingMethod.SeekZero:
                    return LastTargetNearlyZero && isAtTarget();
                case YaskawaHomingMethod.Builtin:
                    bool atHome = _controller.ReadBoolVariable(_axisConfiguration.isAtHome, "isAtHome");
                    if (atHome)
                        _controller.WriteVariable(_axisConfiguration.GoToHome, false, "GoToHome");
                    return atHome;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private bool HomingStarted;
        public override bool Homing
        {
            get
            {
                if (!HomingStarted)
                    return false;

                bool isHoming = false;
                switch (this.HomingMethod)
                {
                    case YaskawaHomingMethod.None:
                        isHoming = false;
                        break;
                    case YaskawaHomingMethod.SeekZero:
                    case YaskawaHomingMethod.Builtin:
                        isHoming = !isAtHome();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                HomingStarted = !isHoming;
                return isHoming;
            }
        }

        public virtual bool IsHot
        {
            get
            {
                if (_axisConfiguration.isHot == null)
                    return false;

                bool ThermAlert = _controller.ReadBoolVariable(_axisConfiguration.isHot);
                if (ThermAlert)
                    Log.W(Cat, "Axis \"" + this.LongNameWithController + "\" is over temperature!");
                return ThermAlert;
            }
        }

        public override bool Moving
        {
            get
            {
                if (IsHot)
                    return false;

                return _controller.ReadBoolVariable(_axisConfiguration.isMoving, "isMoving");
            }
        }

        public override bool ForwardLimitSwitchActive
        {
            get
            {
                return _controller.ReadBoolVariable(_axisConfiguration.isAlarmed, "isAlarmed");
            }
        }

        public override bool ReverseLimitSwitchActive
        {
            get
            {
                return _controller.ReadBoolVariable(_axisConfiguration.isAlarmed, "isAlarmed");
            }
        }

        public override bool HomeSwitchActive
        {
            get
            {
                return _controller.ReadBoolVariable(_axisConfiguration.isAtHome, "isAtHome");
            }
        }

        public override ZxMcMotorType MotorType
        {
            get
            {
                return ZxMcMotorType.Servo;
            }
        }

        public override double HardwarePosition
        {
            get
            {
                //Log.D(Cat, "Getting " + _axisConfiguration.AxisName + " Hardware Position");
                return !this.Operable ? 0.0 : _controller.ReadVariable(_axisConfiguration.CurrentPosition, "CurrentPosition");
            }
        }

        public override void SeekAbsolute(double value)
        {
            if (!this.SafelyCommandable)
                return;

            if (value == LastTargetPosition)
                return;

            try
            {
                //Log.I(Cat, "Seeking " + value + " on " + this.LongNameWithController);
                ClearForMovement();

                if (!_controller.WriteVariable(_axisConfiguration.TargetPosition, value, "TargetPosition"))
                    Log.E(Cat, "Unable to seek position " + value + " on " + LongNameWithController);
                else
                {
                    LastTargetPosition = value;

                    if (_axisConfiguration.GoToTarget != null)
                        _controller.WriteVariable(_axisConfiguration.GoToTarget, true, "GoToTarget");

                    WaitForMotionStarted(500);
                }
            }
            catch (Exception ex)
            {
                Log.W(Cat, "Error waiting for motion to complete: " + ex.Message);
            }
        }

        public override void SeekAbsolute(ZxMcAxisValue value)
        {
            //Log.I(Cat, "Seeking to ZxMcAxisValue: " + value.Value + " " + value.Unit);
            this.SeekAbsolute(value.ToHardwareValue);
        }

        public override void SeekAbsolute(double value, double speed)
        {
            this.SeekAbsolute(value);

            if (!this.SafelyCommandable)
                return;

            this.CurrentSpeed = speed;
            this.SeekRelative(value);
        }

        public override void SeekAbsolute(ZxMcAxisValue value, double speed)
        {
            this.SeekAbsolute(value.ToHardwareValue, speed);
        }

        public override void SeekRelative(double value)
        {
            if (!this.SafelyCommandable)
                return;
            this.SeekAbsolute(value + this.HardwarePosition);
        }

        public override void SeekRelative(ZxMcAxisValue value)
        {
            this.SeekRelative(value.ToHardwareValue);
        }

        public override void SeekRelative(double value, double speed)
        {
            if (!this.SafelyCommandable)
                return;
            this.CurrentSpeed = speed;
            this.SeekRelative(value);
        }

        public override void SeekRelative(ZxMcAxisValue value, double speed)
        {
            this.SeekRelative(value.ToHardwareValue, speed);
        }

        public override bool CanSetSpeed
        {
            get
            {
                return (_axisConfiguration.SetSpeed != null);
            }
        }

        public override double CurrentSpeed
        {
            get
            {
                return _controller.ReadVariable(_axisConfiguration.CurrentSpeed, "CurrentSpeed");
            }
            set
            {
                if (!this.SafelyCommandable)
                    return;

                value = value < this.MinSpeed ? this.MinSpeed : value;
                value = value > this.MaxSpeed ? this.MaxSpeed : value;
                _controller.WriteVariable(_axisConfiguration.CurrentSpeed, value, "CurrentSpeed");
            }
        }

        public override bool CanSeek
        {
            get
            {
                return this.Operable && (_axisConfiguration.TargetPosition != null);
            }
        }

        public override bool CanJog
        {
            get
            {
                return this.Operable && (_axisConfiguration.JogForward != null) && (_axisConfiguration.JogReverse != null);
            }
        }

        public override bool CanHome
        {
            get
            {
                return this.Operable && (this.HomingMethod != YaskawaHomingMethod.None);
            }
        }

        public override bool CanZero
        {
            get
            {
                return this.ConfiguredAllowZero;
            }
        }

        public override void JogAt(double speed)
        {
            if (!this.SafelyCommandable || Math.Abs(speed) < 9.88131291682493E-324)
                return;

            if (speed < 0)
                speed = -1.0 * speed;
            if (speed > this.ConfiguredMaximumSpeed)
                speed = this.ConfiguredMaximumSpeed;

            if (_axisConfiguration.jogSpeed != null)
                _controller.WriteVariable(_axisConfiguration.jogSpeed, speed, "JogSpeed");
            else
                _controller.SetJogSpeed(speed);
        }

        public override void Jog(float magnitude)
        {
            if (!this.SafelyCommandable)
                return;

            JogAt(magnitude * this.ConfiguredMaximumSpeed);

            if (magnitude > 0.000500000023748726)
            {
                _controller.WriteVariable(_axisConfiguration.JogReverse, false, "JogReverse");
                _controller.WriteVariable(_axisConfiguration.JogForward, true, "JogForward");
            }
            else if (magnitude < -0.000500000023748726)
            {
                _controller.WriteVariable(_axisConfiguration.JogForward, false, "JogForward");
                _controller.WriteVariable(_axisConfiguration.JogReverse, true, "JogReverse");
            }
            else
            {
                _controller.WriteVariable(_axisConfiguration.JogForward, false, "JogForward");
                _controller.WriteVariable(_axisConfiguration.JogReverse, false, "JogReverse");
            }
        }

        public override void Stop(bool decelerate = true)
        {
            if (!this.Commandable)
                return;

            _controller.WriteVariable(_axisConfiguration.StopMotion, "true", "StopMotion");
            Task.Delay(25).Wait();
            _controller.WriteVariable(_axisConfiguration.StopMotion, "false", "StopMotion");
        }

        public override void Home()
        {
            if (!this.SafelyCommandable)
                return;

            ClearForMovement();

            switch (this.HomingMethod)
            {
                case YaskawaHomingMethod.SeekZero:
                    this.SeekAbsolute(0.0);
                    HomingStarted = true;
                    break;
                case YaskawaHomingMethod.Builtin:
                    if (_controller.WriteVariable(_axisConfiguration.GoToHome, true, "GoToHome"))
                    {
                        HomingStarted = true;
                        Task.Delay(25).Wait();
                        _controller.WriteVariable(_axisConfiguration.GoToHome, false, "GoToHome");
                    }
                    else
                        Log.E(Cat, "Unable to home axis " + LongNameWithController);
                    break;
            }
        }

        public override void Zero()
        {
            if (!this.SafelyCommandable || !this.ConfiguredAllowZero)
                return;

            this.Stop(true);
            ClearForMovement();
        }

        public override bool SupportsRangeLimits
        {
            get
            {
                return this.AxisType == ZxMcAxisType.Linear;
            }
        }

        public bool WaitForMotionStarted(int timeoutMs)
        {
            try
            {
                for (timeoutMs = timeoutMs <= 0 ? int.MaxValue : timeoutMs; !this.Moving && timeoutMs > 0; timeoutMs -= 10)
                    Thread.Sleep(10);
                Log.I(Cat, "Motion " + (this.Moving ? "started: " : "start timed out: ")  + this.LongNameWithController);
            }
            catch (Exception ex)
            {
                Log.W(Cat, "Error waiting for motion to complete: " + ex.Message);
            }

            return !Moving;
        }

        public override bool WaitForMotionComplete(int timeoutMs)
        {
            try
            {
                Log.I(Cat, "Waiting for " + LastTargetPosition + " on " + this.LongNameWithController);
                for (timeoutMs = timeoutMs <= 0 ? int.MaxValue : timeoutMs; this.Moving && timeoutMs > 0; timeoutMs -= 10)
                    Thread.Sleep(10);
                Log.I(Cat, "Motion " + (this.Moving ? " timed out: " : "complete: ") + this.LongNameWithController);
            } 
            catch (Exception ex)
            {
                Log.W(Cat, "Error waiting for motion to complete: " + ex.Message);
            }

            return !this.Moving;
        }

        public override bool WaitForHomingComplete(int timeoutMs)
        {
            if (!this.Operable)
                return false;

            for (timeoutMs = timeoutMs <= 0 ? int.MaxValue : timeoutMs; this.Homing && timeoutMs > 0; timeoutMs -= 10)
                Thread.Sleep(10);
            return !this.Homing;
        }

        public override void ApplyConfigurationChanges()
        {
            this.AlertOnMovement = this._configuration.AlertOnMovement;
            this.HomingMethod = this._configuration.HomingMethod;
            base.ApplyConfigurationChanges();
        }

        public override void RevertConfigurationChanges()
        {
            this._configuration.AlertOnMovement = this.AlertOnMovement;
            this._configuration.HomingMethod = this.HomingMethod;
            base.RevertConfigurationChanges();
        }

        public override ZxMcAxisConfigurationControl ConfigurationControl
        {
            get
            {
                return (ZxMcAxisConfigurationControl)new YaskawaAxisConfigurationControl(this);
            }
        }

        public override ZxMcAxisConfigurationAdapter Configuration
        {
            get
            {
                return (ZxMcAxisConfigurationAdapter)this._configuration;
            }
        }

        public override ZxMcAxisTechniqueControl TechniqueControl
        {
            get
            {
                return new ZxMcAxisTechniqueControl();
            }
        }

        public override ZxMcAxisTechniqueAdapter Technique
        {
            get
            {
                return (ZxMcAxisTechniqueAdapter)this._technique;
            }
        }

        protected override IZxAxisUnitCache InitializeUnitCache(ZxMcCoreUnit conversionUnit, double conversionValue)
        {
            return (IZxAxisUnitCache)new YaskawaUnitCache((IZxMcAxis)this, conversionUnit, conversionValue, new ZxEngineeringAxisUnit[0]);
        }

        public override void ControllerConnected()
        {
            base.ControllerConnected();
        }

        public override void ControllerDisconnected()
        {
            base.ControllerDisconnected();
        }

    }
}
