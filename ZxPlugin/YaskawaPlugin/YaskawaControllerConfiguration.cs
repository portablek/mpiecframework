﻿using MPiecFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZaXa.Hardware.Motion.Controller;
using ZaXa.Hardware.Motion.Controller.Configuration;
using ZaXa.Logging.Global;
using ZaXa.Tools.OmniConfig;

namespace YaskawaPlugin
{
    public class YaskawaControllerConfiguration : ZxMcControllerConfigurationAdapter
    {
        private const string Cat = "PK:PLG:YKCTRL:CFG";

        private static readonly ZxOmniConfigAdapter.ZxTok<string> IPAddressToken = Tok<string>(nameof(IPAddress), "The IPv4 address of the controller in dot notation (192.168.0.1, etc).", "");
        private static readonly ZxOmniConfigAdapter.ZxTok<string> ConfigFileToken = Tok<string>(nameof(ConfigFile), "Controller Configuration File (*.mpc)", ""); 
        private const string YaskawaDefaultCategory = "Yaskawa";

        public YaskawaControllerConfiguration(
          YaskawaController controller,
          IZxOmniConfig omniConfig)
          : base((IZxMcController)controller, omniConfig)
        {
        }

        protected override string DefaultCategory
        {
            get
            {
                return YaskawaDefaultCategory;
            }
        }

        protected override string DefaultUserDescription
        {
            get
            {
                return "Yaskawa Motion Controller";
            }
        }

        public string IPAddress
        {
            get
            {
                return GetValue(IPAddressToken);
            }
            set
            {
                SetValue<string>(IPAddressToken, value);
            }
        }

        public string ConfigFile
        {
            get
            {
                return GetValue(ConfigFileToken);
            }
            set
            {
                SetValue<string>(ConfigFileToken, value);

                try
                {
                    MPiecConfiguration controllerConfig = MPiecConfiguration.Load(value);
                    this.AxisCount = controllerConfig.Axes.Count();
                }
                catch (Exception ex)
                {
                    Log.E(Cat, "Error reading Controller Configuration File: " + ex.Message);
                }
            }
        }
    }
}
