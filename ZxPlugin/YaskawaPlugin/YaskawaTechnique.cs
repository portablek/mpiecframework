﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZaXa.Hardware.Motion.Axis;
using ZaXa.Hardware.Motion.Axis.Configuration;
using ZaXa.Tools.OmniConfig;

namespace YaskawaPlugin
{
    public class YaskawaTechnique : ZxMcAxisTechniqueAdapter
    {
        private const string GalilDefaultCategory = "Yaskawa Axis";

        public YaskawaTechnique(YaskawaAxis axis, IZxOmniConfig omniConfig)
          : base((IZxMcAxis)axis, omniConfig)
        {
        }

        protected override string DefaultCategory
        {
            get
            {
                return "Yaskawa Axis";
            }
        }
    }
}
