﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZaXa.Hardware.Motion.Controller;
using ZaXa.Hardware.Motion.Controller.Configuration;
using ZaXa.Tools.OmniConfig;

namespace YaskawaPlugin
{
    public class YaskawaControllerTechnique : ZxMcControllerTechniqueAdapter
    {
        private readonly YaskawaController _controller;

        public YaskawaControllerTechnique(YaskawaController controller, IZxOmniConfig omniConfig)
          : base((IZxMcController)controller, omniConfig)
        {
            this._controller = controller;
        }
    }
}
