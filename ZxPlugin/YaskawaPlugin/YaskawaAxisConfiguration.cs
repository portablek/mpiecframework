﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ZaXa.Hardware.Motion.Axis;
using ZaXa.Hardware.Motion.Axis.Configuration;
using ZaXa.Tools.OmniConfig;

namespace YaskawaPlugin
{
    public class YaskawaAxisConfiguration : ZxMcAxisConfigurationAdapter
    {
        private static readonly ZxOmniConfigAdapter.ZxTok<bool> AlertOnMovementToken = ZxOmniConfigAdapter.Tok<bool>(nameof(AlertOnMovement), "On supported systems, enable alarm sounding when this axis is in motion.", false);
        private static readonly ZxOmniConfigAdapter.ZxTok<YaskawaHomingMethod> HomingMethodToken = ZxOmniConfigAdapter.Tok<YaskawaHomingMethod>(nameof(HomingMethod), "The algorithm to be used when the axis is asked to find its home location.", YaskawaHomingMethod.None);
        private const string YaskawaDefaultCategory = "Yaskawa Axis";

        public YaskawaAxisConfiguration(YaskawaAxis axis, IZxOmniConfig config)
          : base((IZxMcAxis)axis, config)
        {
        }

        protected override string DefaultCategory
        {
            get
            {
                return YaskawaDefaultCategory;
            }
        }

        public bool AlertOnMovement
        {
            get
            {
                return this.GetValueEnum<bool>(AlertOnMovementToken);
            }
            set
            {
                this.SetValueEnum<bool>(AlertOnMovementToken, value);
            }
        }

        public YaskawaHomingMethod HomingMethod
        {
            get
            {
                return this.GetValueEnum<YaskawaHomingMethod>(HomingMethodToken);
            }
            set
            {
                this.SetValueEnum<YaskawaHomingMethod>(HomingMethodToken, value);
            }
        }

        protected override double SpeedDefaultBaseUnitPerSecond
        {
            get
            {
                return 200000.0;
            }
        }

        protected override double AccelerationDefaultBaseUnitPerSecondSquared
        {
            get
            {
                return 800000.0;
            }
        }
    }
}
