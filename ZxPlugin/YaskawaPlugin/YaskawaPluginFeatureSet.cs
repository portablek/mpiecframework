﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZaXa.Application.Licensing.Interface;

namespace YaskawaPlugin
{
    internal class YaskawaPluginFeatureSet : ZxLicenseFeatureSet
    {
        public YaskawaPluginFeatureSet() : base(new YaskawaPluginFeature())
        {
        }
    }
}
