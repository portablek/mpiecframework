﻿using MPiecConnectLink;
using MPiecFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZaXa.Hardware.Motion;
using ZaXa.Hardware.Motion.Axis;
using ZaXa.Hardware.Motion.Controller;
using ZaXa.Hardware.Motion.Controller.Configuration;
using ZaXa.Hardware.Motion.Manager;
using ZaXa.Logging.Global;

namespace YaskawaPlugin
{
    public class YaskawaController : ZxMcController<YaskawaController>
    {
        private const string Cat = "PK:PLG:YKCTRL";
        private readonly YaskawaControllerConfiguration _configuration;
        private readonly YaskawaControllerTechnique _technique;
        private MPiecLink _controllerLink;
        private MPiecConfiguration _controllerConfig;

        public string IPAddress { get; private set; }
        public string ConfigFile { get; private set; }

        public YaskawaController(
          ZxMcControllerId id,
          IZxMcManager controllerManager,
          ZxMcConfigurations configurations)
          : base("Yaskawa", id, controllerManager, configurations)
        {
            this._configuration = new YaskawaControllerConfiguration(this, configurations.Configuration);
            this._technique = new YaskawaControllerTechnique(this, configurations.Technique);
            if (String.IsNullOrEmpty(_configuration.ConfigFile))
                _controllerConfig = null;
            else
                _controllerConfig = MPiecConfiguration.Load(_configuration.ConfigFile);
        }

        public void WarmRestart()
        {
            if ((_controllerLink != null) && (_controllerLink.HasConnected()))
            {
                _controllerLink.GetCommandResponse("reboot");
            }
        }

        protected override Guid LicenseFeatureId
        {
            get
            {
                return YaskawaPluginFeature.LicenseFeatureId;
            }
        }

        public override bool Operable
        {
            get
            {
                return ((_controllerLink != null) && _controllerLink.HasConnected());
            }
        }

        public override bool Commandable
        {
            get
            {
//                Log.D(Cat, "Checking Yaskawa Commandable");
                bool val = this.Operable && this.Enabled;
//                Log.D(Cat, "Yaskawa Commandable: " + val);
                return val;
            }
        }

        public override bool SafelyCommandable
        {
            get
            {
                return this.Commandable && !this.Aborted;
            }
        }

        protected override ZxMcAxis<YaskawaController> CreateAxis(
          ZxMcAxisId id, ZxMcConfigurations configSection)
        {
            return (ZxMcAxis<YaskawaController>)new YaskawaAxis(this, id, configSection);
        }

        public override string HardwareDescription
        {
            get
            {
                if (!this.Operable)
                    return "";

                try
                {
                    return _controllerLink.GetCommandResponse("manufacturer") + " " + _controllerLink.GetCommandResponse("product");
                }
                catch
                {
                    return "";
                }
            }
        }

        public override string DriverDescription
        {
            get
            {
                return "PLCi via MPiecFramework";
            }
        }

        public override string HardwareSummary
        {
            get
            {
                return this.HardwareDescription;
            }
        }

        public override bool Connect()
        {
            return doConnect(true);
        }

        public bool doConnect(bool doRetry)
        {
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            path = System.IO.Path.GetDirectoryName(path);
            string ConnectPath = System.IO.Path.Combine(path, MPiecLink.ExecutableName);

            if (this.State == ZxMcControllerState.Connected)
                return true;
            try
            {
                Log.D(Cat, "Connecting to Yaskawa");

                if (!this.CheckLicense())
                {
                    this.SetState(ZxMcControllerState.Unlicensed, "Not permitted by current license.");
                    return false;
                }

                if (!string.IsNullOrEmpty(_configuration.ConfigFile))
                {
                    _controllerConfig = MPiecConfiguration.Load(_configuration.ConfigFile);

                    if (_controllerLink != null)
                        _controllerLink.Dispose();
                    _controllerLink = new MPiecLink();

                    this.SetState(ZxMcControllerState.Unknown, "Attempting Connection");
                    _controllerLink.StartConnect(ConnectPath, _configuration.IPAddress, MPiecLink.DefaultPort);
                    if (!_controllerLink.WaitForConnection(1500))
                    {
                        if (doRetry)
                        {
                            // Couldn't connect - try killing any running link process and starting again
                            Log.D(Cat, "Connection failed; restarting MPiecControl");
                            _controllerLink.ClearConnects();
                            return doConnect(false);
                        }

                        string Error = "Unable to connect to " + _configuration.IPAddress;
                        if (!String.IsNullOrEmpty(_controllerLink.ErrorMessage))
                            Error = _controllerLink.ErrorMessage;
                        this.SetState(ZxMcControllerState.Disconnected, Error);
                        return false;
                    }
                    else
                    {
                        if (_controllerConfig.setEnabled != null)
                            WriteVariable(_controllerConfig.setEnabled, true, "setEnabled");
                        return base.Connect();
                    }
                }
                else
                {
                    this.SetState(ZxMcControllerState.Disconnected, "No Yaskawa Configuration File Specified");
                    return false;
                }
            }
            catch (Exception ex)
            {
                StringBuilder Message = new StringBuilder();
                StringBuilder Trace = new StringBuilder();

                Message.Append(ex.Message);
                Trace.Append(ex.StackTrace);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Message.Append(ex.Message);
                    Trace.Append(ex.StackTrace);
                }

                Log.E(Cat, "Connect exception: " + Message + " at " + Trace);
                this.SetState(ZxMcControllerState.Errored, "Connect exception: " + Message);
                return false;
            }
        }

        public override bool Disconnect()
        {
            if (this.State == ZxMcControllerState.Disconnected)
                return true;
            bool flag = base.Disconnect();
            try
            {
                if (_controllerLink != null)
                {
                    _controllerLink.Shutdown();
                    _controllerLink.Dispose();
                }
                _controllerLink = null;
                return flag;
            }
            catch (Exception ex)
            {
                this.SetState(ZxMcControllerState.Errored, "Disconnect exception: " + ex.Message);
                return false;
            }
        }

        public override bool Reconnect()
        {
            Log.D(Cat, "Reconnecting to Yaskawa controller");
            Disconnect();
            if (_controllerLink == null)
                _controllerLink = new MPiecLink();
            _controllerLink.ClearConnects();
            return base.Reconnect();
        }

        public bool ResetConnection()
        {
            Disconnect();
            return Connect();
        }

        internal MPiecAxis GetAxisConfig(ZxMcAxisId id)
        {
            if (_controllerConfig == null)
                _controllerConfig = MPiecConfiguration.Load(_configuration.ConfigFile);

            if (id.AxisNumber <= _controllerConfig.Axes.Count())
                return _controllerConfig.Axes[id.AxisNumber];
            return null;
        }

        object RWlock = new object();

        internal bool WriteVariable(MPiecVariable Variable, object Value, string VariableDefinition = null, bool IsRetrying=false)
        {
            if (!Operable)
                return false;

            if (Variable == null)
                throw new ArgumentException("Variable " + (String.IsNullOrEmpty(VariableDefinition) ? "name" : "\"" + VariableDefinition + "\"") + " not defined in MP Configuration file");

            string result = _controllerLink.GetCommandResponse("write " + Variable.GetWriteCommand(Value));
            if (string.IsNullOrEmpty(result) || !result.Equals("ok"))
            {
                Log.D(Cat, "Writing: " + Variable.GetWriteCommand(Value).ToString() + " => \"" + result + "\"");
                if (Reconnect())
                    result = _controllerLink.GetCommandResponse("write " + Variable.GetWriteCommand(Value));
            }
            return (!string.IsNullOrEmpty(result) && result.Equals("ok"));
        }

        internal bool SetVariables(List<MPiecVariable> Variables, object Value)
        {
            if ((_controllerLink == null) || !_controllerLink.HasConnected())
                return false;

            StringBuilder WriteCommand = new StringBuilder(2*Variables.Count);
            foreach (MPiecVariable var in Variables)
            {
                WriteCommand.Append(var.GetWriteCommand(Value));
                WriteCommand.Append(",");
            }

            if (WriteCommand.Length > 1)
            {
                WriteCommand.Length--;
                string result = _controllerLink.GetCommandResponse("write " + WriteCommand.ToString());
                if (string.IsNullOrEmpty(result) || !result.Equals("ok"))
                {
                    Log.D(Cat, "Writing: " + WriteCommand.ToString());
                    if (Reconnect())
                        result = _controllerLink.GetCommandResponse("write " + WriteCommand.ToString());
                }
                return (!string.IsNullOrEmpty(result) && result.Equals("ok"));
            }

            return false;
        }

        internal bool IsVirtual => (_controllerLink == null) ? false : _controllerLink.IsVirtual;

        internal bool ReadBoolVariable(MPiecVariable ReadVariable, string VariableDefinition = null)
        {
            if (!Operable)
                return false;

            if (ReadVariable == null)
                throw new ArgumentException("Variable " + (String.IsNullOrEmpty(VariableDefinition) ? "name" : "\"" + VariableDefinition + "\"") + " not defined in MP Configuration file");

            string response = _controllerLink.GetCommandResponse("read " + ReadVariable.GetReadCommand());
            if (string.IsNullOrEmpty(response))
            {
                Log.D(Cat, "Reading: " + ReadVariable.GetReadCommand().ToString());
                if (!Reconnect())
                    return false;
                response = _controllerLink.GetCommandResponse("read " + ReadVariable.GetReadCommand());
            }
            return ReadVariable.IsTrue(response);
        }

        internal double ReadVariable(MPiecVariable ReadVariable, string VariableDefinition = null)
        {
            if (!Operable)
                return 0;

            if (ReadVariable == null)
                throw new ArgumentException("Variable " + (String.IsNullOrEmpty(VariableDefinition) ? "name" : "\"" + VariableDefinition + "\"") + " not defined in MP Configuration file");

            double Value = 0;
            string response = _controllerLink.GetCommandResponse("read " + ReadVariable.GetReadCommand());
            if (string.IsNullOrEmpty(response))
            {
                Log.D(Cat, "Reading: " + ReadVariable.GetReadCommand().ToString());
                if (Reconnect())
                    response = _controllerLink.GetCommandResponse("read " + ReadVariable.GetReadCommand());
            }
            double.TryParse(response, out Value);
            return Value;
        }

        public override int AxisCount
        {
            get
            {
                return (_controllerConfig == null) ? 0 : _controllerConfig.Axes.Count;
            }
        }

        public override bool CanHomeAll
        {
            get
            {
                return base.CanHomeAll;
            }
        }

        public override void HomeAll()
        {
            base.HomeAll();
        }

        public override void Abort()
        {
            if (!this.Commandable)
                return;
            base.Abort();

            bool stopped = false;
            if (_controllerConfig.stopAllMotion != null)
            {
                stopped = WriteVariable(_controllerConfig.stopAllMotion, true, "stopAllMotion");
                Task.Delay(25).Wait();
                WriteVariable(_controllerConfig.stopAllMotion, false, "stopAllMotion");
            }
            else if (_controllerConfig.setEnabled != null)
            {
                stopped = WriteVariable(_controllerConfig.setEnabled, false, "setEnabled");
                _controllerConfig.setEnabled.Value = !stopped;
            }
            else
                Log.I(Cat, "No variable defined in controller config to stop all motion");

            if (!stopped)
                Log.E(Cat, "UNABLE TO ABORT");
        }

        public void SetJogSpeed(double speed)
        {
            if (this.Commandable)
            {
                if (_controllerConfig.jogSpeed != null)
                    WriteVariable(_controllerConfig.jogSpeed, speed, "jogSpeed");
            }
        }

        public override ZxMcControllerConfigurationControl ConfigurationControl
        {
            get
            {
                return (ZxMcControllerConfigurationControl)new YaskawaConfigurationControl(this);
            }
        }

        public override ZxMcControllerConfigurationAdapter Configuration
        {
            get
            {
                return (ZxMcControllerConfigurationAdapter)this._configuration;
            }
        }

        public override void ApplyConfigurationChanges()
        {
            ConfigFile = _configuration.ConfigFile;
            IPAddress = _configuration.IPAddress;

            base.ApplyConfigurationChanges();
        }

        public override void RevertConfigurationChanges()
        {
            _configuration.ConfigFile = ConfigFile;
            _configuration.IPAddress = IPAddress;

            base.RevertConfigurationChanges();
        }

        public override ZxMcControllerTechniqueControl TechniqueControl
        {
            get
            {
                return new ZxMcControllerTechniqueControl();
            }
        }

        public override ZxMcControllerTechniqueAdapter Technique
        {
            get
            {
                return (ZxMcControllerTechniqueAdapter)this._technique;
            }
        }

    }
}
