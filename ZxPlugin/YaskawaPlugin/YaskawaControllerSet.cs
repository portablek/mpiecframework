﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZaXa.Hardware.Motion;
using ZaXa.Hardware.Motion.Controller;
using ZaXa.Hardware.Motion.Manager;
using ZaXa.Hardware.Motion.Plugins;

namespace YaskawaPlugin
{
    public class YaskawaControllerSet : ZxMcPluginControllerSet
    {
        public YaskawaControllerSet(IZxMcManager motionManager)
          : base(motionManager)
        {
        }

        protected override IZxMcController CreateControllerInstance(
          ZxMcControllerId id,
          IZxMcManager controllerManager,
          ZxMcConfigurations configurations)
        {
            return (IZxMcController)new YaskawaController(id, controllerManager, configurations);
        }
    }
}
