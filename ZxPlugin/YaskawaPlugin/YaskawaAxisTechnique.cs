﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZaXa.Hardware.Motion.Axis;
using ZaXa.Hardware.Motion.Axis.Configuration;
using ZaXa.Tools.OmniConfig;

namespace YaskawaPlugin
{
    public class YaskawaAxisTechnique : ZxMcAxisTechniqueAdapter
    {
        private const string YaskawaDefaultCategory = "Yaskawa Axis";

        public YaskawaAxisTechnique(YaskawaAxis axis, IZxOmniConfig omniConfig)
          : base((IZxMcAxis)axis, omniConfig)
        {
        }

        protected override string DefaultCategory
        {
            get
            {
                return "Yaskawa Axis";
            }
        }
    }
}
