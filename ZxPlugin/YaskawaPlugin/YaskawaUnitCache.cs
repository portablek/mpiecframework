﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZaXa.Hardware.Motion.Axis;
using ZaXa.Hardware.Motion.Units.Base;
using ZaXa.Hardware.Motion.Units.Cache;
using ZaXa.Hardware.Motion.Units.Core;

namespace YaskawaPlugin
{
    public class YaskawaUnitCache : ZxAxisUnitCache
    {
        public YaskawaUnitCache(
          IZxMcAxis axis,
          ZxMcCoreUnit conversionUnit,
          double conversionValue,
          params ZxEngineeringAxisUnit[] engineeringAxisUnits)
          : base(axis, new ZxBaseAxisUnit(axis, "ST", "Step"), conversionUnit, conversionValue, engineeringAxisUnits)
        {
        }
    }
}
