﻿using InternetFramework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ZaXa.Logging.Global;

namespace MPiecConnectLink
{
    public class MPiecLink : IDisposable
    {
        private static string ProcessName = "MPiecConnect";
        public static string ExecutableName = "MPIECConnect.exe";
        public static ushort DefaultPort = 23230;

        BufferedInternetClient<LineBasedProtocol> Client = null;
        Process Command = null;

        private string CommandResponse = null;

        private object commandLock = new object();
        private TaskCompletionSource<string> ResponseRecv = new TaskCompletionSource<string>();

        private bool isVirtualKnown = false;
        private bool _isVirtual;
        public bool IsVirtual
        {
            get
            {
                try
                {
                    if ((Client == null) || !Client.IsConnected)
                        return false;

                    if (!isVirtualKnown)
                    {
                        _isVirtual = GetCommandResponse("controllerip").Trim().ToLower().Equals("virtual");
                        isVirtualKnown = true;
                    }

                    return _isVirtual;
                }
                catch { return false; }
            }
        }

        public string ErrorMessage { get; private set; }

        private ushort PortNumber = DefaultPort;

        public void ClearConnects()
        {
            foreach (Process process in Process.GetProcessesByName(ProcessName))
            {
                try
                {
                    process.Kill();
                }
                catch { }
            }
        }

        public bool IsConnectRunning()
        {
            return Process.GetProcessesByName(ProcessName).Count() > 0;
        }

        public void StartConnect(string ConnectPath, string ControllerIP, ushort ServerPort)
        {
            if (!IsConnectRunning())
            {
                ProcessStartInfo startInfo = new ProcessStartInfo()
                {
                    FileName = ConnectPath,
                    Arguments = ControllerIP + " " + ServerPort.ToString(),
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    CreateNoWindow = true
                };

                Command = new Process();
                Command.StartInfo = startInfo;
                Command.OutputDataReceived += Command_OutputDataReceived;
                Command.Exited += Command_Exited;

                Command.Start();
                Command.BeginOutputReadLine();
            }
            else
            {
                // Attempt to connect to the running server at the port given
                StartClient("localhost", ServerPort.ToString());
            }
        }

        private void Command_Exited(object sender, EventArgs e)
        {
            Command = null;
            ClearConnects();
        }

        private void Command_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            string TelnetServerString = "Telnet server started at ";
            string ExceptionString = "Error: ";

            if (!String.IsNullOrEmpty(e.Data))
            {
                // Store any exception message, if process generated one
                if (e.Data.StartsWith(ExceptionString))
                    ErrorMessage = e.Data.Substring(ExceptionString.Length);

                // Start a client to the telnet server, if it started (successful connection made to controller)
                int index = e.Data.IndexOf(TelnetServerString);
                if (index >= 0)
                {
                    string[] serverinfo = e.Data.Substring(index + TelnetServerString.Length).Split(new string[] { ", port" }, StringSplitOptions.RemoveEmptyEntries);
                    //string serverAddress = serverinfo[0];
                    string serverAddress = "localhost";
                    string serverPort = "23";
                    if (serverinfo.Length > 1)
                        serverPort = serverinfo[1];

                    StartClient(serverAddress, serverPort);
                }
            }
        }

        public void StartClient(string ServerAddress, string ServerPort)
        {
            isVirtualKnown = false;
            Client = new TelnetClient(ushort.Parse(ServerPort));
            Client.RemoteDisconnected += Client_RemoteDisconnected;
            Client.IncomingMessage += Client_IncomingMessage;
            Client.PacketType.EndOfLine = new byte[] { (byte)'>', (byte)' ' };
            _ = Client.ConnectAsync(ServerAddress);
        }


        public string GetCommandResponse(string Command, int Timeoutms = 1500)
        {
            string Response = "";
            lock (commandLock)
            {
                try
                {
                    //Log.D("MCL", "Sending: " + Command);
                    ResponseRecv = new TaskCompletionSource<string>();
                    Client.Send(Command + "\r\n");
                    //Log.D("MCL", "Waiting for Result");
                    Task Timeout = Task.Delay(Timeoutms);
                    Task Result = Task.WhenAny(Timeout, ResponseRecv.Task).Result;
                    if (Result == ResponseRecv.Task)
                        Response = ResponseRecv.Task.Result;
                    //Log.D("MCL", "Result: " + Result.ToString());
                }
                catch (Exception ex)
                {
                    Response = "ERROR: " + ex.Message;
                }
            }

            return Response;
        }

        public bool HasConnected()
        {
            return ((Client != null) && Client.IsConnected);
        }

        public bool WaitForConnection(int Timeout)
        {
            return Task.Run(() =>
            {
                while (!HasConnected() && (Timeout > 0))
                {
                    Task.Delay(25).Wait();
                    Timeout -= 25;
                }

                // If timeout expired (we aren't connected), make sure the MPiecConnect process is stopped
                if (!HasConnected())
                {
                    Shutdown();
                    return false;
                }

                Log.W("MCL", "Connected - Timeout = " + Timeout + ", CommandResponse = " + CommandResponse);
                return true;
            }).Result;
        }

        private void Client_IncomingMessage(object sender, InternetFramework.Events.InternetCommunicationEventArgs e)
        {
            string Message = UTF8Encoding.UTF8.GetString(Client.PacketType.Trim(e.Message));
            CommandResponse = Message.Trim();
            if (!String.IsNullOrEmpty(CommandResponse))
            {
                //Log.D("MCL", "Got Response: " + CommandResponse);
                ResponseRecv.SetResult(CommandResponse);
            }
        }

        private void Client_RemoteDisconnected(object sender, InternetFramework.Events.InternetConnectionEventArgs e)
        {
            Shutdown();
        }

        public void Shutdown()
        {
            try
            {
                // If we're waiting for a response, give up
                ResponseRecv.SetCanceled();

                if (Client != null)
                {
                    Client.Disconnect();
                    Client.IncomingMessage -= Client_IncomingMessage;
                    Client.RemoteDisconnected -= Client_RemoteDisconnected;
                }
                Client = null;
                Command = null;
            }
            catch
            {

            }
        }

        private bool disposedValue = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    if (Client != null)
                    {
                        Client.Dispose();
                        Client = null;
                    }
                    if (Command != null)
                    {
                        Command.Kill();
                        Command.Dispose();
                        Command = null;
                    }
                    Shutdown();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~MPiecLink()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
