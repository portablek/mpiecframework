﻿using MPiecFramework;
using MPiecFramework.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetMPIECVar
{
    class Program
    {
        static int EXIT_SUCCESS = 0;
        static int EXIT_ARGUMENT_ERROR = 1;
        static int EXIT_COMMUNICATION_ERROR = 2;

        static int CommandDuration = 500;

        static async Task<string> ReadVariable(string Address, string vardef)
        {
            IMPiec Controller;

            if (Address.Equals("virtual"))
                Controller = MPiec.GetvirtualInterface();
            else
            {
                Controller = MPiec.GetControllerInterface();

                if (!Controller.Connect(Address).Result)
                {
                    Console.WriteLine("Can not connect to controller");
                    return null;
                }
            }

            Console.WriteLine("Connected to " + Controller.GetProductName().Result + " by " + Controller.GetManufacturer().Result);

            Console.WriteLine("Getting " + vardef);
            Dictionary<string, dynamic> Results = Controller.ReadGlobalVariables(new List<string>() { vardef }).Result;
            List<string> values = new List<string>();
            foreach (dynamic result in Results.Values)
                values.Add(result == null ? "" : Convert.ToString(result));

            await Controller.Disconnect();
            return values[0];
        }

        static async Task Main(string[] args)
        {
            string MyName = Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetExecutingAssembly().Location);

            string Address = (args.Count() >= 1) ? args[0] : "";
            string Variable = (args.Count() >= 2) ? args[1] : "";
            string Type = (args.Count() >= 3) ? args[2] : "BOOL";

            if (string.IsNullOrEmpty(Address) || String.IsNullOrEmpty(Variable))
            {
                Console.WriteLine(MyName + " - Get variable on Yaskawa MPIEC controllers\n");
                Console.WriteLine("\tSyntax: " + MyName + " <Controller Address> <Variable Name> [<Type>]");
                Console.WriteLine("\t<Controller Address>\tIPv4 address of controller in dot-notation (ex: 192.168.1.1)");
                Console.WriteLine("\t<Variable Name>\t\tName of variable on controller to set");
                Console.WriteLine("\t<Type>\t\t\tMPWorks variable type (BOOL, LREAL, etc). If not specified, BOOL will be assumed");
                Console.WriteLine("\nWill end with exit code " +
                                    EXIT_SUCCESS + " if variable successfully read, " +
                                    EXIT_ARGUMENT_ERROR + " 1 on argument error, and " +
                                    EXIT_COMMUNICATION_ERROR + " on communication error");
                Environment.Exit(EXIT_ARGUMENT_ERROR);
            }

            string vardef = Variable + ":" + Type;
            Console.WriteLine(Address + " => " + vardef);

            try
            {
                string result = await ReadVariable(Address, vardef);
                if (!String.IsNullOrEmpty(result))
                    Environment.Exit(EXIT_COMMUNICATION_ERROR);
                Console.WriteLine(result);
            }
            catch (Exception ex)
            {
                string tabs = "";
                while (ex.InnerException != null)
                {
                    Console.WriteLine(tabs + ex.Message);
                    ex = ex.InnerException;
                    tabs = tabs + "\t";
                }
                Console.WriteLine(tabs + ex.Message);

                Environment.Exit(EXIT_COMMUNICATION_ERROR);
            }

            Environment.Exit(EXIT_SUCCESS);
        }

    }
}
