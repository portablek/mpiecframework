﻿using MPiecFramework.Events;
using MPiecFramework.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MPiecFramework
{
    public static class MPiec
    {
        /// <summary>
        /// Return an instance of the MPiec framework interface appropriate for the current process
        /// </summary>
        /// <returns>Appropriate controller interface</returns>
        public static IMPiec GetControllerInterface()
        {
            //if (Environment.Is64BitProcess)
            //{
            //    Assembly a = Assembly.Load("MPiec64");
            //    Type type = a.GetType("MPiecFramework.MPiec64");
            //    return (IMPiec)Activator.CreateInstance(type);
            //}
            //else
            //{
                Assembly a = Assembly.Load("MPiec32");
                Type type = a.GetType("MPiecFramework.MPiec32");
                return (IMPiec)Activator.CreateInstance(type);
            //}
        }

        public static IMPiec GetvirtualInterface()
        {
            return new VirtualMPiec();
        }
    };
}
