﻿using MPiecFramework.Events;
using MPiecFramework.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MPiecFramework
{
    /// <summary>
    /// Implement a virtual MPiec controller
    /// </summary>
    public class VirtualMPiec : IMPiec
    {
        string VersionString = "1.0";

        #region Public Properties

        public bool HasConnected => true;
        public string IPAddress => "Virtual";
        public UInt16 Port => 0;
        public UInt16 Timeout { get; set; }

        #endregion

        #region Error and Event Handling

        /// <summary>
        /// General error encountered attempting to communicate with controller (ethernet timeout/reachability issue, etc)
        /// </summary>
        public event EventHandler<MPIECCommunicationErrorEventArgs> CommunicationError;
        protected virtual void OnCommunicationError(MPIECCommunicationErrorEventArgs e)
        {
            if (CommunicationError != null)
                CommunicationError.Invoke(this, e);
            else
            {
                throw new MPIECCommunicationException(e);
            }
        }

        /// <summary>
        /// Setting name not found when attempting to read or write a device setting
        /// </summary>
        public event EventHandler<MPIECSettingNotFoundEventArgs> SettingNotFound;
        protected virtual void OnSettingNotFound(MPIECSettingNotFoundEventArgs e)
        {
            if (SettingNotFound != null)
                SettingNotFound.Invoke(this, e);
            else
                throw new MPIECSettingNotFoundException(e);
        }

        /// <summary>
        /// Invalid or unknown subscription identifier specified when subscribing to or reading values on device
        /// </summary>
        public event EventHandler<MPIECInvalidSubscriptionEventArgs> InvalidSubscription;
        protected virtual void OnInvalidSubscription(MPIECInvalidSubscriptionEventArgs e)
        {
            if (InvalidSubscription != null)
                InvalidSubscription.Invoke(this, e);
            else
                throw new MPIECInvalidSubscriptionException(e);
        }

        /// <summary>
        /// Error encountered attempting to read values to variables on device
        /// </summary>
        public event EventHandler<MPIECVariableReadErrorEventArgs> ErrorReadingVariables;
        protected virtual void OnErrorReadingVariables(MPIECVariableReadErrorEventArgs e)
        {
            if (ErrorReadingVariables != null)
                ErrorReadingVariables.Invoke(this, e);
            else
                throw new MPIECVariableReadException(e);
        }

        /// <summary>
        /// Error encountered attempting to write values to variables on device
        /// </summary>
        public event EventHandler<MPIECVariableWriteErrorEventArgs> ErrorWritingVariables;
        protected virtual void OnErrorWritingVariables(MPIECVariableWriteErrorEventArgs e)
        {
            if (ErrorWritingVariables != null)
                ErrorWritingVariables.Invoke(this, e);
            else
                throw new MPIECVariableWriteException(e);
        }

        /// <summary>
        /// Error attempting to control the PLC on the motion controller
        /// </summary>
        public event EventHandler<MPIECControlErrorEventArgs> PLCControlError;
        protected virtual void OnControlError(MPIECControlErrorEventArgs e)
        {
            if (PLCControlError != null)
                PLCControlError.Invoke(this, e);
            else
                throw new MPIECControlException(e);
        }

        /// <summary>
        /// Error attempting to read a file on the device
        /// </summary>
        public event EventHandler<MPIECFileEventArgs> FileReadError;
        protected virtual void OnFileReadError(MPIECFileEventArgs e)
        {
            if (FileReadError != null)
                FileReadError.Invoke(this, e);
            else
                throw new MPIECFileAccessException(e);
        }

        /// <summary>
        /// Error attempting to write a file on the device
        /// </summary>
        public event EventHandler<MPIECFileEventArgs> FileWriteError;
        protected virtual void OnFileWriteError(MPIECFileEventArgs e)
        {
            if (FileWriteError != null)
                FileWriteError.Invoke(this, e);
            else
                throw new MPIECFileAccessException(e);
        }

        #endregion

        #region Lifecycle

        public Task<bool> Connect(string IPAddress, UInt16 Timeout = 0, UInt16 Port = 0) { return Task<bool>.Run(() => { return true; }); }

        public Task Disconnect() { return Task.Run(() => { return; }); }

        #endregion

        #region Device Attribute Getters

        ProgramState virtualProgramState = ProgramState.Running;

        public async Task<string> GetManufacturer() { return await Task.Run(() => { return "Portable Knowledge"; }); }
        public async Task<string> GetProductName() { return await Task.Run(() => { return "Virtual Controller"; }); }
        public async Task<string> GetDeviceIdentity() { return await Task.Run(() => { return ""; }); }
        public async Task<string> GetHardwareVersion() { return await Task.Run(() => { return VersionString; }); }
        public async Task<string> GetFirmwareVersion() { return await Task.Run(() => { return VersionString; }); }
        public async Task<string> GetRuntimeVersion() { return await Task.Run(() => { return VersionString; }); }
        public async Task<string> GetBootImageName() { return await Task.Run(() => { return "None"; }); }
        public async Task<DateTime> GetImageBuildDate() { return await Task.Run(() => { return DateTime.MinValue; }); }
        public async Task<bool> IsImageInstalled() { return await Task.Run(() => { return true; }); }
        public async Task<bool> IsSourceInstalled() { return await Task.Run(() => { return false; }); }
        public async Task<MPiecMemory> GetMemoryState() { return await Task.Run(() => { return new MPiecMemory(); }); }
        public async Task<ProgramState> GetProgramState() { return await Task.Run(() => { return virtualProgramState; }); }
        public async Task<UInt32> GetThreadingPriorityLevel() { return await Task.Run(() => { return (UInt32)0; }); }

        #endregion

        #region On-Device Variable Subscriptions for bulk/repeated reads

        private List<System.Timers.Timer> ReadTimers = new List<System.Timers.Timer>();

        Dictionary<string, IEnumerable<MPiecVariable>> Subscriptions = new Dictionary<string, IEnumerable<MPiecVariable>>();

        public async Task<string> SubscribeToVariables(IEnumerable<MPiecVariable> Variables, string SubscriptionID = null)
        {
            return await Task.Run(() =>
            {
                if (string.IsNullOrEmpty(SubscriptionID))
                    SubscriptionID = new Guid().ToString();

                if (Subscriptions.ContainsKey(SubscriptionID))
                    Subscriptions[SubscriptionID] = Variables;
                else
                    Subscriptions.Add(SubscriptionID, Variables);

                return SubscriptionID;
            });
        }

        public async Task<string> SubscribeToGlobalVariables(IEnumerable<string> VariableDefinitions, string SubscriptionID = null)
        {
            List<MPiecVariable> Variables = new List<MPiecVariable>();
            foreach (string Definition in VariableDefinitions)
                Variables.Add(new MPiecVariable(Definition));
            return await SubscribeToVariables(Variables, SubscriptionID);
        }

        public async Task<string> SubscribeToLocalVariables(IEnumerable<string> VariableDefinitions, string SubscriptionID = null)
        {
            List<MPiecVariable> Variables = new List<MPiecVariable>();
            foreach (string Definition in VariableDefinitions)
                Variables.Add(new MPiecVariable(Definition, MPiecVariable.Instance));
            return await SubscribeToVariables(Variables, SubscriptionID);
        }

        public async Task Unsubscribe(string SubscriptionID)
        {
            await Task.Run(() =>
            {
                if (Subscriptions.ContainsKey(SubscriptionID))
                    Subscriptions.Remove(SubscriptionID);
                else
                    OnInvalidSubscription(new MPIECInvalidSubscriptionEventArgs(SubscriptionID, null));
            });
        }

        public async Task UnsubscribeAll()
        {
            List<string> SubIDs = new List<string>(Subscriptions.Keys);
            foreach (string SubscriptionID in SubIDs)
                await Unsubscribe(SubscriptionID);
        }

        public async Task<Dictionary<string, dynamic>> ReadSubscriptionValues(string SubscriptionID)
        {
            Dictionary<string, dynamic> Values = new Dictionary<string, dynamic>();
            if (!Subscriptions.ContainsKey(SubscriptionID))
                OnInvalidSubscription(new MPIECInvalidSubscriptionEventArgs(SubscriptionID, new Exception("Unknown subscription ID: " + SubscriptionID)));
            else
            {
                IEnumerable<MPiecVariable> Variables = Subscriptions[SubscriptionID];
                await Task.Run(async () =>
                {
                    Dictionary<string, dynamic> varValues = await ReadVariables(Variables);
                    if (varValues.Count != Variables.Count())
                        OnInvalidSubscription(new MPIECInvalidSubscriptionEventArgs(SubscriptionID, new Exception(varValues.Count + " varaibles read (expected " + Variables.Count() + ")")));
                    foreach (string varName in varValues.Keys)
                        Values.Add(varName, varValues[varName]);
                });
            }
            return Values;
        }

        public System.Timers.Timer StartSubscriptionReadTimer(string SubscriptionID, int ReadInterval, EventHandler<MPIECSubscriptionReadEventArgs> timerHandler)
        {
            System.Timers.Timer readTimer = new System.Timers.Timer((double)ReadInterval);
            readTimer.Elapsed += (object sender, System.Timers.ElapsedEventArgs e) =>
            {
                timerHandler.Invoke(this, new MPIECSubscriptionReadEventArgs(ReadSubscriptionValues(SubscriptionID).Result));
            };
            readTimer.AutoReset = true;
            readTimer.Enabled = true;
            ReadTimers.Add(readTimer);
            readTimer.Start();

            return readTimer;
        }

        public void StopSubscriptionReadTimer(System.Timers.Timer readTimer)
        {
            try
            {
                if (readTimer != null)
                {
                    readTimer.Enabled = false;
                    readTimer.Dispose();
                }
            }
            catch
            {
                // Ignore any error clearing this timer
            }
        }

        public void StopAllSubscriptionReadTimers()
        {
            foreach (System.Timers.Timer readTimer in ReadTimers)
                StopSubscriptionReadTimer(readTimer);
        }

        #endregion

        #region On-Device Variable one-time Reading

        Dictionary<string, MPiecVariable> virtualVariables = new Dictionary<string, MPiecVariable>();
        private bool disposedValue;

        public async Task<Dictionary<string, dynamic>> ReadVariables(IEnumerable<MPiecVariable> Variables)
        {
            Dictionary<string, dynamic> Values = new Dictionary<string, dynamic>();
            await Task.Run(() =>
            {
                try
                {
                    foreach (MPiecVariable Variable in Variables)
                    {
                        if (virtualVariables.ContainsKey(Variable.DeviceName))
                            Values.Add(Variable.Name, virtualVariables[Variable.DeviceName].Value);
                        else
                            Values.Add(Variable.Name, "?");
                    }
                }
                catch (Exception ex)
                {
                    ErrorReadingVariables?.Invoke(this, new MPIECVariableReadErrorEventArgs(Variables.ToList(), ex));
                }
            });
            return Values;
        }

        public async Task<Dictionary<string, dynamic>> ReadGlobalVariables(IEnumerable<string> VariableDefinitions)
        {
            List<MPiecVariable> Variables = new List<MPiecVariable>();
            foreach (string Definition in VariableDefinitions)
                Variables.Add(new MPiecVariable(Definition));

            return await ReadVariables(Variables);
        }

        public async Task<Dictionary<string, dynamic>> ReadLocalVariables(IEnumerable<string> VariableDefinitions)
        {
            List<MPiecVariable> Variables = new List<MPiecVariable>();
            foreach (string Definition in VariableDefinitions)
                Variables.Add(new MPiecVariable(Definition, MPiecVariable.Instance));

            return await ReadVariables(Variables);
        }

        #endregion

        #region On-Device Variable Writing

        public async Task<bool> WriteVariables(IEnumerable<MPiecVariable> Variables)
        {
            return await Task.Run(() =>
            {
                try
                {
                    foreach (MPiecVariable var in Variables)
                    {
                        if (virtualVariables.ContainsKey(var.DeviceName))
                            virtualVariables[var.DeviceName] = var;
                        else
                            virtualVariables.Add(var.DeviceName, var);
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            });
        }

        public async Task<bool> WriteGlobalVariables(IEnumerable<string> VariableDefinitions)
        {
            List<MPiecVariable> Variables = new List<MPiecVariable>();
            foreach (string Definition in VariableDefinitions)
                Variables.Add(new MPiecVariable(Definition));

            return await WriteVariables(Variables);
        }

        public async Task<bool> WriteLocalVariables(IEnumerable<string> VariableDefinitions)
        {
            List<MPiecVariable> Variables = new List<MPiecVariable>();
            foreach (string Definition in VariableDefinitions)
                Variables.Add(new MPiecVariable(Definition, MPiecVariable.Instance));

            return await WriteVariables(Variables);
        }

        public async Task<bool> SetVariable(MPiecVariable Variable, bool isTrue)
        {
            Variable.Value = Variable.VariableType.Parse(isTrue);
            return await WriteVariables(new List<MPiecVariable>() { Variable });
        }

        #endregion

        #region PLC Control

        /// <summary>
        /// Activate the boot program (copy from Flash memory to RAM). Required on new upload and after reset
        /// before starting the program.
        /// </summary>
        /// <returns>TRUE on successful activation, FALSE otherwise</returns>
        public async Task<bool> Initialize()
        {
            return await Task.Run(() =>
            {
                try
                {
                    virtualProgramState = ProgramState.Loading;
                }
                catch (Exception ex)
                {
                    OnControlError(new MPIECControlErrorEventArgs(ex));
                    return false;
                }
                return true;
            });
        }

        private async Task<bool> Start()
        {
            return await Task.Run(() =>
            {
                try
                {
                    virtualProgramState = ProgramState.On;
                }
                catch (Exception ex)
                {
                    OnControlError(new MPIECControlErrorEventArgs(ex));
                    return false;
                }
                return true;
            });
        }

        public async Task<bool> WarmStart()
        {
            return await Start();
        }

        public async Task<bool> ColdStart()
        {
            return await Start();
        }

        public async Task<bool> HotStart()
        {
            return await Start();
        }

        public async Task<bool> Stop()
        {
            return await Task.Run(() =>
            {
                try
                {
                    virtualProgramState = ProgramState.Stop;
                }
                catch (Exception ex)
                {
                    OnControlError(new MPIECControlErrorEventArgs(ex));
                    return false;
                }
                return true;
            });
        }

        public async Task<bool> ResetPLC()
        {
            return await Task.Run(() =>
            {
                try
                {
                    virtualProgramState = ProgramState.Resetting;
                }
                catch (Exception ex)
                {
                    OnControlError(new MPIECControlErrorEventArgs(ex));
                    return false;
                }
                return true;
            });
        }

        #endregion

        #region File I/O

        public async Task<MemoryStream> GetFile(string fileName)
        {
            return await Task.Run(() =>
            {
                return (MemoryStream)null;
            });
        }
        public async Task<bool> PutFile(string fileName, MemoryStream fileData)
        {
            return await Task.Run(() =>
            {
                return false;
            });
        }

        public async Task<bool> DeleteFile(string fileName)
        {
            return await Task.Run(() =>
            {
                return false;
            });
        }

        #endregion

        #region IDisposable Interface 

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~VirtualMPiec()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}

