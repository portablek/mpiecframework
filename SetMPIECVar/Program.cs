﻿using MPiecFramework;
using MPiecFramework.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetMPIECVar
{
    class Program
    {
        static int EXIT_SUCCESS = 0;
        static int EXIT_ARGUMENT_ERROR = 1;
        static int EXIT_COMMUNICATION_ERROR = 2;

        static int CommandDuration = 500;

        static async Task<bool> WriteVariable(string Address, string vardef, string vardefoff)
        {
            IMPiec Controller;

            if (Address.Equals("virtual"))
                Controller = MPiec.GetvirtualInterface();
            else
            {
                Controller = MPiec.GetControllerInterface();

                if (!Controller.Connect(Address).Result)
                {
                    Console.WriteLine("Can not connect to controller");
                    return false;
                }
            }

            Console.WriteLine("Connected to " + Controller.GetProductName().Result + " by " + Controller.GetManufacturer().Result);

            Console.WriteLine("Setting " + vardef);
            if (!await Controller.WriteGlobalVariables(new List<string>() { vardef }))
            {
                Console.WriteLine("Unable to write to variable.  Aborted.");
                return false;
            }

            if (!String.IsNullOrEmpty(vardefoff))
            {
                await Task.Delay(CommandDuration);
                Console.WriteLine("Setting " + vardefoff);
                if (!await Controller.WriteGlobalVariables(new List<string>() { vardefoff }))
                {
                    Console.WriteLine("Unable to write to variable.  Aborted.");
                    return false;
                }
            }

            await Controller.Disconnect();
            return true;
        }

        static async Task Main(string[] args)
        {
            string MyName = Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetExecutingAssembly().Location);

            string Address = (args.Count() >= 1) ? args[0] : "";
            string Variable = (args.Count() >= 2) ? args[1] : "";
            string Value = (args.Count() >= 3) ? args[2] : "True";
            string Type = (args.Count() >= 4) ? args[3] : "BOOL";
            bool Momentary = (args.Count() < 3);

            if (string.IsNullOrEmpty(Address) || String.IsNullOrEmpty(Variable))
            {
                Console.WriteLine(MyName + " - Set variable on Yaskawa MPIEC controllers\n");
                Console.WriteLine("\tSyntax: " + MyName + " <Controller Address> <Variable Name> [<Value> [<Type>]]");
                Console.WriteLine("\t<Controller Address>\tIPv4 address of controller in dot-notation (ex: 192.168.1.1)");
                Console.WriteLine("\t<Variable Name>\t\tName of variable on controller to set");
                Console.WriteLine("\t<Value>\t\t\tValue to set variable to. If not specified, variable will be momentairly set to TRUE");
                Console.WriteLine("\t<Type>\t\t\tMPWorks variable type (BOOL, LREAL, etc). If not specified, BOOL will be assumed");
                Console.WriteLine("\nWill end with exit code " + 
                                    EXIT_SUCCESS + " if variable successfully set, " + 
                                    EXIT_ARGUMENT_ERROR + " 1 on argument error, and " +
                                    EXIT_COMMUNICATION_ERROR + " on communication error");
                Environment.Exit(EXIT_ARGUMENT_ERROR);
            }

            string vardef = Variable + ":" + Type + "=" + Value;
            string vardefoff = Variable + ":" + Type + "=False";
            Console.WriteLine(Address + " => " + vardef);

            try
            {
                if (!await WriteVariable(Address, vardef, Momentary ? vardefoff : ""))
                    Environment.Exit(EXIT_COMMUNICATION_ERROR);
            }
            catch (Exception ex)
            {
                string tabs = "";
                while (ex.InnerException != null)
                {
                    Console.WriteLine(tabs + ex.Message);
                    ex = ex.InnerException;
                    tabs = tabs + "\t";
                }
                Console.WriteLine(tabs + ex.Message);

                Environment.Exit(EXIT_COMMUNICATION_ERROR);
            }

            Environment.Exit(EXIT_SUCCESS);
        }

    }
}
