﻿using InternetFramework;
using InternetFramework.Extensions;
using MPiecFramework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace MPIECConnect
{
    class Program
    {
        class ConsoleInterface
        {
            int NumConnections = 0;
            IMPiec Controller;
            TelnetServer Telnet;

            public void RunConsoleInterface(string controllerAddress, ushort connectPort)
            {
                if (controllerAddress.Equals("virtual"))
                {
                    Controller = MPiec.GetvirtualInterface();
                }
                else
                {
                    Controller = MPiec.GetControllerInterface();

                    if (!Controller.Connect(controllerAddress).Result)
                    {
                        Console.WriteLine("Can not connect to controller");
                        Environment.ExitCode = -1;
                        return;
                    }
                }

                Console.WriteLine("Connected to " + Controller.GetProductName().Result + " by " + Controller.GetManufacturer().Result);

                //Console.WriteLine("Valid Host IP Addresses:");
                //foreach (IPAddress Address in IPAddressExtensions.LocalIPAddresses())
                //    Console.WriteLine("\t " + Address.ToString() + " (" + Address.MapToIPv4().ToString() + ")");

                //Console.WriteLine("Selected IP Address: " + IPAddressExtensions.LocalIPv4Address().ToString());

                Telnet = new TelnetServer(IPAddress.Any, connectPort);
                Telnet.ServerStarted += Telnet_ServerStarted;
                Telnet.NewConnection += Telnet_NewConnection;
                Telnet.IncomingMessage += Telnet_IncomingMessage;
                Telnet.RemoteDisconnected += Telnet_RemoteDisconnected;

                Console.WriteLine();
                Telnet.Start();
            }

            private void Telnet_ServerStarted(object sender, InternetFramework.Events.InternetEventArgs e)
            {
                Console.WriteLine("Telnet server started at " + e.Local.Address + ", port " + e.Local.Port);
            }

            private void Telnet_NewConnection(object sender, InternetFramework.Events.InternetConnectionEventArgs e)
            {
                NumConnections++;
                ShowPrompt(e.Remote);
            }

            private void Telnet_RemoteDisconnected(object sender, InternetFramework.Events.InternetConnectionEventArgs e)
            {
                NumConnections--;

                // Cleanly end and exit after last connected client disconnects
                if (NumConnections == 0)
                    Telnet.Stop();
            }


            public string[] GetCommandResponse(string Command)
            {
                try
                {
                    string[] Params = Command.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                    IEnumerable<string> defs = String.Join(" ", Params.Skip(1)).Split(',');
                    switch (Params[0].ToLower())
                    {
                        case "write":
                            if (!Controller.WriteGlobalVariables(defs).Result)
                                return new string[] { "Unable to write global variables" };
                            else
                                return new string[] { "ok" };
                        case "read":
                            //BoolAndOut:BOOL,BoolOrOut:BOOL,RealSumOutput:REAL
                            Dictionary<string, dynamic> Results = Controller.ReadGlobalVariables(defs).Result;
                            List<string> values = new List<string>();
                            foreach (dynamic result in Results.Values)
                                values.Add(result == null ? "" : Convert.ToString(result));
                            return values.ToArray();
                        case "subscribe":
                            string id = Controller.SubscribeToGlobalVariables(defs).Result;
                            return new string[] { id };
                        case "readsub":
                            Results = Controller.ReadSubscriptionValues(Params[1]).Result;
                            List<string> returns = new List<string>();
                            foreach (string variable in Results.Keys)
                                returns.Add(variable + ":" + Results[variable]);
                            return returns.ToArray();
                        case "product":
                            return new string[] { Controller.GetProductName().Result };
                        case "manufacturer":
                            return new string[] { Controller.GetManufacturer().Result };
                        case "reboot":
                            Controller.Stop();
                            Controller.WarmStart();
                            return new string[] { "ok" };
                    }
                }
                catch (Exception ex)
                {
                    while (ex.InnerException != null)
                        ex = ex.InnerException;

                    return new string[] { ex.Message };
                }
                return null;
            }

            private void Telnet_IncomingMessage(object sender, InternetFramework.Events.InternetCommunicationEventArgs e)
            {
                string Command = UTF8Encoding.UTF8.GetString(Telnet.PacketType.Trim(e.Message));
                if (Command.ToLower() == "quit")
                {
                    Telnet.Disconnect(e.Remote);
                }
                else if (Command.ToLower() == "connections")
                {
                    Telnet.SendLine(e.Remote, "Active Connections: " + NumConnections);
                    ShowPrompt(e.Remote);
                }
                else if (Command.ToLower() == "controllerip")
                {
                    Telnet.SendLine(e.Remote, (Controller == null ? "(none)" : Controller.IPAddress));
                    ShowPrompt(e.Remote);
                }
                else
                {
                    string[] Responses = GetCommandResponse(Command);
                    if (Responses != null)
                    {
                        foreach (string Response in Responses)
                            Telnet.SendLine(e.Remote, Response);
                    }
                    ShowPrompt(e.Remote);
                }
            }

            private void ShowPrompt(INetworkNode Remote)
            {
                Telnet.Send(Remote, "> ");
            }

        }

        static void Main(string[] args)
        {
            System.AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            string controllerAddress = "192.168.20.57";
            ushort port = 23;

            Console.WriteLine("MPIECConnect");
            Console.WriteLine();
            ConsoleInterface console = new ConsoleInterface();

            //for (int i = 0; i < args.Count(); i++)
            //    Console.WriteLine("Args[" + i + "] = " + args[i]);

            if (args.Count() >= 1)
                controllerAddress = args[0];
            if (args.Count() >= 2)
                port = Convert.ToUInt16(args[1]);

            console.RunConsoleInterface(controllerAddress, port);
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            string Message;

            // Just display the root error, if we can determine it
            Exception ex = (Exception)e.ExceptionObject;
            if (ex != null)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                Message = ex.Message;
            }
            else
                Message = "Unknown exception occured";

            Console.WriteLine("Error: " + Message);
        }
    }
}
