﻿namespace MPiecControlTester
{
    partial class frmTestControlPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTestControlPanel));
            this.btnSaveConfig = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btnLoadConfig = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.grpController = new System.Windows.Forms.GroupBox();
            this.btnWarmRestart = new System.Windows.Forms.Button();
            this.flowAxes = new System.Windows.Forms.FlowLayoutPanel();
            this.btnClearAllAlarms = new System.Windows.Forms.Button();
            this.btnStopAllMotion = new System.Windows.Forms.Button();
            this.btnDisableSystem = new System.Windows.Forms.Button();
            this.btnEnableSystem = new System.Windows.Forms.Button();
            this.lblSystemEnabled = new System.Windows.Forms.Label();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.txtControllerAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnEditConfiguration = new System.Windows.Forms.Button();
            this.btnNewConfiguration = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grpController.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSaveConfig
            // 
            this.btnSaveConfig.Location = new System.Drawing.Point(10, 191);
            this.btnSaveConfig.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSaveConfig.Name = "btnSaveConfig";
            this.btnSaveConfig.Size = new System.Drawing.Size(126, 27);
            this.btnSaveConfig.TabIndex = 0;
            this.btnSaveConfig.Text = "Save Configuration";
            this.btnSaveConfig.UseVisualStyleBackColor = true;
            this.btnSaveConfig.Click += new System.EventHandler(this.btnSaveConfig_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "mpc";
            this.saveFileDialog1.Filter = "Controller Configuration (*.mpc)|*.mpc|All Files (*.*)|*.*";
            this.saveFileDialog1.Title = "Save Controller Configuration As...";
            // 
            // btnLoadConfig
            // 
            this.btnLoadConfig.Location = new System.Drawing.Point(11, 25);
            this.btnLoadConfig.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnLoadConfig.Name = "btnLoadConfig";
            this.btnLoadConfig.Size = new System.Drawing.Size(126, 27);
            this.btnLoadConfig.TabIndex = 1;
            this.btnLoadConfig.Text = "Load Configuration";
            this.btnLoadConfig.UseVisualStyleBackColor = true;
            this.btnLoadConfig.Click += new System.EventHandler(this.btnLoadConfig_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "mpc";
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Controller Configuration (*.mpc)|*.mpc|All Files (*.*)|*.*";
            this.openFileDialog1.Title = "Load Controller Configuration From...";
            // 
            // grpController
            // 
            this.grpController.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpController.Controls.Add(this.btnWarmRestart);
            this.grpController.Controls.Add(this.flowAxes);
            this.grpController.Controls.Add(this.btnClearAllAlarms);
            this.grpController.Controls.Add(this.btnStopAllMotion);
            this.grpController.Controls.Add(this.btnDisableSystem);
            this.grpController.Controls.Add(this.btnEnableSystem);
            this.grpController.Controls.Add(this.lblSystemEnabled);
            this.grpController.Location = new System.Drawing.Point(184, 56);
            this.grpController.Name = "grpController";
            this.grpController.Size = new System.Drawing.Size(1086, 597);
            this.grpController.TabIndex = 2;
            this.grpController.TabStop = false;
            this.grpController.Text = "(No Configuration Loaded)";
            // 
            // btnWarmRestart
            // 
            this.btnWarmRestart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWarmRestart.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWarmRestart.Location = new System.Drawing.Point(949, 18);
            this.btnWarmRestart.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnWarmRestart.Name = "btnWarmRestart";
            this.btnWarmRestart.Size = new System.Drawing.Size(127, 24);
            this.btnWarmRestart.TabIndex = 13;
            this.btnWarmRestart.Text = "Warm Restart";
            this.btnWarmRestart.UseVisualStyleBackColor = true;
            this.btnWarmRestart.Click += new System.EventHandler(this.btnWarmRestart_Click);
            // 
            // flowAxes
            // 
            this.flowAxes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowAxes.AutoScroll = true;
            this.flowAxes.Location = new System.Drawing.Point(9, 95);
            this.flowAxes.Name = "flowAxes";
            this.flowAxes.Size = new System.Drawing.Size(1071, 496);
            this.flowAxes.TabIndex = 14;
            // 
            // btnClearAllAlarms
            // 
            this.btnClearAllAlarms.Location = new System.Drawing.Point(397, 31);
            this.btnClearAllAlarms.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnClearAllAlarms.Name = "btnClearAllAlarms";
            this.btnClearAllAlarms.Size = new System.Drawing.Size(123, 39);
            this.btnClearAllAlarms.TabIndex = 13;
            this.btnClearAllAlarms.Text = "Clear All Alarms";
            this.btnClearAllAlarms.UseVisualStyleBackColor = true;
            this.btnClearAllAlarms.Click += new System.EventHandler(this.btnClearAllAlarms_Click);
            // 
            // btnStopAllMotion
            // 
            this.btnStopAllMotion.Location = new System.Drawing.Point(240, 31);
            this.btnStopAllMotion.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnStopAllMotion.Name = "btnStopAllMotion";
            this.btnStopAllMotion.Size = new System.Drawing.Size(126, 39);
            this.btnStopAllMotion.TabIndex = 12;
            this.btnStopAllMotion.Text = "Stop All Motion";
            this.btnStopAllMotion.UseVisualStyleBackColor = true;
            this.btnStopAllMotion.Click += new System.EventHandler(this.btnStopAllMotion_Click);
            // 
            // btnDisableSystem
            // 
            this.btnDisableSystem.Location = new System.Drawing.Point(116, 49);
            this.btnDisableSystem.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDisableSystem.Name = "btnDisableSystem";
            this.btnDisableSystem.Size = new System.Drawing.Size(71, 27);
            this.btnDisableSystem.TabIndex = 11;
            this.btnDisableSystem.Text = "Disable";
            this.btnDisableSystem.UseVisualStyleBackColor = true;
            this.btnDisableSystem.Click += new System.EventHandler(this.btnDisableSystem_Click);
            // 
            // btnEnableSystem
            // 
            this.btnEnableSystem.Location = new System.Drawing.Point(37, 49);
            this.btnEnableSystem.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEnableSystem.Name = "btnEnableSystem";
            this.btnEnableSystem.Size = new System.Drawing.Size(71, 27);
            this.btnEnableSystem.TabIndex = 10;
            this.btnEnableSystem.Text = "Enable";
            this.btnEnableSystem.UseVisualStyleBackColor = true;
            this.btnEnableSystem.Click += new System.EventHandler(this.btnEnableSystem_Click);
            // 
            // lblSystemEnabled
            // 
            this.lblSystemEnabled.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblSystemEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSystemEnabled.Image = global::MPiecControlTester.Properties.Resources.light_yellow;
            this.lblSystemEnabled.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblSystemEnabled.Location = new System.Drawing.Point(6, 26);
            this.lblSystemEnabled.Name = "lblSystemEnabled";
            this.lblSystemEnabled.Size = new System.Drawing.Size(181, 23);
            this.lblSystemEnabled.TabIndex = 0;
            this.lblSystemEnabled.Text = "System Enabled";
            this.lblSystemEnabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisconnect.Location = new System.Drawing.Point(424, 24);
            this.btnDisconnect.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(90, 24);
            this.btnDisconnect.TabIndex = 9;
            this.btnDisconnect.Text = "Disconnect...";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.Location = new System.Drawing.Point(329, 24);
            this.btnConnect.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(90, 24);
            this.btnConnect.TabIndex = 8;
            this.btnConnect.Text = "Connect...";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // txtControllerAddress
            // 
            this.txtControllerAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtControllerAddress.Location = new System.Drawing.Point(184, 25);
            this.txtControllerAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtControllerAddress.Name = "txtControllerAddress";
            this.txtControllerAddress.Size = new System.Drawing.Size(126, 23);
            this.txtControllerAddress.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(182, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Controller IP Address";
            // 
            // btnEditConfiguration
            // 
            this.btnEditConfiguration.Location = new System.Drawing.Point(11, 135);
            this.btnEditConfiguration.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEditConfiguration.Name = "btnEditConfiguration";
            this.btnEditConfiguration.Size = new System.Drawing.Size(126, 27);
            this.btnEditConfiguration.TabIndex = 10;
            this.btnEditConfiguration.Text = "Edit Configuration";
            this.btnEditConfiguration.UseVisualStyleBackColor = true;
            this.btnEditConfiguration.Click += new System.EventHandler(this.btnEditConfiguration_Click);
            // 
            // btnNewConfiguration
            // 
            this.btnNewConfiguration.Location = new System.Drawing.Point(11, 81);
            this.btnNewConfiguration.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnNewConfiguration.Name = "btnNewConfiguration";
            this.btnNewConfiguration.Size = new System.Drawing.Size(126, 27);
            this.btnNewConfiguration.TabIndex = 11;
            this.btnNewConfiguration.Text = "New Configuration";
            this.btnNewConfiguration.UseVisualStyleBackColor = true;
            this.btnNewConfiguration.Click += new System.EventHandler(this.btnNewConfiguration_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.btnLoadConfig);
            this.groupBox1.Controls.Add(this.btnEditConfiguration);
            this.groupBox1.Controls.Add(this.btnNewConfiguration);
            this.groupBox1.Controls.Add(this.btnSaveConfig);
            this.groupBox1.Location = new System.Drawing.Point(6, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(160, 643);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Controller Configuration";
            // 
            // frmTestControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1282, 661);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpController);
            this.Controls.Add(this.btnDisconnect);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.txtControllerAddress);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "frmTestControlPanel";
            this.Text = "Yaskawa MP Control";
            this.grpController.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSaveConfig;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btnLoadConfig;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox grpController;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.TextBox txtControllerAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSystemEnabled;
        private System.Windows.Forms.Button btnDisableSystem;
        private System.Windows.Forms.Button btnEnableSystem;
        private System.Windows.Forms.FlowLayoutPanel flowAxes;
        private System.Windows.Forms.Button btnClearAllAlarms;
        private System.Windows.Forms.Button btnStopAllMotion;
        private System.Windows.Forms.Button btnEditConfiguration;
        private System.Windows.Forms.Button btnNewConfiguration;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnWarmRestart;
    }
}

