﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MPiecFramework;
using MPiecControlTester.Properties;

namespace MPiecControlTester
{
    public partial class ctlAxisStatus : UserControl
    {
        private MPiecAxis Axis;
        private IMPiec Controller;

        public ctlAxisStatus(MPiecAxis AxisInfo, IMPiec Controller)
        {
            InitializeComponent();
            Axis = AxisInfo;
            this.Controller = Controller;
            ResetAxisControls();
        }

        public void SetNewAxis(MPiecAxis NewAxis)
        {
            Axis = NewAxis;
            ResetAxisControls();
        }

        public void UpdateStatus(Label label, MPiecVariable variable, Dictionary<string, dynamic> Vals = null, bool Inverse = false)
        {
            if (variable == null)
                label.Image = Resources.light_yellow;
            else
            {
                bool IsTrue = variable.IsTrue(Vals);
                if (Inverse)
                    IsTrue = !IsTrue;
                label.Image = (IsTrue ? Resources.light_green : Resources.light_red);
            }
        }
        public void UpdateStatus(Button button, MPiecVariable variable, Dictionary<string, dynamic> Vals = null, bool Inverse = false)
        {
            if (variable == null)
                button.Enabled = false;
            else
            {
                bool IsTrue = variable.IsTrue(Vals);
                if (Inverse)
                    IsTrue = !IsTrue;
                button.Enabled = IsTrue;
            }
        }

        public void UpdateStatus(Dictionary<string, dynamic> Vals)
        {
            if (Vals == null)
                return;

            UpdateStatus(lblEnabled, Axis.isEnabled, Vals);

            UpdateStatus(lblAlarm, Axis.isAlarmed, Vals, true);
            UpdateStatus(btnClearAlarm, Axis.isAlarmed, Vals);

            UpdateStatus(lblAtHome, Axis.isAtHome, Vals);
            UpdateStatus(lblAtTarget, Axis.isAtTarget, Vals);
            UpdateStatus(lblInMotion, Axis.isMovingToTarget, Vals);
            UpdateStatus(lblHoming, Axis.isHoming, Vals);
            UpdateStatus(lblJogging, Axis.isJogging, Vals);

            if (Vals.Keys.Contains(Axis.CurrentPosition.DeviceName))
                txtCurrentPosition.Text = ((double)Convert.ToDouble(Vals[Axis.CurrentPosition.DeviceName])).ToString("F5");
            if (Vals.Keys.Contains(Axis.CurrentPosition.Name))
                txtCurrentPosition.Text = ((double)Convert.ToDouble(Vals[Axis.CurrentPosition.Name])).ToString("F5");

            if (Axis.CurrentSpeed != null)
            {
                if (Vals.Keys.Contains(Axis.CurrentSpeed.DeviceName))
                    txtCurrentSpeed.Text = ((double)Convert.ToDouble(Vals[Axis.CurrentSpeed.DeviceName])).ToString("F5");
                if (Vals.Keys.Contains(Axis.CurrentSpeed.Name))
                    txtCurrentSpeed.Text = ((double)Convert.ToDouble(Vals[Axis.CurrentSpeed.Name])).ToString("F5");
            }
        }

        public void ResetAxisControls()
        {
            if (Axis == null)
            {
                grpAxis.Text = "(Undefined Axis)";
                UpdateStatus(lblEnabled, null);
                btnEnable.Enabled = btnDisable.Enabled = false;
                UpdateStatus(lblAlarm, null);
                btnClearAlarm.Enabled = false;
                UpdateStatus(lblAtHome, null);
                btnGoHome.Enabled = false;
                UpdateStatus(lblAtTarget, null);
                btnGotoTarget.Enabled = false;
                UpdateStatus(lblInMotion, null);
                txtCurrentPosition.Text = "";
                txtTargetPosition.Text = "";
                btnJogForward.Enabled = false;
                btnJogReverse.Enabled = false;
                txtCurrentSpeed.Text = "";
                btnSetSpeed.Enabled = false;
                btnStopMotion.Enabled = false;
                btnClearStopMotion.Enabled = false;
            }
            else
            {
                grpAxis.Text = Axis.AxisName;
                UpdateStatus(lblEnabled, Axis.isEnabled);
                btnEnable.Enabled = btnDisable.Enabled = Axis.SetEnabled != null;
                UpdateStatus(lblAlarm, Axis.isAlarmed);
                btnClearAlarm.Enabled = Axis.ClearAlarm != null;
                UpdateStatus(lblAtHome, Axis.isAtHome);
                btnGoHome.Enabled = Axis.GoToHome != null;
                UpdateStatus(lblAtTarget, Axis.isAtTarget);
                btnGotoTarget.Enabled = Axis.GoToTarget != null;
                UpdateStatus(lblInMotion, Axis.isMovingToTarget);
                txtCurrentPosition.Text = Axis.CurrentPosition != null ? Axis.CurrentPosition.Value : "";
                txtTargetPosition.Text = Axis.TargetPosition != null ? Axis.TargetPosition.Value : txtTargetPosition.Text;
                btnJogForward.Enabled = Axis.JogForward != null;
                btnJogReverse.Enabled = Axis.JogReverse != null;
                txtCurrentSpeed.Text = Axis.CurrentSpeed != null ? Axis.CurrentSpeed.Value : "";
                btnSetSpeed.Enabled = Axis.SetSpeed != null;
                btnStopMotion.Enabled = Axis.StopMotion != null;
                btnClearStopMotion.Enabled = Axis.StopMotion != null;
            }
        }

        private async void btnEnable_Click(object sender, EventArgs e)
        {
            btnEnable.Enabled = false;
            try
            {
                if ((Axis != null) && (Axis.SetEnabled != null))
                    await Controller.SetVariable(Axis.SetEnabled, true);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                btnEnable.Enabled = true;
            }
        }

        private async void btnDisable_Click(object sender, EventArgs e)
        {
            btnDisable.Enabled = false;
            try
            {
                if ((Axis != null) && (Axis.SetEnabled != null))
                    await Controller.SetVariable(Axis.SetEnabled, false);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                btnDisable.Enabled = true;
            }
        }

        private async void btnResetEncoderPosition_Click(object sender, EventArgs e)
        {
            btnResetEncoderPosition.Enabled = false;
            try
            {
                if ((Axis != null) && (Axis.SetEncoderPosition != null))
                {
                    if (MessageBox.Show("Set Encoder Position", "Reset the encorder zero position?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        await Controller.SetVariable(Axis.SetEncoderPosition, true);
                }
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                btnResetEncoderPosition.Enabled = true;
            }
        }

        private async void btnClearAlarm_Click(object sender, EventArgs e)
        {
            btnClearAlarm.Enabled = false;
            try
            {
                if ((Axis != null) && (Axis.ClearAlarm != null))
                    await Controller.SetVariable(Axis.ClearAlarm, true);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                btnClearAlarm.Enabled = true;
            }
        }

        private async void btnGoHome_Click(object sender, EventArgs e)
        {
            btnGoHome.Enabled = false;
            try
            {
                if ((Axis != null) && (Axis.GoToHome != null))
                    await Controller.SetVariable(Axis.GoToHome, true);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                btnGoHome.Enabled = true;
            }
        }

        private async void btnClearHome_Click(object sender, EventArgs e)
        {
            btnClearHome.Enabled = false;
            try
            {
                if ((Axis != null) && (Axis.GoToHome != null))
                    await Controller.SetVariable(Axis.GoToHome, false);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                btnClearHome.Enabled = true;
            }

        }


        private async void btnStopMotion_Click(object sender, EventArgs e)
        {
            btnStopMotion.Enabled = false;
            try
            {
                if ((Axis != null) && (Axis.StopMotion != null))
                    await Controller.SetVariable(Axis.StopMotion, true);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                btnStopMotion.Enabled = true;
            }
        }

        private async void btnClearStopMotion_Click(object sender, EventArgs e)
        {
            btnStopMotion.Enabled = false;
            try
            {
                if ((Axis != null) && (Axis.StopMotion != null))
                    await Controller.SetVariable(Axis.StopMotion, false);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                btnStopMotion.Enabled = true;
            }
        }

        private async void btnClearAll_Click(object sender, EventArgs e)
        {
            btnStopMotion.Enabled = false;
            try
            {
                if ((Axis != null) && (Axis.GoToTarget != null))
                    await Controller.SetVariable(Axis.GoToTarget, false);
                if ((Axis != null) && (Axis.JogForward != null))
                    await Controller.SetVariable(Axis.JogForward, false);
                if ((Axis != null) && (Axis.JogReverse != null))
                    await Controller.SetVariable(Axis.JogReverse, false);
                if ((Axis != null) && (Axis.GoToHome != null))
                    await Controller.SetVariable(Axis.GoToHome, false);
                if ((Axis != null) && (Axis.StopMotion != null))
                    await Controller.SetVariable(Axis.StopMotion, false);
                if ((Axis != null) && (Axis.ClearAlarm != null))
                    await Controller.SetVariable(Axis.ClearAlarm, true);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                btnStopMotion.Enabled = true;
            }

        }


        private async void btnGotoTarget_Click(object sender, EventArgs e)
        {
            btnGotoTarget.Enabled = false;
            try
            {
                if ((Axis != null) && (Axis.GoToTarget != null) && (Axis.TargetPosition != null))
                {
                    //Axis.TargetPosition.Value =  Axis.TargetPosition.VariableType.Parse(txtTargetPosition.Text);
                    Axis.TargetPosition.Value = Convert.ToDouble(txtTargetPosition.Text);
                    await Controller.WriteVariables(new List<MPiecVariable>() { Axis.TargetPosition });
                    await Controller.SetVariable(Axis.GoToTarget, true);

                }
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                btnGotoTarget.Enabled = true;
            }
        }

        private async void btnEndMove_Click(object sender, EventArgs e)
        {
            btnEndMove.Enabled = false;
            try
            {
                if ((Axis != null) && (Axis.GoToTarget != null) && (Axis.TargetPosition != null))
                {
                    await Controller.SetVariable(Axis.GoToTarget, false);
                }
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                btnEndMove.Enabled = true;
            }
        }

        private async void btnSetSpeed_Click(object sender, EventArgs e)
        {
            btnSetSpeed.Enabled = false;
            try
            {
                if ((Axis != null) && (Axis.SetSpeed != null))
                {
                    Axis.SetSpeed.Value = Axis.TargetPosition.VariableType.Parse(txtTargetSpeed.Text);
                    await Controller.WriteVariables(new List<MPiecVariable>() { Axis.SetSpeed });
                }
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                btnSetSpeed.Enabled = true;
            }

        }

        private async void btnJogReverse_MouseDown(object sender, MouseEventArgs e)
        {
            if ((Axis == null) || (Axis.JogReverse == null))
                return;

            await Controller.SetVariable(Axis.JogReverse, true);
        }

        private async void btnJogReverse_MouseUp(object sender, MouseEventArgs e)
        {
            if ((Axis == null) || (Axis.JogReverse == null))
                return;

            await Controller.SetVariable(Axis.JogReverse, false);
        }

        private async void btnJogForward_MouseDown(object sender, MouseEventArgs e)
        {
            if ((Axis == null) || (Axis.JogForward == null))
                return;

            await Controller.SetVariable(Axis.JogForward, true);
        }

        private async void btnJogForward_MouseUp(object sender, MouseEventArgs e)
        {
            if ((Axis == null) || (Axis.JogForward == null))
                return;

            await Controller.SetVariable(Axis.JogForward, false);
        }

    }
}
