﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MPiecFramework.Events;
using MPiecFramework;

namespace MPiecControlTester
{
    public partial class ctlConfigVariable : UserControl
    {
        public ctlConfigVariable()
        {
            InitializeComponent();

            cmbLocation.Items.AddRange(new string[] {
                "GlobalVariables",
                "InstanceVariables"
            });

            cmbType.Items.AddRange(Enum.GetNames(typeof(MotionWorksType)));
        }

        public ctlConfigVariable(string Description) : this()
        {
            grpInfo.Text = Description;
        }

        public ctlConfigVariable(string Description, MPiecVariable Variable) : this(Description)
        {
            SetVariable(Variable);
        }

        public void SetVariable(MPiecVariable Variable)
        {
            cmbLocation.SelectedIndex = (Variable == null) ? -1 : cmbLocation.Items.IndexOf(Variable.Location);
            cmbType.SelectedIndex = (Variable == null) ? -1 : cmbType.Items.IndexOf(Variable.VariableType.ToString());
            txtConfigName.Text = (Variable == null) ? "" : Variable.Name;
        }

        public MPiecVariable GetVariable()
        {
            if (string.IsNullOrEmpty(txtConfigName.Text) || cmbLocation.SelectedIndex < 0 || cmbType.SelectedIndex < 0)
                return null;

            return new MPiecVariable(cmbLocation.Text, txtConfigName.Text, (MotionWorksType)Enum.Parse(typeof(MotionWorksType), cmbType.Text));
        }

        public string Description { get { return grpInfo.Text; } }

        [Browsable(true), EditorBrowsable(EditorBrowsableState.Always), Bindable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public override string Text
        {
            get { return grpInfo.Text; }
            set { grpInfo.Text = value; }
        }
    }
}
