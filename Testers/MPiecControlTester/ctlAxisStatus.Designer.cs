﻿namespace MPiecControlTester
{
    partial class ctlAxisStatus
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctlAxisStatus));
            this.grpAxis = new System.Windows.Forms.GroupBox();
            this.btnClearStopMotion = new System.Windows.Forms.Button();
            this.lblHoming = new System.Windows.Forms.Label();
            this.lblJogging = new System.Windows.Forms.Label();
            this.btnEndMove = new System.Windows.Forms.Button();
            this.btnStopMotion = new System.Windows.Forms.Button();
            this.txtCurrentSpeed = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTargetSpeed = new System.Windows.Forms.TextBox();
            this.btnSetSpeed = new System.Windows.Forms.Button();
            this.lblInMotion = new System.Windows.Forms.Label();
            this.btnResetEncoderPosition = new System.Windows.Forms.Button();
            this.btnJogReverse = new System.Windows.Forms.Button();
            this.btnJogForward = new System.Windows.Forms.Button();
            this.txtTargetPosition = new System.Windows.Forms.TextBox();
            this.btnGotoTarget = new System.Windows.Forms.Button();
            this.btnGoHome = new System.Windows.Forms.Button();
            this.lblAtTarget = new System.Windows.Forms.Label();
            this.lblAtHome = new System.Windows.Forms.Label();
            this.txtCurrentPosition = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClearAlarm = new System.Windows.Forms.Button();
            this.lblAlarm = new System.Windows.Forms.Label();
            this.btnDisable = new System.Windows.Forms.Button();
            this.btnEnable = new System.Windows.Forms.Button();
            this.lblEnabled = new System.Windows.Forms.Label();
            this.btnClearHome = new System.Windows.Forms.Button();
            this.btnClearAll = new System.Windows.Forms.Button();
            this.grpAxis.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpAxis
            // 
            this.grpAxis.Controls.Add(this.btnClearAll);
            this.grpAxis.Controls.Add(this.btnClearHome);
            this.grpAxis.Controls.Add(this.btnClearStopMotion);
            this.grpAxis.Controls.Add(this.lblHoming);
            this.grpAxis.Controls.Add(this.lblJogging);
            this.grpAxis.Controls.Add(this.btnEndMove);
            this.grpAxis.Controls.Add(this.btnStopMotion);
            this.grpAxis.Controls.Add(this.txtCurrentSpeed);
            this.grpAxis.Controls.Add(this.label2);
            this.grpAxis.Controls.Add(this.txtTargetSpeed);
            this.grpAxis.Controls.Add(this.btnSetSpeed);
            this.grpAxis.Controls.Add(this.lblInMotion);
            this.grpAxis.Controls.Add(this.btnResetEncoderPosition);
            this.grpAxis.Controls.Add(this.btnJogReverse);
            this.grpAxis.Controls.Add(this.btnJogForward);
            this.grpAxis.Controls.Add(this.txtTargetPosition);
            this.grpAxis.Controls.Add(this.btnGotoTarget);
            this.grpAxis.Controls.Add(this.btnGoHome);
            this.grpAxis.Controls.Add(this.lblAtTarget);
            this.grpAxis.Controls.Add(this.lblAtHome);
            this.grpAxis.Controls.Add(this.txtCurrentPosition);
            this.grpAxis.Controls.Add(this.label1);
            this.grpAxis.Controls.Add(this.btnClearAlarm);
            this.grpAxis.Controls.Add(this.lblAlarm);
            this.grpAxis.Controls.Add(this.btnDisable);
            this.grpAxis.Controls.Add(this.btnEnable);
            this.grpAxis.Controls.Add(this.lblEnabled);
            this.grpAxis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpAxis.Location = new System.Drawing.Point(0, 0);
            this.grpAxis.Name = "grpAxis";
            this.grpAxis.Size = new System.Drawing.Size(609, 159);
            this.grpAxis.TabIndex = 0;
            this.grpAxis.TabStop = false;
            this.grpAxis.Text = "Axis Status";
            // 
            // btnClearStopMotion
            // 
            this.btnClearStopMotion.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClearStopMotion.Location = new System.Drawing.Point(517, 131);
            this.btnClearStopMotion.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnClearStopMotion.Name = "btnClearStopMotion";
            this.btnClearStopMotion.Size = new System.Drawing.Size(85, 22);
            this.btnClearStopMotion.TabIndex = 36;
            this.btnClearStopMotion.Text = "Clear Stop";
            this.btnClearStopMotion.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnClearStopMotion.UseVisualStyleBackColor = true;
            this.btnClearStopMotion.Click += new System.EventHandler(this.btnClearStopMotion_Click);
            // 
            // lblHoming
            // 
            this.lblHoming.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblHoming.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoming.Image = global::MPiecControlTester.Properties.Resources.light_yellow;
            this.lblHoming.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblHoming.Location = new System.Drawing.Point(406, 16);
            this.lblHoming.Name = "lblHoming";
            this.lblHoming.Size = new System.Drawing.Size(93, 23);
            this.lblHoming.TabIndex = 35;
            this.lblHoming.Text = "Homing";
            this.lblHoming.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJogging
            // 
            this.lblJogging.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblJogging.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJogging.Image = global::MPiecControlTester.Properties.Resources.light_yellow;
            this.lblJogging.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblJogging.Location = new System.Drawing.Point(149, 128);
            this.lblJogging.Name = "lblJogging";
            this.lblJogging.Size = new System.Drawing.Size(69, 23);
            this.lblJogging.TabIndex = 34;
            this.lblJogging.Text = "Jogging";
            this.lblJogging.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnEndMove
            // 
            this.btnEndMove.Location = new System.Drawing.Point(424, 99);
            this.btnEndMove.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEndMove.Name = "btnEndMove";
            this.btnEndMove.Size = new System.Drawing.Size(75, 21);
            this.btnEndMove.TabIndex = 33;
            this.btnEndMove.Text = "End Move";
            this.btnEndMove.UseVisualStyleBackColor = true;
            this.btnEndMove.Click += new System.EventHandler(this.btnEndMove_Click);
            // 
            // btnStopMotion
            // 
            this.btnStopMotion.Image = ((System.Drawing.Image)(resources.GetObject("btnStopMotion.Image")));
            this.btnStopMotion.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnStopMotion.Location = new System.Drawing.Point(517, 18);
            this.btnStopMotion.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnStopMotion.Name = "btnStopMotion";
            this.btnStopMotion.Size = new System.Drawing.Size(85, 109);
            this.btnStopMotion.TabIndex = 32;
            this.btnStopMotion.Text = "Axis Motion";
            this.btnStopMotion.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnStopMotion.UseVisualStyleBackColor = true;
            this.btnStopMotion.Click += new System.EventHandler(this.btnStopMotion_Click);
            // 
            // txtCurrentSpeed
            // 
            this.txtCurrentSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrentSpeed.Location = new System.Drawing.Point(115, 70);
            this.txtCurrentSpeed.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtCurrentSpeed.Name = "txtCurrentSpeed";
            this.txtCurrentSpeed.ReadOnly = true;
            this.txtCurrentSpeed.Size = new System.Drawing.Size(104, 23);
            this.txtCurrentSpeed.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(46, 70);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 23);
            this.label2.TabIndex = 30;
            this.label2.Text = "Speed";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTargetSpeed
            // 
            this.txtTargetSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTargetSpeed.Location = new System.Drawing.Point(223, 70);
            this.txtTargetSpeed.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtTargetSpeed.Name = "txtTargetSpeed";
            this.txtTargetSpeed.Size = new System.Drawing.Size(104, 23);
            this.txtTargetSpeed.TabIndex = 29;
            // 
            // btnSetSpeed
            // 
            this.btnSetSpeed.Location = new System.Drawing.Point(331, 71);
            this.btnSetSpeed.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSetSpeed.Name = "btnSetSpeed";
            this.btnSetSpeed.Size = new System.Drawing.Size(89, 21);
            this.btnSetSpeed.TabIndex = 28;
            this.btnSetSpeed.Text = "Set Speed";
            this.btnSetSpeed.UseVisualStyleBackColor = true;
            this.btnSetSpeed.Click += new System.EventHandler(this.btnSetSpeed_Click);
            // 
            // lblInMotion
            // 
            this.lblInMotion.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblInMotion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInMotion.Image = global::MPiecControlTester.Properties.Resources.light_yellow;
            this.lblInMotion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblInMotion.Location = new System.Drawing.Point(413, 129);
            this.lblInMotion.Name = "lblInMotion";
            this.lblInMotion.Size = new System.Drawing.Size(86, 23);
            this.lblInMotion.TabIndex = 27;
            this.lblInMotion.Text = "In Motion";
            this.lblInMotion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnResetEncoderPosition
            // 
            this.btnResetEncoderPosition.Location = new System.Drawing.Point(0, 137);
            this.btnResetEncoderPosition.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnResetEncoderPosition.Name = "btnResetEncoderPosition";
            this.btnResetEncoderPosition.Size = new System.Drawing.Size(118, 20);
            this.btnResetEncoderPosition.TabIndex = 26;
            this.btnResetEncoderPosition.Text = "Set Encoder Position";
            this.btnResetEncoderPosition.UseVisualStyleBackColor = true;
            this.btnResetEncoderPosition.Click += new System.EventHandler(this.btnResetEncoderPosition_Click);
            // 
            // btnJogReverse
            // 
            this.btnJogReverse.Location = new System.Drawing.Point(223, 129);
            this.btnJogReverse.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnJogReverse.Name = "btnJogReverse";
            this.btnJogReverse.Size = new System.Drawing.Size(36, 22);
            this.btnJogReverse.TabIndex = 25;
            this.btnJogReverse.Text = "<<";
            this.btnJogReverse.UseVisualStyleBackColor = true;
            this.btnJogReverse.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnJogReverse_MouseDown);
            this.btnJogReverse.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnJogReverse_MouseUp);
            // 
            // btnJogForward
            // 
            this.btnJogForward.Location = new System.Drawing.Point(263, 129);
            this.btnJogForward.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnJogForward.Name = "btnJogForward";
            this.btnJogForward.Size = new System.Drawing.Size(36, 22);
            this.btnJogForward.TabIndex = 24;
            this.btnJogForward.Text = ">>";
            this.btnJogForward.UseVisualStyleBackColor = true;
            this.btnJogForward.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnJogForward_MouseDown);
            this.btnJogForward.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnJogForward_MouseUp);
            // 
            // txtTargetPosition
            // 
            this.txtTargetPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTargetPosition.Location = new System.Drawing.Point(223, 99);
            this.txtTargetPosition.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtTargetPosition.Name = "txtTargetPosition";
            this.txtTargetPosition.Size = new System.Drawing.Size(104, 23);
            this.txtTargetPosition.TabIndex = 23;
            // 
            // btnGotoTarget
            // 
            this.btnGotoTarget.Location = new System.Drawing.Point(331, 99);
            this.btnGotoTarget.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnGotoTarget.Name = "btnGotoTarget";
            this.btnGotoTarget.Size = new System.Drawing.Size(89, 21);
            this.btnGotoTarget.TabIndex = 22;
            this.btnGotoTarget.Text = "Start Move";
            this.btnGotoTarget.UseVisualStyleBackColor = true;
            this.btnGotoTarget.Click += new System.EventHandler(this.btnGotoTarget_Click);
            // 
            // btnGoHome
            // 
            this.btnGoHome.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGoHome.Location = new System.Drawing.Point(321, 14);
            this.btnGoHome.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnGoHome.Name = "btnGoHome";
            this.btnGoHome.Size = new System.Drawing.Size(80, 27);
            this.btnGoHome.TabIndex = 21;
            this.btnGoHome.Text = "Go To Home";
            this.btnGoHome.UseVisualStyleBackColor = true;
            this.btnGoHome.Click += new System.EventHandler(this.btnGoHome_Click);
            // 
            // lblAtTarget
            // 
            this.lblAtTarget.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblAtTarget.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtTarget.Image = global::MPiecControlTester.Properties.Resources.light_yellow;
            this.lblAtTarget.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAtTarget.Location = new System.Drawing.Point(328, 129);
            this.lblAtTarget.Name = "lblAtTarget";
            this.lblAtTarget.Size = new System.Drawing.Size(79, 23);
            this.lblAtTarget.TabIndex = 20;
            this.lblAtTarget.Text = "At Target";
            this.lblAtTarget.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAtHome
            // 
            this.lblAtHome.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAtHome.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblAtHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtHome.Image = global::MPiecControlTester.Properties.Resources.light_yellow;
            this.lblAtHome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAtHome.Location = new System.Drawing.Point(406, 41);
            this.lblAtHome.Name = "lblAtHome";
            this.lblAtHome.Size = new System.Drawing.Size(93, 23);
            this.lblAtHome.TabIndex = 19;
            this.lblAtHome.Text = "At Home";
            this.lblAtHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCurrentPosition
            // 
            this.txtCurrentPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrentPosition.Location = new System.Drawing.Point(115, 98);
            this.txtCurrentPosition.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtCurrentPosition.Name = "txtCurrentPosition";
            this.txtCurrentPosition.ReadOnly = true;
            this.txtCurrentPosition.Size = new System.Drawing.Size(104, 23);
            this.txtCurrentPosition.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(46, 98);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 23);
            this.label1.TabIndex = 17;
            this.label1.Text = "Position";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnClearAlarm
            // 
            this.btnClearAlarm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearAlarm.Location = new System.Drawing.Point(236, 39);
            this.btnClearAlarm.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnClearAlarm.Name = "btnClearAlarm";
            this.btnClearAlarm.Size = new System.Drawing.Size(71, 27);
            this.btnClearAlarm.TabIndex = 16;
            this.btnClearAlarm.Text = "Clear Alarm";
            this.btnClearAlarm.UseVisualStyleBackColor = true;
            this.btnClearAlarm.Click += new System.EventHandler(this.btnClearAlarm_Click);
            // 
            // lblAlarm
            // 
            this.lblAlarm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAlarm.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblAlarm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlarm.Image = global::MPiecControlTester.Properties.Resources.light_yellow;
            this.lblAlarm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAlarm.Location = new System.Drawing.Point(210, 14);
            this.lblAlarm.Name = "lblAlarm";
            this.lblAlarm.Size = new System.Drawing.Size(98, 23);
            this.lblAlarm.TabIndex = 15;
            this.lblAlarm.Text = "Run State";
            this.lblAlarm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDisable
            // 
            this.btnDisable.Location = new System.Drawing.Point(139, 37);
            this.btnDisable.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDisable.Name = "btnDisable";
            this.btnDisable.Size = new System.Drawing.Size(60, 27);
            this.btnDisable.TabIndex = 14;
            this.btnDisable.Text = "Disable";
            this.btnDisable.UseVisualStyleBackColor = true;
            this.btnDisable.Click += new System.EventHandler(this.btnDisable_Click);
            // 
            // btnEnable
            // 
            this.btnEnable.Location = new System.Drawing.Point(78, 37);
            this.btnEnable.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEnable.Name = "btnEnable";
            this.btnEnable.Size = new System.Drawing.Size(57, 27);
            this.btnEnable.TabIndex = 13;
            this.btnEnable.Text = "Enable";
            this.btnEnable.UseVisualStyleBackColor = true;
            this.btnEnable.Click += new System.EventHandler(this.btnEnable_Click);
            // 
            // lblEnabled
            // 
            this.lblEnabled.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnabled.Image = global::MPiecControlTester.Properties.Resources.light_yellow;
            this.lblEnabled.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblEnabled.Location = new System.Drawing.Point(18, 14);
            this.lblEnabled.Name = "lblEnabled";
            this.lblEnabled.Size = new System.Drawing.Size(181, 23);
            this.lblEnabled.TabIndex = 12;
            this.lblEnabled.Text = "Axis Enabled";
            this.lblEnabled.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnClearHome
            // 
            this.btnClearHome.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClearHome.Location = new System.Drawing.Point(321, 44);
            this.btnClearHome.Margin = new System.Windows.Forms.Padding(2);
            this.btnClearHome.Name = "btnClearHome";
            this.btnClearHome.Size = new System.Drawing.Size(80, 22);
            this.btnClearHome.TabIndex = 37;
            this.btnClearHome.Text = "Clear Home";
            this.btnClearHome.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnClearHome.UseVisualStyleBackColor = true;
            this.btnClearHome.Click += new System.EventHandler(this.btnClearHome_Click);
            // 
            // btnClearAll
            // 
            this.btnClearAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearAll.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClearAll.Location = new System.Drawing.Point(5, 37);
            this.btnClearAll.Margin = new System.Windows.Forms.Padding(2);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(69, 27);
            this.btnClearAll.TabIndex = 38;
            this.btnClearAll.Text = "Clear All";
            this.btnClearAll.UseVisualStyleBackColor = true;
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // ctlAxisStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grpAxis);
            this.Name = "ctlAxisStatus";
            this.Size = new System.Drawing.Size(609, 159);
            this.grpAxis.ResumeLayout(false);
            this.grpAxis.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpAxis;
        private System.Windows.Forms.Button btnDisable;
        private System.Windows.Forms.Button btnEnable;
        private System.Windows.Forms.Label lblEnabled;
        private System.Windows.Forms.Button btnClearAlarm;
        private System.Windows.Forms.Label lblAlarm;
        private System.Windows.Forms.Label lblAtTarget;
        private System.Windows.Forms.Label lblAtHome;
        private System.Windows.Forms.TextBox txtCurrentPosition;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTargetPosition;
        private System.Windows.Forms.Button btnGotoTarget;
        private System.Windows.Forms.Button btnGoHome;
        private System.Windows.Forms.Button btnResetEncoderPosition;
        private System.Windows.Forms.Button btnJogReverse;
        private System.Windows.Forms.Button btnJogForward;
        private System.Windows.Forms.TextBox txtCurrentSpeed;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTargetSpeed;
        private System.Windows.Forms.Button btnSetSpeed;
        private System.Windows.Forms.Label lblInMotion;
        private System.Windows.Forms.Button btnStopMotion;
        private System.Windows.Forms.Button btnEndMove;
        private System.Windows.Forms.Button btnClearStopMotion;
        private System.Windows.Forms.Label lblHoming;
        private System.Windows.Forms.Label lblJogging;
        private System.Windows.Forms.Button btnClearHome;
        private System.Windows.Forms.Button btnClearAll;
    }
}
