﻿namespace MPiecControlTester
{
    partial class frmEditConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditConfiguration));
            this.txtConfigName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddAxis = new System.Windows.Forms.Button();
            this.btnRemoveAxis = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnRevert = new System.Windows.Forms.Button();
            this.ctlStopAllMotion = new MPiecControlTester.ctlConfigVariable();
            this.ctlClearAllAlarms = new MPiecControlTester.ctlConfigVariable();
            this.ctlSetEnabled = new MPiecControlTester.ctlConfigVariable();
            this.ctlIsEnabled = new MPiecControlTester.ctlConfigVariable();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ctlAxisConfig1 = new MPiecControlTester.ctlAxisConfig();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.ctlJogDeceleration = new MPiecControlTester.ctlConfigVariable();
            this.ctlJogSpeed = new MPiecControlTester.ctlConfigVariable();
            this.ctljogAcceleration = new MPiecControlTester.ctlConfigVariable();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtConfigName
            // 
            this.txtConfigName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfigName.Location = new System.Drawing.Point(8, 45);
            this.txtConfigName.Margin = new System.Windows.Forms.Padding(2);
            this.txtConfigName.Name = "txtConfigName";
            this.txtConfigName.Size = new System.Drawing.Size(290, 23);
            this.txtConfigName.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(290, 19);
            this.label1.TabIndex = 8;
            this.label1.Text = "System Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAddAxis
            // 
            this.btnAddAxis.Location = new System.Drawing.Point(16, 22);
            this.btnAddAxis.Name = "btnAddAxis";
            this.btnAddAxis.Size = new System.Drawing.Size(109, 23);
            this.btnAddAxis.TabIndex = 15;
            this.btnAddAxis.Text = "Add Axis";
            this.btnAddAxis.UseVisualStyleBackColor = true;
            this.btnAddAxis.Click += new System.EventHandler(this.btnAddAxis_Click);
            // 
            // btnRemoveAxis
            // 
            this.btnRemoveAxis.Location = new System.Drawing.Point(131, 22);
            this.btnRemoveAxis.Name = "btnRemoveAxis";
            this.btnRemoveAxis.Size = new System.Drawing.Size(109, 23);
            this.btnRemoveAxis.TabIndex = 16;
            this.btnRemoveAxis.Text = "Remove Axis";
            this.btnRemoveAxis.UseVisualStyleBackColor = true;
            this.btnRemoveAxis.Click += new System.EventHandler(this.btnRemoveAxis_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Location = new System.Drawing.Point(1101, 877);
            this.btnOK.Margin = new System.Windows.Forms.Padding(2);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(152, 35);
            this.btnOK.TabIndex = 17;
            this.btnOK.Text = "OK - Accept Changes";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnRevert
            // 
            this.btnRevert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRevert.Location = new System.Drawing.Point(173, 877);
            this.btnRevert.Margin = new System.Windows.Forms.Padding(2);
            this.btnRevert.Name = "btnRevert";
            this.btnRevert.Size = new System.Drawing.Size(141, 35);
            this.btnRevert.TabIndex = 18;
            this.btnRevert.Text = "Revert Changes";
            this.btnRevert.UseVisualStyleBackColor = true;
            this.btnRevert.Click += new System.EventHandler(this.btnRevert_Click);
            // 
            // ctlStopAllMotion
            // 
            this.ctlStopAllMotion.Location = new System.Drawing.Point(11, 409);
            this.ctlStopAllMotion.Margin = new System.Windows.Forms.Padding(6);
            this.ctlStopAllMotion.Name = "ctlStopAllMotion";
            this.ctlStopAllMotion.Size = new System.Drawing.Size(287, 100);
            this.ctlStopAllMotion.TabIndex = 13;
            this.ctlStopAllMotion.Text = "stopAllMotion";
            // 
            // ctlClearAllAlarms
            // 
            this.ctlClearAllAlarms.Location = new System.Drawing.Point(11, 303);
            this.ctlClearAllAlarms.Margin = new System.Windows.Forms.Padding(6);
            this.ctlClearAllAlarms.Name = "ctlClearAllAlarms";
            this.ctlClearAllAlarms.Size = new System.Drawing.Size(287, 100);
            this.ctlClearAllAlarms.TabIndex = 12;
            this.ctlClearAllAlarms.Text = "clearAllAlarms";
            // 
            // ctlSetEnabled
            // 
            this.ctlSetEnabled.Location = new System.Drawing.Point(11, 197);
            this.ctlSetEnabled.Margin = new System.Windows.Forms.Padding(6);
            this.ctlSetEnabled.Name = "ctlSetEnabled";
            this.ctlSetEnabled.Size = new System.Drawing.Size(287, 100);
            this.ctlSetEnabled.TabIndex = 11;
            this.ctlSetEnabled.Text = "setEnabled";
            // 
            // ctlIsEnabled
            // 
            this.ctlIsEnabled.Location = new System.Drawing.Point(11, 91);
            this.ctlIsEnabled.Margin = new System.Windows.Forms.Padding(6);
            this.ctlIsEnabled.Name = "ctlIsEnabled";
            this.ctlIsEnabled.Size = new System.Drawing.Size(287, 100);
            this.ctlIsEnabled.TabIndex = 10;
            this.ctlIsEnabled.Text = "isEnabled";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(16, 51);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(915, 799);
            this.tabControl1.TabIndex = 14;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ctlAxisConfig1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(907, 773);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ctlAxisConfig1
            // 
            this.ctlAxisConfig1.AutoScroll = true;
            this.ctlAxisConfig1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlAxisConfig1.Location = new System.Drawing.Point(3, 3);
            this.ctlAxisConfig1.Margin = new System.Windows.Forms.Padding(6);
            this.ctlAxisConfig1.Name = "ctlAxisConfig1";
            this.ctlAxisConfig1.Size = new System.Drawing.Size(901, 767);
            this.ctlAxisConfig1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.ctljogAcceleration);
            this.groupBox1.Controls.Add(this.ctlJogSpeed);
            this.groupBox1.Controls.Add(this.ctlJogDeceleration);
            this.groupBox1.Controls.Add(this.ctlIsEnabled);
            this.groupBox1.Controls.Add(this.ctlSetEnabled);
            this.groupBox1.Controls.Add(this.ctlClearAllAlarms);
            this.groupBox1.Controls.Add(this.ctlStopAllMotion);
            this.groupBox1.Controls.Add(this.txtConfigName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(11, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(303, 853);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "System";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnAddAxis);
            this.groupBox2.Controls.Add(this.btnRemoveAxis);
            this.groupBox2.Controls.Add(this.tabControl1);
            this.groupBox2.Location = new System.Drawing.Point(322, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(937, 856);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Axes";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(11, 877);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(152, 35);
            this.btnCancel.TabIndex = 21;
            this.btnCancel.Text = "Cancel - Close";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // ctlJogDeceleration
            // 
            this.ctlJogDeceleration.Location = new System.Drawing.Point(9, 740);
            this.ctlJogDeceleration.Margin = new System.Windows.Forms.Padding(6);
            this.ctlJogDeceleration.Name = "ctlJogDeceleration";
            this.ctlJogDeceleration.Size = new System.Drawing.Size(287, 100);
            this.ctlJogDeceleration.TabIndex = 14;
            this.ctlJogDeceleration.Text = "jogDeceleration";
            // 
            // ctlJogSpeed
            // 
            this.ctlJogSpeed.Location = new System.Drawing.Point(9, 528);
            this.ctlJogSpeed.Margin = new System.Windows.Forms.Padding(6);
            this.ctlJogSpeed.Name = "ctlJogSpeed";
            this.ctlJogSpeed.Size = new System.Drawing.Size(287, 100);
            this.ctlJogSpeed.TabIndex = 15;
            this.ctlJogSpeed.Text = "jogSpeed";
            // 
            // ctljogAcceleration
            // 
            this.ctljogAcceleration.Location = new System.Drawing.Point(11, 635);
            this.ctljogAcceleration.Margin = new System.Windows.Forms.Padding(6);
            this.ctljogAcceleration.Name = "ctljogAcceleration";
            this.ctljogAcceleration.Size = new System.Drawing.Size(287, 100);
            this.ctljogAcceleration.TabIndex = 16;
            this.ctljogAcceleration.Text = "jogAcceleration";
            // 
            // frmEditConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 928);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnRevert);
            this.Controls.Add(this.btnOK);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmEditConfiguration";
            this.Text = "Yaskawa MP Configuration Editor";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtConfigName;
        private System.Windows.Forms.Label label1;
        private ctlConfigVariable ctlIsEnabled;
        private ctlConfigVariable ctlSetEnabled;
        private ctlConfigVariable ctlClearAllAlarms;
        private ctlConfigVariable ctlStopAllMotion;
        private System.Windows.Forms.Button btnAddAxis;
        private System.Windows.Forms.Button btnRemoveAxis;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnRevert;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private ctlAxisConfig ctlAxisConfig1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCancel;
        private ctlConfigVariable ctljogAcceleration;
        private ctlConfigVariable ctlJogSpeed;
        private ctlConfigVariable ctlJogDeceleration;
    }
}