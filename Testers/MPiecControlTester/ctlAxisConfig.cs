﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MPiecFramework;

namespace MPiecControlTester
{
    public partial class ctlAxisConfig : UserControl
    {
        MPiecAxis axis;

        public ctlAxisConfig()
        {
            InitializeComponent();
        }

        public ctlAxisConfig(MPiecAxis axis) : this()
        {
            SetAxis(axis);
        }

        public void SetAxis(MPiecAxis newAxis)
        {
            this.axis = newAxis;
            txtAxisName.Text = axis.AxisName;
            ctlClearAlarm.SetVariable(axis.ClearAlarm);
            ctlCurrentPosition.SetVariable(axis.CurrentPosition);
            ctlCurrentSpeed.SetVariable(axis.CurrentSpeed);
            ctlGoToHome.SetVariable(axis.GoToHome);
            ctlGoToTarget.SetVariable(axis.GoToTarget);
            ctlIsAlarmed.SetVariable(axis.isAlarmed);
            ctlIsAtHome.SetVariable(axis.isAtHome);
            ctlIsAtTarget.SetVariable(axis.isAtTarget);
            ctlIsEnabled.SetVariable(axis.isEnabled);
            ctlIsMovingToTarget.SetVariable(axis.isMovingToTarget);
            ctlJogSpeed.SetVariable(axis.jogSpeed);
            ctlJogForward.SetVariable(axis.JogForward);
            ctlJogReverse.SetVariable(axis.JogReverse);
            ctlSetEnabled.SetVariable(axis.SetEnabled);
            ctlSetEncoderPosition.SetVariable(axis.SetEncoderPosition);
            ctlSetSpeed.SetVariable(axis.SetSpeed);
            ctlStopMotion.SetVariable(axis.StopMotion);
            ctlTargetPosition.SetVariable(axis.TargetPosition);
            ctlIsJogging.SetVariable(axis.isJogging);
            ctlIsHoming.SetVariable(axis.isHoming);
            ctlIsMoving.SetVariable(axis.isMoving);
            ctlIsHot.SetVariable(axis.isHot);
        }

        public MPiecAxis GetAxis()
        {
            if (axis == null)
                axis = new MPiecAxis();

            axis.AxisName = txtAxisName.Text;
            axis.ClearAlarm = ctlClearAlarm.GetVariable();
            axis.CurrentPosition = ctlCurrentPosition.GetVariable();
            axis.CurrentSpeed = ctlCurrentSpeed.GetVariable();
            axis.GoToHome = ctlGoToHome.GetVariable();
            axis.GoToTarget = ctlGoToTarget.GetVariable();
            axis.isAlarmed = ctlIsAlarmed.GetVariable();
            axis.isAtHome = ctlIsAtHome.GetVariable();
            axis.isAtTarget = ctlIsAtTarget.GetVariable();
            axis.isEnabled = ctlIsEnabled.GetVariable();
            axis.isMovingToTarget = ctlIsMovingToTarget.GetVariable();
            axis.JogForward = ctlJogForward.GetVariable();
            axis.JogReverse = ctlJogReverse.GetVariable();
            axis.jogSpeed = ctlJogSpeed.GetVariable();
            axis.SetEnabled = ctlSetEnabled.GetVariable();
            axis.SetEncoderPosition = ctlSetEncoderPosition.GetVariable();
            axis.SetSpeed = ctlSetSpeed.GetVariable();
            axis.StopMotion = ctlStopMotion.GetVariable();
            axis.TargetPosition = ctlTargetPosition.GetVariable();
            axis.isJogging = ctlIsJogging.GetVariable();
            axis.isHoming = ctlIsHoming.GetVariable();
            axis.isMoving = ctlIsMoving.GetVariable();
            axis.isHot = ctlIsHot.GetVariable();

            return axis;
        }
    }
}
