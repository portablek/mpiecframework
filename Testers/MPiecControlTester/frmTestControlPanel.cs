﻿using MPiecControlTester.Properties;
using MPiecFramework;
using MPiecFramework.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MPiecControlTester
{
    public partial class frmTestControlPanel : Form
    {
        private IMPiec Controller = MPiec.GetControllerInterface();
        private MPiecConfiguration Config = null;
        private string WatchSubscription = null;
        private List<ctlAxisStatus> AxisControls = new List<ctlAxisStatus>();

        public frmTestControlPanel()
        {
            InitializeComponent();
            UpdateConfig(null);
        }

        private void UpdateStatus(Label label, MPiecVariable variable, Dictionary<string, dynamic> Vals = null)
        {
            if (variable == null)
                label.Image = Resources.light_yellow;
            else
            {
                label.Image = (variable.IsTrue(Vals) ? Resources.light_green : Resources.light_red);
            }
        }

        private void UpdateConfig(MPiecConfiguration newConfig)
        {
            if (newConfig == null)
            {
                grpController.Text = "(No Configuration Loaded)";
                grpController.Enabled = false;
                flowAxes.Controls.Clear();
                AxisControls.Clear();
                btnSaveConfig.Enabled = false;
                btnConnect.Enabled = btnDisconnect.Enabled = false;
            }
            else
            {
                Config = newConfig;
                grpController.Text = Config.Name;
                
                flowAxes.SuspendLayout();
                flowAxes.Controls.Clear();
                foreach (MPiecAxis axis in Config.Axes)
                {
                    ctlAxisStatus ctlStatus = new ctlAxisStatus(axis, Controller);
                    AxisControls.Add(ctlStatus);
                    flowAxes.Controls.Add(ctlStatus);
                }
                flowAxes.ResumeLayout();

                btnSaveConfig.Enabled = true;
                btnConnect.Enabled = btnDisconnect.Enabled = true;
            }

            UpdateSystemConfig(newConfig);
        }

        private void UpdateSystemConfig(MPiecConfiguration newConfig)
        {
            if (newConfig == null)
            {
                lblSystemEnabled.Image = Resources.light_yellow;
                btnEnableSystem.Enabled = btnDisableSystem.Enabled = false;
                btnClearAllAlarms.Enabled = false;
                btnStopAllMotion.Enabled = false;
            }
            else
            {
                UpdateStatus(lblSystemEnabled, newConfig.isEnabled);
                btnEnableSystem.Enabled = btnDisableSystem.Enabled = newConfig.setEnabled != null;
                btnClearAllAlarms.Enabled = newConfig.clearAllAlarms != null;
                btnStopAllMotion.Enabled = newConfig.stopAllMotion != null;
            }
        }

        private void UpdateSystemConfig(Dictionary<string, dynamic> Vals)
        {
            if (Config == null)
                return;

            UpdateStatus(lblSystemEnabled, Config.isEnabled, Vals);
        }


        private async Task<string> SubscribeToVariables()
        {
            if ((Config == null) || (!Controller.HasConnected))
                return null;

            // Get full list of variables to monitor for this subscription
            List<MPiecVariable> Variables = new List<MPiecVariable>();
            if (Config.isEnabled != null)
                Variables.Add(Config.isEnabled);
            foreach (MPiecAxis Axis in Config.Axes)
                Variables.AddRange(Axis.MonitorVariables);

            return await Controller.SubscribeToVariables(Variables);
        }

        private void UpdateWatchedValues(object sender, MPIECSubscriptionReadEventArgs e)
        {
            if (txtControllerAddress.InvokeRequired)
            {
                txtControllerAddress.Invoke(new MethodInvoker(() => UpdateWatchedValues(sender, e)));
            }
            else
            {

                //StringBuilder vals = new StringBuilder();
                //foreach (string naem in e.VariableValues.Keys)
                //    vals.Append(naem + " = " + Convert.ToString(e.VariableValues[naem]) + "\n");
                //MessageBox.Show("Updating results: " + vals.ToString() + ", AxisControls = " + AxisControls.Count);
                UpdateSystemConfig(e.VariableValues);
                foreach (ctlAxisStatus axisStatus in AxisControls)
                    axisStatus.UpdateStatus(e.VariableValues);
            }
        }
        private async void btnConnect_Click(object sender, EventArgs e)
        {
            btnDisconnect.Enabled = btnConnect.Enabled = false;
            try
            {
                if (await Controller.Connect(txtControllerAddress.Text))
                {
                    grpController.Enabled = true;
                    WatchSubscription = await SubscribeToVariables();
                    if (!String.IsNullOrEmpty(WatchSubscription))
                    {
                        Controller.StartSubscriptionReadTimer(WatchSubscription, 250, UpdateWatchedValues);
                    }
                }
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered connecting to controller: " + ex.Message,
                    "Unable to Connect", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                btnDisconnect.Enabled = btnConnect.Enabled = true;
            }
        }

        private async void btnDisconnect_Click(object sender, EventArgs e)
        {
            btnDisconnect.Enabled = btnConnect.Enabled = false;
            try
            {
                WatchSubscription = null;
                await Controller.Disconnect();
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered disconnecting from controller: " + ex.Message,
                    "Unable to Disconnect", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                btnDisconnect.Enabled = btnConnect.Enabled = true;
            }
        }

        private async void btnEnableSystem_Click(object sender, EventArgs e)
        {
            btnEnableSystem.Enabled = false;
            try
            {
                if ((Config != null) && (Config.setEnabled != null))
                    await Controller.SetVariable(Config.setEnabled, true);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                btnEnableSystem.Enabled = true;
            }
        }

        private async void btnDisableSystem_Click(object sender, EventArgs e)
        {
            btnDisableSystem.Enabled = false;
            try
            {
                if ((Config != null) && (Config.setEnabled != null))
                    await Controller.SetVariable(Config.setEnabled, false);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                btnDisableSystem.Enabled = true;
            }
        }

        private async void btnStopAllMotion_Click(object sender, EventArgs e)
        {
            btnStopAllMotion.Enabled = false;
            try
            {
                if ((Config != null) && (Config.stopAllMotion != null))
                    await Controller.SetVariable(Config.stopAllMotion, true);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                btnStopAllMotion.Enabled = true;
            }
        }

        private async void btnClearAllAlarms_Click(object sender, EventArgs e)
        {
            btnClearAllAlarms.Enabled = false;
            try
            {
                if ((Config != null) && (Config.clearAllAlarms != null))
                    await Controller.SetVariable(Config.clearAllAlarms, true);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;
                MessageBox.Show("Error encoutnered: " + ex.Message, "Unable to Change Setting", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                btnClearAllAlarms.Enabled = true;
            }
        }


        private void btnLoadConfig_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                MPiecConfiguration config = MPiecConfiguration.Load(openFileDialog1.FileName);
                UpdateConfig(config);
            }
        }

        private void btnSaveConfig_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                Config.Save(saveFileDialog1.FileName);
        }

        private void btnEditConfiguration_Click(object sender, EventArgs e)
        {
            frmEditConfiguration editConfig = new frmEditConfiguration(Config);
            if (editConfig.ShowDialog() == DialogResult.OK)
                UpdateConfig(editConfig.GetConfig());
        }

        private void btnNewConfiguration_Click(object sender, EventArgs e)
        {
            UpdateConfig(new MPiecConfiguration());
            btnEditConfiguration_Click(null, null);
        }

        private void btnWarmRestart_Click(object sender, EventArgs e)
        {
            if (Controller != null)
            {
                Controller.Stop();
                Controller.WarmStart();
            }
        }
    }
}
