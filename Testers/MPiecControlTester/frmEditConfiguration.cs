﻿using MPiecFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MPiecControlTester
{
    public partial class frmEditConfiguration : Form
    {
        MPiecConfiguration config;

        public frmEditConfiguration()
        {
            InitializeComponent();
        }

        public frmEditConfiguration(MPiecConfiguration newConfig) : this()
        {
            SetConfig(newConfig);
        }

        public void AddAxisTab(MPiecAxis axis)
        {
            TabPage tabAxis = new TabPage(axis.AxisName);
            ctlAxisConfig axisConfig = new ctlAxisConfig(axis);
            axisConfig.Dock = DockStyle.Fill;
            tabAxis.Controls.Add(axisConfig);
            tabControl1.TabPages.Add(tabAxis);
        }

        public void SetConfig(MPiecConfiguration newConfig)
        {
            this.config = newConfig;
            txtConfigName.Text = newConfig.Name;
            ctlClearAllAlarms.SetVariable(config.clearAllAlarms);
            ctlIsEnabled.SetVariable(config.isEnabled);
            ctlSetEnabled.SetVariable(config.setEnabled);
            ctlStopAllMotion.SetVariable(config.stopAllMotion);
            ctlJogSpeed.SetVariable(config.jogSpeed);
            ctljogAcceleration.SetVariable(config.jogAcceleration);
            ctlJogDeceleration.SetVariable(config.jogDeceleration);

            tabControl1.SuspendLayout();
            tabControl1.TabPages.Clear();
            foreach (MPiecAxis axis in config.Axes)
                AddAxisTab(axis);
            tabControl1.ResumeLayout();
        }

        public MPiecConfiguration GetConfig()
        {
            if (config == null)
                config = new MPiecConfiguration();

            config.Name = txtConfigName.Text;
            config.clearAllAlarms = ctlClearAllAlarms.GetVariable();
            config.isEnabled = ctlIsEnabled.GetVariable();
            config.setEnabled = ctlSetEnabled.GetVariable();
            config.stopAllMotion = ctlStopAllMotion.GetVariable();
            config.jogSpeed = ctlJogSpeed.GetVariable();
            config.jogAcceleration = ctljogAcceleration.GetVariable();
            config.jogDeceleration = ctlJogDeceleration.GetVariable();

            config.Axes.Clear();
            foreach (TabPage tabAxis in tabControl1.TabPages)
            {
                ctlAxisConfig axisConfig = tabAxis.Controls[0] as ctlAxisConfig;
                if (axisConfig != null)
                    config.Axes.Add(axisConfig.GetAxis());
            }

            return config;
        }

        private void btnRevert_Click(object sender, EventArgs e)
        {
            SetConfig(config);
        }

        private void btnAddAxis_Click(object sender, EventArgs e)
        {
            AddAxisTab(new MPiecAxis());
        }

        private void btnRemoveAxis_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab != null)
                tabControl1.TabPages.Remove(tabControl1.SelectedTab);
        }
    }
}
