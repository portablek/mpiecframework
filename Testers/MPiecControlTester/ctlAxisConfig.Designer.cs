﻿namespace MPiecControlTester
{
    partial class ctlAxisConfig
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAxisName = new System.Windows.Forms.TextBox();
            this.ctlIsMovingToTarget = new MPiecControlTester.ctlConfigVariable();
            this.ctlIsEnabled = new MPiecControlTester.ctlConfigVariable();
            this.ctlSetEnabled = new MPiecControlTester.ctlConfigVariable();
            this.ctlStopMotion = new MPiecControlTester.ctlConfigVariable();
            this.ctlCurrentSpeed = new MPiecControlTester.ctlConfigVariable();
            this.ctlSetSpeed = new MPiecControlTester.ctlConfigVariable();
            this.ctlCurrentPosition = new MPiecControlTester.ctlConfigVariable();
            this.ctlSetEncoderPosition = new MPiecControlTester.ctlConfigVariable();
            this.ctlJogReverse = new MPiecControlTester.ctlConfigVariable();
            this.ctlIsAlarmed = new MPiecControlTester.ctlConfigVariable();
            this.ctlGoToTarget = new MPiecControlTester.ctlConfigVariable();
            this.ctlJogForward = new MPiecControlTester.ctlConfigVariable();
            this.ctlIsAtHome = new MPiecControlTester.ctlConfigVariable();
            this.ctlTargetPosition = new MPiecControlTester.ctlConfigVariable();
            this.ctlGoToHome = new MPiecControlTester.ctlConfigVariable();
            this.ctlIsAtTarget = new MPiecControlTester.ctlConfigVariable();
            this.ctlClearAlarm = new MPiecControlTester.ctlConfigVariable();
            this.ctlIsJogging = new MPiecControlTester.ctlConfigVariable();
            this.ctlIsHoming = new MPiecControlTester.ctlConfigVariable();
            this.ctlIsMoving = new MPiecControlTester.ctlConfigVariable();
            this.ctlIsHot = new MPiecControlTester.ctlConfigVariable();
            this.ctlJogSpeed = new MPiecControlTester.ctlConfigVariable();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(876, 23);
            this.label1.TabIndex = 15;
            this.label1.Text = "Status";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 479);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(876, 23);
            this.label2.TabIndex = 16;
            this.label2.Text = "Motion";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 829);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(876, 23);
            this.label3.TabIndex = 17;
            this.label3.Text = "Maintenance";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(154, 7);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(287, 23);
            this.label4.TabIndex = 18;
            this.label4.Text = "Axis Name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAxisName
            // 
            this.txtAxisName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAxisName.Location = new System.Drawing.Point(447, 7);
            this.txtAxisName.Margin = new System.Windows.Forms.Padding(2);
            this.txtAxisName.Name = "txtAxisName";
            this.txtAxisName.Size = new System.Drawing.Size(287, 23);
            this.txtAxisName.TabIndex = 19;
            // 
            // ctlIsMovingToTarget
            // 
            this.ctlIsMovingToTarget.Location = new System.Drawing.Point(17, 170);
            this.ctlIsMovingToTarget.Margin = new System.Windows.Forms.Padding(6);
            this.ctlIsMovingToTarget.Name = "ctlIsMovingToTarget";
            this.ctlIsMovingToTarget.Size = new System.Drawing.Size(287, 100);
            this.ctlIsMovingToTarget.TabIndex = 23;
            this.ctlIsMovingToTarget.Text = "isMovingToTarget";
            // 
            // ctlIsEnabled
            // 
            this.ctlIsEnabled.Location = new System.Drawing.Point(17, 64);
            this.ctlIsEnabled.Margin = new System.Windows.Forms.Padding(6);
            this.ctlIsEnabled.Name = "ctlIsEnabled";
            this.ctlIsEnabled.Size = new System.Drawing.Size(287, 100);
            this.ctlIsEnabled.TabIndex = 21;
            this.ctlIsEnabled.Text = "isEnabled";
            // 
            // ctlSetEnabled
            // 
            this.ctlSetEnabled.Location = new System.Drawing.Point(603, 855);
            this.ctlSetEnabled.Margin = new System.Windows.Forms.Padding(6);
            this.ctlSetEnabled.Name = "ctlSetEnabled";
            this.ctlSetEnabled.Size = new System.Drawing.Size(287, 100);
            this.ctlSetEnabled.TabIndex = 20;
            this.ctlSetEnabled.Text = "SetEnabled";
            // 
            // ctlStopMotion
            // 
            this.ctlStopMotion.Location = new System.Drawing.Point(17, 717);
            this.ctlStopMotion.Margin = new System.Windows.Forms.Padding(6);
            this.ctlStopMotion.Name = "ctlStopMotion";
            this.ctlStopMotion.Size = new System.Drawing.Size(287, 100);
            this.ctlStopMotion.TabIndex = 14;
            this.ctlStopMotion.Text = "StopMotion";
            // 
            // ctlCurrentSpeed
            // 
            this.ctlCurrentSpeed.Location = new System.Drawing.Point(603, 366);
            this.ctlCurrentSpeed.Margin = new System.Windows.Forms.Padding(6);
            this.ctlCurrentSpeed.Name = "ctlCurrentSpeed";
            this.ctlCurrentSpeed.Size = new System.Drawing.Size(287, 100);
            this.ctlCurrentSpeed.TabIndex = 13;
            this.ctlCurrentSpeed.Text = "CurrentSpeed";
            // 
            // ctlSetSpeed
            // 
            this.ctlSetSpeed.Location = new System.Drawing.Point(310, 611);
            this.ctlSetSpeed.Margin = new System.Windows.Forms.Padding(6);
            this.ctlSetSpeed.Name = "ctlSetSpeed";
            this.ctlSetSpeed.Size = new System.Drawing.Size(287, 100);
            this.ctlSetSpeed.TabIndex = 12;
            this.ctlSetSpeed.Text = "SetSpeed";
            // 
            // ctlCurrentPosition
            // 
            this.ctlCurrentPosition.Location = new System.Drawing.Point(603, 268);
            this.ctlCurrentPosition.Margin = new System.Windows.Forms.Padding(6);
            this.ctlCurrentPosition.Name = "ctlCurrentPosition";
            this.ctlCurrentPosition.Size = new System.Drawing.Size(287, 100);
            this.ctlCurrentPosition.TabIndex = 10;
            this.ctlCurrentPosition.Text = "CurrentPosition";
            // 
            // ctlSetEncoderPosition
            // 
            this.ctlSetEncoderPosition.Location = new System.Drawing.Point(310, 855);
            this.ctlSetEncoderPosition.Margin = new System.Windows.Forms.Padding(6);
            this.ctlSetEncoderPosition.Name = "ctlSetEncoderPosition";
            this.ctlSetEncoderPosition.Size = new System.Drawing.Size(287, 100);
            this.ctlSetEncoderPosition.TabIndex = 9;
            this.ctlSetEncoderPosition.Text = "SetEncoderPosition";
            // 
            // ctlJogReverse
            // 
            this.ctlJogReverse.Location = new System.Drawing.Point(603, 505);
            this.ctlJogReverse.Margin = new System.Windows.Forms.Padding(6);
            this.ctlJogReverse.Name = "ctlJogReverse";
            this.ctlJogReverse.Size = new System.Drawing.Size(287, 100);
            this.ctlJogReverse.TabIndex = 8;
            this.ctlJogReverse.Text = "JogReverse";
            // 
            // ctlIsAlarmed
            // 
            this.ctlIsAlarmed.Location = new System.Drawing.Point(310, 64);
            this.ctlIsAlarmed.Margin = new System.Windows.Forms.Padding(6);
            this.ctlIsAlarmed.Name = "ctlIsAlarmed";
            this.ctlIsAlarmed.Size = new System.Drawing.Size(287, 100);
            this.ctlIsAlarmed.TabIndex = 7;
            this.ctlIsAlarmed.Text = "isAlarmed";
            // 
            // ctlGoToTarget
            // 
            this.ctlGoToTarget.Location = new System.Drawing.Point(17, 611);
            this.ctlGoToTarget.Margin = new System.Windows.Forms.Padding(6);
            this.ctlGoToTarget.Name = "ctlGoToTarget";
            this.ctlGoToTarget.Size = new System.Drawing.Size(287, 100);
            this.ctlGoToTarget.TabIndex = 6;
            this.ctlGoToTarget.Text = "GoToTarget";
            // 
            // ctlJogForward
            // 
            this.ctlJogForward.Location = new System.Drawing.Point(310, 505);
            this.ctlJogForward.Margin = new System.Windows.Forms.Padding(6);
            this.ctlJogForward.Name = "ctlJogForward";
            this.ctlJogForward.Size = new System.Drawing.Size(287, 100);
            this.ctlJogForward.TabIndex = 5;
            this.ctlJogForward.Text = "JogForward";
            // 
            // ctlIsAtHome
            // 
            this.ctlIsAtHome.Location = new System.Drawing.Point(310, 268);
            this.ctlIsAtHome.Margin = new System.Windows.Forms.Padding(6);
            this.ctlIsAtHome.Name = "ctlIsAtHome";
            this.ctlIsAtHome.Size = new System.Drawing.Size(287, 100);
            this.ctlIsAtHome.TabIndex = 4;
            this.ctlIsAtHome.Text = "isAtHome";
            // 
            // ctlTargetPosition
            // 
            this.ctlTargetPosition.Location = new System.Drawing.Point(603, 170);
            this.ctlTargetPosition.Margin = new System.Windows.Forms.Padding(6);
            this.ctlTargetPosition.Name = "ctlTargetPosition";
            this.ctlTargetPosition.Size = new System.Drawing.Size(287, 100);
            this.ctlTargetPosition.TabIndex = 3;
            this.ctlTargetPosition.Text = "TargetPosition";
            // 
            // ctlGoToHome
            // 
            this.ctlGoToHome.Location = new System.Drawing.Point(17, 505);
            this.ctlGoToHome.Margin = new System.Windows.Forms.Padding(6);
            this.ctlGoToHome.Name = "ctlGoToHome";
            this.ctlGoToHome.Size = new System.Drawing.Size(287, 100);
            this.ctlGoToHome.TabIndex = 2;
            this.ctlGoToHome.Text = "GoToHome";
            // 
            // ctlIsAtTarget
            // 
            this.ctlIsAtTarget.Location = new System.Drawing.Point(310, 170);
            this.ctlIsAtTarget.Margin = new System.Windows.Forms.Padding(6);
            this.ctlIsAtTarget.Name = "ctlIsAtTarget";
            this.ctlIsAtTarget.Size = new System.Drawing.Size(287, 100);
            this.ctlIsAtTarget.TabIndex = 1;
            this.ctlIsAtTarget.Text = "isAtTarget";
            // 
            // ctlClearAlarm
            // 
            this.ctlClearAlarm.Location = new System.Drawing.Point(17, 855);
            this.ctlClearAlarm.Margin = new System.Windows.Forms.Padding(6);
            this.ctlClearAlarm.Name = "ctlClearAlarm";
            this.ctlClearAlarm.Size = new System.Drawing.Size(287, 100);
            this.ctlClearAlarm.TabIndex = 0;
            this.ctlClearAlarm.Text = "ClearAlarm";
            // 
            // ctlIsJogging
            // 
            this.ctlIsJogging.Location = new System.Drawing.Point(17, 366);
            this.ctlIsJogging.Margin = new System.Windows.Forms.Padding(6);
            this.ctlIsJogging.Name = "ctlIsJogging";
            this.ctlIsJogging.Size = new System.Drawing.Size(287, 100);
            this.ctlIsJogging.TabIndex = 24;
            this.ctlIsJogging.Text = "isJogging";
            // 
            // ctlIsHoming
            // 
            this.ctlIsHoming.Location = new System.Drawing.Point(17, 268);
            this.ctlIsHoming.Margin = new System.Windows.Forms.Padding(6);
            this.ctlIsHoming.Name = "ctlIsHoming";
            this.ctlIsHoming.Size = new System.Drawing.Size(287, 100);
            this.ctlIsHoming.TabIndex = 25;
            this.ctlIsHoming.Text = "isHoming";
            // 
            // ctlIsMoving
            // 
            this.ctlIsMoving.Location = new System.Drawing.Point(310, 366);
            this.ctlIsMoving.Margin = new System.Windows.Forms.Padding(6);
            this.ctlIsMoving.Name = "ctlIsMoving";
            this.ctlIsMoving.Size = new System.Drawing.Size(287, 100);
            this.ctlIsMoving.TabIndex = 26;
            this.ctlIsMoving.Text = "isMoving";
            // 
            // ctlIsHot
            // 
            this.ctlIsHot.Location = new System.Drawing.Point(603, 67);
            this.ctlIsHot.Margin = new System.Windows.Forms.Padding(6);
            this.ctlIsHot.Name = "ctlIsHot";
            this.ctlIsHot.Size = new System.Drawing.Size(287, 100);
            this.ctlIsHot.TabIndex = 27;
            this.ctlIsHot.Text = "isHot";
            // 
            // ctlJogSpeed
            // 
            this.ctlJogSpeed.Location = new System.Drawing.Point(603, 611);
            this.ctlJogSpeed.Margin = new System.Windows.Forms.Padding(6);
            this.ctlJogSpeed.Name = "ctlJogSpeed";
            this.ctlJogSpeed.Size = new System.Drawing.Size(287, 100);
            this.ctlJogSpeed.TabIndex = 28;
            this.ctlJogSpeed.Text = "JogSpeed";
            // 
            // ctlAxisConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.ctlJogSpeed);
            this.Controls.Add(this.ctlIsHot);
            this.Controls.Add(this.ctlIsMoving);
            this.Controls.Add(this.ctlIsHoming);
            this.Controls.Add(this.ctlIsJogging);
            this.Controls.Add(this.ctlIsMovingToTarget);
            this.Controls.Add(this.ctlIsEnabled);
            this.Controls.Add(this.ctlSetEnabled);
            this.Controls.Add(this.txtAxisName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ctlStopMotion);
            this.Controls.Add(this.ctlCurrentSpeed);
            this.Controls.Add(this.ctlSetSpeed);
            this.Controls.Add(this.ctlCurrentPosition);
            this.Controls.Add(this.ctlSetEncoderPosition);
            this.Controls.Add(this.ctlJogReverse);
            this.Controls.Add(this.ctlIsAlarmed);
            this.Controls.Add(this.ctlGoToTarget);
            this.Controls.Add(this.ctlJogForward);
            this.Controls.Add(this.ctlIsAtHome);
            this.Controls.Add(this.ctlTargetPosition);
            this.Controls.Add(this.ctlGoToHome);
            this.Controls.Add(this.ctlIsAtTarget);
            this.Controls.Add(this.ctlClearAlarm);
            this.Name = "ctlAxisConfig";
            this.Size = new System.Drawing.Size(906, 958);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ctlConfigVariable ctlClearAlarm;
        private ctlConfigVariable ctlIsAtTarget;
        private ctlConfigVariable ctlGoToHome;
        private ctlConfigVariable ctlTargetPosition;
        private ctlConfigVariable ctlIsAtHome;
        private ctlConfigVariable ctlJogForward;
        private ctlConfigVariable ctlGoToTarget;
        private ctlConfigVariable ctlIsAlarmed;
        private ctlConfigVariable ctlJogReverse;
        private ctlConfigVariable ctlSetEncoderPosition;
        private ctlConfigVariable ctlCurrentPosition;
        private ctlConfigVariable ctlSetSpeed;
        private ctlConfigVariable ctlCurrentSpeed;
        private ctlConfigVariable ctlStopMotion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAxisName;
        private ctlConfigVariable ctlSetEnabled;
        private ctlConfigVariable ctlIsEnabled;
        private ctlConfigVariable ctlIsMovingToTarget;
        private ctlConfigVariable ctlIsJogging;
        private ctlConfigVariable ctlIsHoming;
        private ctlConfigVariable ctlIsMoving;
        private ctlConfigVariable ctlIsHot;
        private ctlConfigVariable ctlJogSpeed;
    }
}
