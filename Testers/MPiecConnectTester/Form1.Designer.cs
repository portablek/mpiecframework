﻿namespace MPiecConnectTester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.txtConnectPath = new System.Windows.Forms.TextBox();
            this.btnBrowsePath = new System.Windows.Forms.Button();
            this.btnRunConnect = new System.Windows.Forms.Button();
            this.grpConnect = new System.Windows.Forms.GroupBox();
            this.txtReadVals = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnRead = new System.Windows.Forms.Button();
            this.txtReadName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSubscribe = new System.Windows.Forms.Button();
            this.txtSubscriptionInfo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnWriteVar = new System.Windows.Forms.Button();
            this.txtWriteVal = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGetProduct = new System.Windows.Forms.Button();
            this.txtProduct = new System.Windows.Forms.TextBox();
            this.btnStopConnects = new System.Windows.Forms.Button();
            this.btnStopConnect = new System.Windows.Forms.Button();
            this.grpConnect.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "mpiecconnect.exe";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "MPiecConnect Path";
            // 
            // txtConnectPath
            // 
            this.txtConnectPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConnectPath.Location = new System.Drawing.Point(38, 63);
            this.txtConnectPath.Name = "txtConnectPath";
            this.txtConnectPath.Size = new System.Drawing.Size(908, 31);
            this.txtConnectPath.TabIndex = 1;
            // 
            // btnBrowsePath
            // 
            this.btnBrowsePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowsePath.Location = new System.Drawing.Point(963, 43);
            this.btnBrowsePath.Name = "btnBrowsePath";
            this.btnBrowsePath.Size = new System.Drawing.Size(131, 51);
            this.btnBrowsePath.TabIndex = 2;
            this.btnBrowsePath.Text = "Browse";
            this.btnBrowsePath.UseVisualStyleBackColor = true;
            this.btnBrowsePath.Click += new System.EventHandler(this.btnBrowsePath_Click);
            // 
            // btnRunConnect
            // 
            this.btnRunConnect.Location = new System.Drawing.Point(38, 148);
            this.btnRunConnect.Name = "btnRunConnect";
            this.btnRunConnect.Size = new System.Drawing.Size(211, 59);
            this.btnRunConnect.TabIndex = 3;
            this.btnRunConnect.Text = "Run Connect";
            this.btnRunConnect.UseVisualStyleBackColor = true;
            this.btnRunConnect.Click += new System.EventHandler(this.btnRunConnect_Click);
            // 
            // grpConnect
            // 
            this.grpConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpConnect.Controls.Add(this.txtReadVals);
            this.grpConnect.Controls.Add(this.label5);
            this.grpConnect.Controls.Add(this.btnRead);
            this.grpConnect.Controls.Add(this.txtReadName);
            this.grpConnect.Controls.Add(this.label4);
            this.grpConnect.Controls.Add(this.btnSubscribe);
            this.grpConnect.Controls.Add(this.txtSubscriptionInfo);
            this.grpConnect.Controls.Add(this.label3);
            this.grpConnect.Controls.Add(this.btnWriteVar);
            this.grpConnect.Controls.Add(this.txtWriteVal);
            this.grpConnect.Controls.Add(this.label2);
            this.grpConnect.Controls.Add(this.btnGetProduct);
            this.grpConnect.Controls.Add(this.txtProduct);
            this.grpConnect.Enabled = false;
            this.grpConnect.Location = new System.Drawing.Point(281, 148);
            this.grpConnect.Name = "grpConnect";
            this.grpConnect.Size = new System.Drawing.Size(813, 562);
            this.grpConnect.TabIndex = 4;
            this.grpConnect.TabStop = false;
            this.grpConnect.Text = "groupBox1";
            // 
            // txtReadVals
            // 
            this.txtReadVals.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReadVals.Location = new System.Drawing.Point(128, 348);
            this.txtReadVals.Multiline = true;
            this.txtReadVals.Name = "txtReadVals";
            this.txtReadVals.Size = new System.Drawing.Size(512, 208);
            this.txtReadVals.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 293);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 25);
            this.label5.TabIndex = 11;
            this.label5.Text = "Read";
            // 
            // btnRead
            // 
            this.btnRead.Location = new System.Drawing.Point(658, 290);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(134, 30);
            this.btnRead.TabIndex = 10;
            this.btnRead.Text = "Read...";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // txtReadName
            // 
            this.txtReadName.Location = new System.Drawing.Point(128, 290);
            this.txtReadName.Name = "txtReadName";
            this.txtReadName.Size = new System.Drawing.Size(512, 31);
            this.txtReadName.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 219);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 25);
            this.label4.TabIndex = 8;
            this.label4.Text = "Subscribe";
            // 
            // btnSubscribe
            // 
            this.btnSubscribe.Location = new System.Drawing.Point(658, 216);
            this.btnSubscribe.Name = "btnSubscribe";
            this.btnSubscribe.Size = new System.Drawing.Size(134, 30);
            this.btnSubscribe.TabIndex = 7;
            this.btnSubscribe.Text = "Subscribe...";
            this.btnSubscribe.UseVisualStyleBackColor = true;
            this.btnSubscribe.Click += new System.EventHandler(this.btnSubscribe_Click);
            // 
            // txtSubscriptionInfo
            // 
            this.txtSubscriptionInfo.Location = new System.Drawing.Point(128, 216);
            this.txtSubscriptionInfo.Name = "txtSubscriptionInfo";
            this.txtSubscriptionInfo.Size = new System.Drawing.Size(512, 31);
            this.txtSubscriptionInfo.TabIndex = 6;
            this.txtSubscriptionInfo.Text = "BoolAndOut:BOOL,BoolOrOut:BOOL,RealSumOutput:REAL";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 25);
            this.label3.TabIndex = 5;
            this.label3.Text = "Write:";
            // 
            // btnWriteVar
            // 
            this.btnWriteVar.Location = new System.Drawing.Point(658, 130);
            this.btnWriteVar.Name = "btnWriteVar";
            this.btnWriteVar.Size = new System.Drawing.Size(134, 30);
            this.btnWriteVar.TabIndex = 4;
            this.btnWriteVar.Text = "Write..";
            this.btnWriteVar.UseVisualStyleBackColor = true;
            this.btnWriteVar.Click += new System.EventHandler(this.btnWriteVar_Click);
            // 
            // txtWriteVal
            // 
            this.txtWriteVal.Location = new System.Drawing.Point(128, 130);
            this.txtWriteVal.Name = "txtWriteVal";
            this.txtWriteVal.Size = new System.Drawing.Size(512, 31);
            this.txtWriteVal.TabIndex = 3;
            this.txtWriteVal.Text = "BoolInput1:BOOL=true,BoolInput2:BOOL=false,RealInput1:REAL=1.234,RealInput2:REAL=" +
    "0.251";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Product";
            // 
            // btnGetProduct
            // 
            this.btnGetProduct.Location = new System.Drawing.Point(658, 46);
            this.btnGetProduct.Name = "btnGetProduct";
            this.btnGetProduct.Size = new System.Drawing.Size(134, 30);
            this.btnGetProduct.TabIndex = 1;
            this.btnGetProduct.Text = "Get...";
            this.btnGetProduct.UseVisualStyleBackColor = true;
            this.btnGetProduct.Click += new System.EventHandler(this.btnGetProduct_Click);
            // 
            // txtProduct
            // 
            this.txtProduct.Location = new System.Drawing.Point(128, 46);
            this.txtProduct.Name = "txtProduct";
            this.txtProduct.Size = new System.Drawing.Size(512, 31);
            this.txtProduct.TabIndex = 0;
            // 
            // btnStopConnects
            // 
            this.btnStopConnects.Location = new System.Drawing.Point(24, 628);
            this.btnStopConnects.Name = "btnStopConnects";
            this.btnStopConnects.Size = new System.Drawing.Size(211, 59);
            this.btnStopConnects.TabIndex = 5;
            this.btnStopConnects.Text = "Stop All Connects";
            this.btnStopConnects.UseVisualStyleBackColor = true;
            this.btnStopConnects.Click += new System.EventHandler(this.btnStopConnects_Click);
            // 
            // btnStopConnect
            // 
            this.btnStopConnect.Location = new System.Drawing.Point(38, 234);
            this.btnStopConnect.Name = "btnStopConnect";
            this.btnStopConnect.Size = new System.Drawing.Size(211, 59);
            this.btnStopConnect.TabIndex = 6;
            this.btnStopConnect.Text = "Stop Connect";
            this.btnStopConnect.UseVisualStyleBackColor = true;
            this.btnStopConnect.Click += new System.EventHandler(this.btnStopConnect_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1106, 722);
            this.Controls.Add(this.btnStopConnect);
            this.Controls.Add(this.btnStopConnects);
            this.Controls.Add(this.grpConnect);
            this.Controls.Add(this.btnRunConnect);
            this.Controls.Add(this.btnBrowsePath);
            this.Controls.Add(this.txtConnectPath);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.grpConnect.ResumeLayout(false);
            this.grpConnect.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtConnectPath;
        private System.Windows.Forms.Button btnBrowsePath;
        private System.Windows.Forms.Button btnRunConnect;
        private System.Windows.Forms.GroupBox grpConnect;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGetProduct;
        private System.Windows.Forms.TextBox txtProduct;
        private System.Windows.Forms.Button btnStopConnects;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnWriteVar;
        private System.Windows.Forms.TextBox txtWriteVal;
        private System.Windows.Forms.TextBox txtReadVals;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.TextBox txtReadName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSubscribe;
        private System.Windows.Forms.TextBox txtSubscriptionInfo;
        private System.Windows.Forms.Button btnStopConnect;
    }
}

