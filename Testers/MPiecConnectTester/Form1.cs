﻿using InternetFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MPiecConnectTester
{
    public partial class Form1 : Form
    {
        Process Command = null;
        LineBasedTCPClient Client = null;
     
        public Form1()
        {
            InitializeComponent();
        }

        private void btnBrowsePath_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                txtConnectPath.Text = openFileDialog1.FileName;
        }

        private void ClearConnects()
        {
            foreach (Process process in Process.GetProcessesByName("MPiecConnect"))
                process.Kill();
        }

        private void btnRunConnect_Click(object sender, EventArgs e)
        {
            ClearConnects();

            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                FileName = txtConnectPath.Text,
//                Arguments = @"192.168.20.65 23230",
                Arguments = @"virtual 23230",
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardInput = true,
                CreateNoWindow = true
            };
            
            Command = new Process();
            Command.StartInfo = startInfo;
            Command.OutputDataReceived += Command_OutputDataReceived;
            Command.Exited += Command_Exited;

            Command.Start();
            Command.BeginOutputReadLine();
        }

        private void btnStopConnects_Click(object sender, EventArgs e)
        {
            ClearConnects();
        }

        private void Command_Exited(object sender, EventArgs e)
        {
            MessageBox.Show("Command Exited");
            grpConnect.Enabled = false;
        }

        private void SeverStarted(string serverAddress, string serverPort)
        {
            if (grpConnect.InvokeRequired)
            {
                grpConnect.Invoke(new MethodInvoker(() => SeverStarted(serverAddress, serverPort)));

            }
            else
            {
                //MessageBox.Show("Server Started: " + serverAddress + ":" + serverPort);
                Client = new LineBasedTCPClient(ushort.Parse(serverPort));
                Client.NewConnection += Client_NewConnection;
                Client.RemoteDisconnected += Client_RemoteDisconnected;
                Client.IncomingMessage += Client_IncomingMessage;
                Client.PacketType.EndOfLine = new byte[] { (byte)'>', (byte)' ' };
                _ = Client.ConnectAsync(serverAddress);
            }
        }

        string LastCommand = null;

        private void ParseResponse(string Response)
        {
            if (String.IsNullOrEmpty(LastCommand))
                return;

            if (grpConnect.InvokeRequired)
            {
                grpConnect.Invoke(new MethodInvoker(() => ParseResponse(Response)));
            }
            else
            {
                if (LastCommand.StartsWith("product"))
                {
                    txtProduct.Text = Response;
                    return;
                }

                if (LastCommand.StartsWith("write"))
                {
                    MessageBox.Show("Write Result: " + Response);
                    return;
                }

                if (LastCommand.StartsWith("subscribe"))
                {
                    txtReadName.Text = Response;
                    return;
                }

                if (LastCommand.StartsWith("read"))
                {
                    txtReadVals.Text = Response;
                    return;
                }
            }
        }

        private string CommandResponse = null;
        public string GetCommandResponse(string Command, int Timeout = 1500)
        {
            LastCommand = Command;
            CommandResponse = null;

            Client.Send(Command);
            Client.Send(LineBasedProtocol.CRLF);

            Task.Run(async () =>
            {
                while (CommandResponse == null)
                    await Task.Delay(100);
            }).Wait(Timeout);

            return CommandResponse;
        }

        private void Client_IncomingMessage(object sender, InternetFramework.Events.InternetCommunicationEventArgs e)
        {
            string Message = UTF8Encoding.UTF8.GetString(Client.PacketType.Trim(e.Message));
            CommandResponse = Message.Trim();
        }

        private void SendCommand(string Command)
        {
            if ((Client != null) && Client.IsConnected)
            {
                ParseResponse(GetCommandResponse(Command));
            }
        }


        private void Client_RemoteDisconnected(object sender, InternetFramework.Events.InternetConnectionEventArgs e)
        {
            if (grpConnect.InvokeRequired)
            {
                grpConnect.Invoke(new MethodInvoker(() => Client_RemoteDisconnected(sender, e)));
            }
            else
            {
                grpConnect.Enabled = false;
                if (Command != null)
                    Command.Kill();
            }
        }

        private void Client_NewConnection(object sender, InternetFramework.Events.InternetConnectionEventArgs e)
        {
            if (grpConnect.InvokeRequired)
            {
                grpConnect.Invoke(new MethodInvoker(() => Client_NewConnection(sender, e)));

            }
            else
            {
                grpConnect.Enabled = true;
            }
        }

        private void Command_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            string TelnetServerString = "Telnet server started at ";
            
            if (!String.IsNullOrEmpty(e.Data))
            {
                int index = e.Data.IndexOf(TelnetServerString);
                if (index >= 0)
                {
                    string[] serverinfo = e.Data.Substring(index + TelnetServerString.Length).Split(new string[] { ", port" }, StringSplitOptions.RemoveEmptyEntries);
                    string serverAddress = serverinfo[0];
                    string serverPort = "23";
                    if (serverinfo.Length > 1)
                        serverPort = serverinfo[1];
                    SeverStarted(serverAddress, serverPort);
                }
            }
        }

        private void btnGetProduct_Click(object sender, EventArgs e)
        {
            SendCommand("product");
        }

        private void btnWriteVar_Click(object sender, EventArgs e)
        {
            SendCommand("write " + txtWriteVal.Text);
        }

        private void btnSubscribe_Click(object sender, EventArgs e)
        {
            SendCommand("subscribe " + txtSubscriptionInfo.Text);
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            txtReadVals.Text = "";
            SendCommand("readsub " + txtReadName.Text);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            btnStopConnect_Click(sender, e);
        }

        private void btnStopConnect_Click(object sender, EventArgs e)
        {
            if (Client != null)
                Client.Disconnect();
            Client = null;

            if (Command != null)
                Command.Kill();
            Command = null;
        }
    }
}
