﻿namespace MPiecCommunicationTester
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.label1 = new System.Windows.Forms.Label();
            this.txtControllerAddress = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.grpController = new System.Windows.Forms.GroupBox();
            this.btnFileIO = new System.Windows.Forms.Button();
            this.btnInitialize = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnHotStart = new System.Windows.Forms.Button();
            this.btnWarmStart = new System.Windows.Forms.Button();
            this.btnColdStart = new System.Windows.Forms.Button();
            this.btnDeviceVariables = new System.Windows.Forms.Button();
            this.txtProiorityLevel = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtClrRuntimeVersion = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProgramState = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtRetainDataFree = new System.Windows.Forms.TextBox();
            this.txtRetainDataSize = new System.Windows.Forms.TextBox();
            this.txtApplicationDataFree = new System.Windows.Forms.TextBox();
            this.txtDeviceIdentity = new System.Windows.Forms.TextBox();
            this.txtApplicationDataSize = new System.Windows.Forms.TextBox();
            this.txtApplicationProgramFree = new System.Windows.Forms.TextBox();
            this.txtApplicationProgramSize = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.chkSourceInstalled = new System.Windows.Forms.CheckBox();
            this.txtImageDate = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chkImageInstalled = new System.Windows.Forms.CheckBox();
            this.txtBootImageName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFirmwareVersion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtHardwareVersion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.grpController.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Controller IP Address";
            // 
            // txtControllerAddress
            // 
            this.txtControllerAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtControllerAddress.Location = new System.Drawing.Point(18, 25);
            this.txtControllerAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtControllerAddress.Name = "txtControllerAddress";
            this.txtControllerAddress.Size = new System.Drawing.Size(126, 23);
            this.txtControllerAddress.TabIndex = 1;
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.Location = new System.Drawing.Point(148, 25);
            this.btnConnect.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(90, 24);
            this.btnConnect.TabIndex = 2;
            this.btnConnect.Text = "Connect...";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(18, 54);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.progressBar1.MarqueeAnimationSpeed = 300;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(376, 18);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 3;
            this.progressBar1.Visible = false;
            // 
            // grpController
            // 
            this.grpController.Controls.Add(this.btnFileIO);
            this.grpController.Controls.Add(this.btnInitialize);
            this.grpController.Controls.Add(this.btnReset);
            this.grpController.Controls.Add(this.btnHotStart);
            this.grpController.Controls.Add(this.btnWarmStart);
            this.grpController.Controls.Add(this.btnColdStart);
            this.grpController.Controls.Add(this.btnStop);
            this.grpController.Controls.Add(this.btnDeviceVariables);
            this.grpController.Controls.Add(this.txtProiorityLevel);
            this.grpController.Controls.Add(this.label14);
            this.grpController.Controls.Add(this.txtClrRuntimeVersion);
            this.grpController.Controls.Add(this.label3);
            this.grpController.Controls.Add(this.label2);
            this.grpController.Controls.Add(this.txtProgramState);
            this.grpController.Controls.Add(this.label13);
            this.grpController.Controls.Add(this.txtRetainDataFree);
            this.grpController.Controls.Add(this.txtRetainDataSize);
            this.grpController.Controls.Add(this.txtApplicationDataFree);
            this.grpController.Controls.Add(this.txtDeviceIdentity);
            this.grpController.Controls.Add(this.txtApplicationDataSize);
            this.grpController.Controls.Add(this.txtApplicationProgramFree);
            this.grpController.Controls.Add(this.txtApplicationProgramSize);
            this.grpController.Controls.Add(this.label12);
            this.grpController.Controls.Add(this.label11);
            this.grpController.Controls.Add(this.label10);
            this.grpController.Controls.Add(this.label9);
            this.grpController.Controls.Add(this.label8);
            this.grpController.Controls.Add(this.chkSourceInstalled);
            this.grpController.Controls.Add(this.txtImageDate);
            this.grpController.Controls.Add(this.label7);
            this.grpController.Controls.Add(this.chkImageInstalled);
            this.grpController.Controls.Add(this.txtBootImageName);
            this.grpController.Controls.Add(this.label6);
            this.grpController.Controls.Add(this.txtFirmwareVersion);
            this.grpController.Controls.Add(this.label5);
            this.grpController.Controls.Add(this.txtHardwareVersion);
            this.grpController.Controls.Add(this.label4);
            this.grpController.Enabled = false;
            this.grpController.Location = new System.Drawing.Point(18, 82);
            this.grpController.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.grpController.Name = "grpController";
            this.grpController.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.grpController.Size = new System.Drawing.Size(376, 445);
            this.grpController.TabIndex = 4;
            this.grpController.TabStop = false;
            this.grpController.Text = "Not Connected";
            // 
            // btnFileIO
            // 
            this.btnFileIO.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileIO.Location = new System.Drawing.Point(230, 414);
            this.btnFileIO.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnFileIO.Name = "btnFileIO";
            this.btnFileIO.Size = new System.Drawing.Size(135, 22);
            this.btnFileIO.TabIndex = 39;
            this.btnFileIO.Text = "On-Device File I/O...";
            this.btnFileIO.UseVisualStyleBackColor = true;
            this.btnFileIO.Click += new System.EventHandler(this.btnFileIO_Click);
            // 
            // btnInitialize
            // 
            this.btnInitialize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInitialize.Location = new System.Drawing.Point(9, 401);
            this.btnInitialize.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnInitialize.Name = "btnInitialize";
            this.btnInitialize.Size = new System.Drawing.Size(90, 23);
            this.btnInitialize.TabIndex = 38;
            this.btnInitialize.Text = "Initialize PLC";
            this.btnInitialize.UseVisualStyleBackColor = true;
            this.btnInitialize.Click += new System.EventHandler(this.btnInitialize_Click);
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(9, 372);
            this.btnReset.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(90, 23);
            this.btnReset.TabIndex = 37;
            this.btnReset.Text = "Reset PLC";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnHotStart
            // 
            this.btnHotStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHotStart.Location = new System.Drawing.Point(197, 332);
            this.btnHotStart.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnHotStart.Name = "btnHotStart";
            this.btnHotStart.Size = new System.Drawing.Size(90, 23);
            this.btnHotStart.TabIndex = 36;
            this.btnHotStart.Text = "Start (Hot)";
            this.btnHotStart.UseVisualStyleBackColor = true;
            this.btnHotStart.Click += new System.EventHandler(this.btnHotStart_Click);
            // 
            // btnWarmStart
            // 
            this.btnWarmStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWarmStart.Location = new System.Drawing.Point(103, 332);
            this.btnWarmStart.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnWarmStart.Name = "btnWarmStart";
            this.btnWarmStart.Size = new System.Drawing.Size(90, 23);
            this.btnWarmStart.TabIndex = 35;
            this.btnWarmStart.Text = "Start (Warm)";
            this.btnWarmStart.UseVisualStyleBackColor = true;
            this.btnWarmStart.Click += new System.EventHandler(this.btnWarmStart_Click);
            // 
            // btnColdStart
            // 
            this.btnColdStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnColdStart.Location = new System.Drawing.Point(9, 332);
            this.btnColdStart.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnColdStart.Name = "btnColdStart";
            this.btnColdStart.Size = new System.Drawing.Size(90, 23);
            this.btnColdStart.TabIndex = 34;
            this.btnColdStart.Text = "Start (Cold)";
            this.btnColdStart.UseVisualStyleBackColor = true;
            this.btnColdStart.Click += new System.EventHandler(this.btnColdStart_Click);
            // 
            // btnDeviceVariables
            // 
            this.btnDeviceVariables.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeviceVariables.Location = new System.Drawing.Point(194, 372);
            this.btnDeviceVariables.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDeviceVariables.Name = "btnDeviceVariables";
            this.btnDeviceVariables.Size = new System.Drawing.Size(169, 34);
            this.btnDeviceVariables.TabIndex = 32;
            this.btnDeviceVariables.Text = "Program Variables...";
            this.btnDeviceVariables.UseVisualStyleBackColor = true;
            this.btnDeviceVariables.Click += new System.EventHandler(this.btnDeviceVariables_Click);
            // 
            // txtProiorityLevel
            // 
            this.txtProiorityLevel.Location = new System.Drawing.Point(125, 260);
            this.txtProiorityLevel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtProiorityLevel.Name = "txtProiorityLevel";
            this.txtProiorityLevel.ReadOnly = true;
            this.txtProiorityLevel.Size = new System.Drawing.Size(240, 20);
            this.txtProiorityLevel.TabIndex = 31;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label14.Location = new System.Drawing.Point(4, 260);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(120, 20);
            this.label14.TabIndex = 30;
            this.label14.Text = "Threading Priority";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtClrRuntimeVersion
            // 
            this.txtClrRuntimeVersion.Location = new System.Drawing.Point(245, 75);
            this.txtClrRuntimeVersion.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtClrRuntimeVersion.Name = "txtClrRuntimeVersion";
            this.txtClrRuntimeVersion.ReadOnly = true;
            this.txtClrRuntimeVersion.Size = new System.Drawing.Size(120, 20);
            this.txtClrRuntimeVersion.TabIndex = 29;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label3.Location = new System.Drawing.Point(125, 75);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 20);
            this.label3.TabIndex = 28;
            this.label3.Text = "Runtime Version";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label2.Location = new System.Drawing.Point(4, 15);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 20);
            this.label2.TabIndex = 27;
            this.label2.Text = "Identity";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtProgramState
            // 
            this.txtProgramState.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProgramState.Location = new System.Drawing.Point(125, 300);
            this.txtProgramState.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtProgramState.Name = "txtProgramState";
            this.txtProgramState.ReadOnly = true;
            this.txtProgramState.Size = new System.Drawing.Size(164, 23);
            this.txtProgramState.TabIndex = 26;
            this.txtProgramState.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 300);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(120, 23);
            this.label13.TabIndex = 25;
            this.label13.Text = "Program State";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRetainDataFree
            // 
            this.txtRetainDataFree.Location = new System.Drawing.Point(245, 167);
            this.txtRetainDataFree.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtRetainDataFree.Name = "txtRetainDataFree";
            this.txtRetainDataFree.ReadOnly = true;
            this.txtRetainDataFree.Size = new System.Drawing.Size(120, 20);
            this.txtRetainDataFree.TabIndex = 24;
            // 
            // txtRetainDataSize
            // 
            this.txtRetainDataSize.Location = new System.Drawing.Point(125, 167);
            this.txtRetainDataSize.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtRetainDataSize.Name = "txtRetainDataSize";
            this.txtRetainDataSize.ReadOnly = true;
            this.txtRetainDataSize.Size = new System.Drawing.Size(120, 20);
            this.txtRetainDataSize.TabIndex = 23;
            // 
            // txtApplicationDataFree
            // 
            this.txtApplicationDataFree.Location = new System.Drawing.Point(245, 147);
            this.txtApplicationDataFree.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtApplicationDataFree.Name = "txtApplicationDataFree";
            this.txtApplicationDataFree.ReadOnly = true;
            this.txtApplicationDataFree.Size = new System.Drawing.Size(120, 20);
            this.txtApplicationDataFree.TabIndex = 22;
            // 
            // txtDeviceIdentity
            // 
            this.txtDeviceIdentity.Location = new System.Drawing.Point(125, 15);
            this.txtDeviceIdentity.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtDeviceIdentity.Name = "txtDeviceIdentity";
            this.txtDeviceIdentity.ReadOnly = true;
            this.txtDeviceIdentity.Size = new System.Drawing.Size(240, 20);
            this.txtDeviceIdentity.TabIndex = 1;
            // 
            // txtApplicationDataSize
            // 
            this.txtApplicationDataSize.Location = new System.Drawing.Point(125, 147);
            this.txtApplicationDataSize.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtApplicationDataSize.Name = "txtApplicationDataSize";
            this.txtApplicationDataSize.ReadOnly = true;
            this.txtApplicationDataSize.Size = new System.Drawing.Size(120, 20);
            this.txtApplicationDataSize.TabIndex = 21;
            // 
            // txtApplicationProgramFree
            // 
            this.txtApplicationProgramFree.Location = new System.Drawing.Point(245, 127);
            this.txtApplicationProgramFree.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtApplicationProgramFree.Name = "txtApplicationProgramFree";
            this.txtApplicationProgramFree.ReadOnly = true;
            this.txtApplicationProgramFree.Size = new System.Drawing.Size(120, 20);
            this.txtApplicationProgramFree.TabIndex = 20;
            // 
            // txtApplicationProgramSize
            // 
            this.txtApplicationProgramSize.Location = new System.Drawing.Point(125, 127);
            this.txtApplicationProgramSize.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtApplicationProgramSize.Name = "txtApplicationProgramSize";
            this.txtApplicationProgramSize.ReadOnly = true;
            this.txtApplicationProgramSize.Size = new System.Drawing.Size(120, 20);
            this.txtApplicationProgramSize.TabIndex = 19;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(245, 106);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(120, 20);
            this.label12.TabIndex = 18;
            this.label12.Text = "Free";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(125, 106);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(120, 20);
            this.label11.TabIndex = 17;
            this.label11.Text = "Total";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(4, 167);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 20);
            this.label10.TabIndex = 16;
            this.label10.Text = "Retain Data";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(4, 147);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 20);
            this.label9.TabIndex = 15;
            this.label9.Text = "Data";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(4, 127);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 20);
            this.label8.TabIndex = 14;
            this.label8.Text = "Program";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkSourceInstalled
            // 
            this.chkSourceInstalled.AutoSize = true;
            this.chkSourceInstalled.BackColor = System.Drawing.SystemColors.ControlLight;
            this.chkSourceInstalled.Enabled = false;
            this.chkSourceInstalled.Location = new System.Drawing.Point(251, 203);
            this.chkSourceInstalled.Name = "chkSourceInstalled";
            this.chkSourceInstalled.Size = new System.Drawing.Size(102, 17);
            this.chkSourceInstalled.TabIndex = 13;
            this.chkSourceInstalled.Text = "Source Installed";
            this.chkSourceInstalled.UseVisualStyleBackColor = false;
            // 
            // txtImageDate
            // 
            this.txtImageDate.Location = new System.Drawing.Point(125, 241);
            this.txtImageDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtImageDate.Name = "txtImageDate";
            this.txtImageDate.ReadOnly = true;
            this.txtImageDate.Size = new System.Drawing.Size(240, 20);
            this.txtImageDate.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label7.Location = new System.Drawing.Point(4, 241);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 20);
            this.label7.TabIndex = 11;
            this.label7.Text = "Build Date";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkImageInstalled
            // 
            this.chkImageInstalled.AutoSize = true;
            this.chkImageInstalled.BackColor = System.Drawing.SystemColors.ControlLight;
            this.chkImageInstalled.Enabled = false;
            this.chkImageInstalled.Location = new System.Drawing.Point(128, 203);
            this.chkImageInstalled.Name = "chkImageInstalled";
            this.chkImageInstalled.Size = new System.Drawing.Size(122, 17);
            this.chkImageInstalled.TabIndex = 10;
            this.chkImageInstalled.Text = "Boot Image Installed";
            this.chkImageInstalled.UseVisualStyleBackColor = false;
            // 
            // txtBootImageName
            // 
            this.txtBootImageName.Location = new System.Drawing.Point(125, 222);
            this.txtBootImageName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtBootImageName.Name = "txtBootImageName";
            this.txtBootImageName.ReadOnly = true;
            this.txtBootImageName.Size = new System.Drawing.Size(240, 20);
            this.txtBootImageName.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label6.Location = new System.Drawing.Point(4, 222);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 20);
            this.label6.TabIndex = 8;
            this.label6.Text = "Boot Image";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFirmwareVersion
            // 
            this.txtFirmwareVersion.Location = new System.Drawing.Point(245, 55);
            this.txtFirmwareVersion.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtFirmwareVersion.Name = "txtFirmwareVersion";
            this.txtFirmwareVersion.ReadOnly = true;
            this.txtFirmwareVersion.Size = new System.Drawing.Size(120, 20);
            this.txtFirmwareVersion.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label5.Location = new System.Drawing.Point(125, 55);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 20);
            this.label5.TabIndex = 6;
            this.label5.Text = "Firmware Version";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtHardwareVersion
            // 
            this.txtHardwareVersion.Location = new System.Drawing.Point(245, 35);
            this.txtHardwareVersion.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtHardwareVersion.Name = "txtHardwareVersion";
            this.txtHardwareVersion.ReadOnly = true;
            this.txtHardwareVersion.Size = new System.Drawing.Size(120, 20);
            this.txtHardwareVersion.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label4.Location = new System.Drawing.Point(125, 35);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Hardware Version";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisconnect.Location = new System.Drawing.Point(245, 25);
            this.btnDisconnect.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(90, 24);
            this.btnDisconnect.TabIndex = 5;
            this.btnDisconnect.Text = "Disconnect...";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // btnStop
            // 
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Image = ((System.Drawing.Image)(resources.GetObject("btnStop.Image")));
            this.btnStop.Location = new System.Drawing.Point(291, 297);
            this.btnStop.Margin = new System.Windows.Forms.Padding(2);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(72, 58);
            this.btnStop.TabIndex = 33;
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 541);
            this.Controls.Add(this.btnDisconnect);
            this.Controls.Add(this.grpController);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.txtControllerAddress);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "frmMain";
            this.Text = "Yaskawa MPiec Series Interface";
            this.grpController.ResumeLayout(false);
            this.grpController.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtControllerAddress;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.GroupBox grpController;
        private System.Windows.Forms.TextBox txtDeviceIdentity;
        private System.Windows.Forms.TextBox txtFirmwareVersion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtHardwareVersion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBootImageName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkImageInstalled;
        private System.Windows.Forms.CheckBox chkSourceInstalled;
        private System.Windows.Forms.TextBox txtImageDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtRetainDataFree;
        private System.Windows.Forms.TextBox txtRetainDataSize;
        private System.Windows.Forms.TextBox txtApplicationDataFree;
        private System.Windows.Forms.TextBox txtApplicationDataSize;
        private System.Windows.Forms.TextBox txtApplicationProgramFree;
        private System.Windows.Forms.TextBox txtApplicationProgramSize;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtProgramState;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtClrRuntimeVersion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtProiorityLevel;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Button btnDeviceVariables;
        private System.Windows.Forms.Button btnHotStart;
        private System.Windows.Forms.Button btnWarmStart;
        private System.Windows.Forms.Button btnColdStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnInitialize;
        private System.Windows.Forms.Button btnFileIO;
    }
}

