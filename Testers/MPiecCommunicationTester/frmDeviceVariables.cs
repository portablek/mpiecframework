﻿using MPiecFramework;
using MPiecFramework.Events;
using MPiecFramework.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MPiecCommunicationTester
{
    public partial class frmDeviceVariables : Form
    {
        private IMPiec Controller = null;

        public frmDeviceVariables(IMPiec Controller)
        {
            InitializeComponent();
            this.Controller = Controller;
        }

        private async void btnWriteVariables_Click(object sender, EventArgs e)
        {
            try
            {
                grpWrite.Enabled = false;

                await Controller.WriteGlobalVariables(cmbWriteDefinitions.Text.Split(','));

                if (!cmbWriteDefinitions.Items.Contains(cmbWriteDefinitions.Text))
                    cmbWriteDefinitions.Items.Add(cmbWriteDefinitions.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + (ex.InnerException != null ? ":" + ex.InnerException.Message : ""), "Unable to write variables",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                grpWrite.Enabled = true;
            }
        }

        private async void btnSubscribe_Click(object sender, EventArgs e)
        {
            try
            {
                grpSubscriptions.Enabled = false;

                string ID = await Controller.SubscribeToGlobalVariables(cmbSubscriptionDefinitions.Text.Split(','), cmbSubscriptionName.Text);

                if (!cmbSubscriptionName.Items.Contains(ID))
                    cmbSubscriptionName.Items.Add(ID);
                if (!cmbSubscriptionDefinitions.Items.Contains(cmbSubscriptionDefinitions.Text))
                    cmbSubscriptionDefinitions.Items.Add(cmbSubscriptionDefinitions.Text);

                frmDeviceSubscription newSubscription = new frmDeviceSubscription(Controller, ID);
                newSubscription.Show();
            } 
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + (ex.InnerException != null ? ":" + ex.InnerException.Message : ""), "Unable to create subscription",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                grpSubscriptions.Enabled = true;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
