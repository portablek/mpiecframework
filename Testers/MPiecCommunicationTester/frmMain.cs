﻿using MPiecFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MPiecCommunicationTester
{
    public partial class frmMain : Form
    {
        IMPiec Controller = MPiec.GetControllerInterface();
        Timer StateUpdateTimer = null;

        public frmMain()
        {
            InitializeComponent();
//            Controller.CommunicationError += Controller_CommunicationError;
            Controller.SettingNotFound += Controller_SettingNotFound;
//            Controller.PLCControlError += Controller_PLCControlError;
            Controller.FileReadError += Controller_FileError;
            Controller.FileWriteError += Controller_FileError;
        }

        private void Controller_FileError(object sender, MPiecFramework.Events.MPIECFileEventArgs e)
        {
            string ErrorMessage = "Unable to " + (e.WasWriting ? "write" : "read") + " " + e.Filename + ": ";
            MessageBox.Show(
                "Time : " + e.Timestamp.ToLocalTime() + "\n" +
                "Error: " + ErrorMessage + (e.HasMessage ? e.Message : "Unknown error attempting to change PLC state."),
                "File I/O Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void Controller_PLCControlError(object sender, MPiecFramework.Events.MPIECControlErrorEventArgs e)
        {
            MessageBox.Show(
                "Time : " + e.Timestamp.ToLocalTime() + "\n" +
                "Error: " + (e.HasMessage ? e.Message : "Unknown error attempting to change PLC state."),
                "Communication Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void Controller_SettingNotFound(object sender, MPiecFramework.Events.MPIECSettingNotFoundEventArgs e)
        {
            Console.WriteLine("Controller setting not found: " + e.Setting + ". " + (e.HasMessage ? e.Message : ""));
        }

        private void Controller_CommunicationError(object sender, MPiecFramework.Events.MPIECCommunicationErrorEventArgs e)
        {
            MessageBox.Show(
                "Time : " + e.Timestamp.ToLocalTime() + "\n" +
                "Error: " + (e.HasMessage ? e.Message : "Unknown error attempting to contact controller."),
                "Communication Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private Task<bool> DoConnect(string IPAddress)
        {
            return Controller.Connect(IPAddress);
        }

        private async void btnDisconnect_Click(object sender, EventArgs e)
        {
            if (sender != null)
                progressBar1.Visible = true;

            if (Controller != null)
                await Controller.Disconnect();

            grpController.Text = "Not Connected";
            grpController.Enabled = false;
            txtDeviceIdentity.Text = string.Empty;
            txtHardwareVersion.Text = string.Empty;
            txtFirmwareVersion.Text = string.Empty;
            txtClrRuntimeVersion.Text = string.Empty;
            txtBootImageName.Text = string.Empty;
            txtImageDate.Text = string.Empty;
            txtApplicationProgramFree.Text = string.Empty;
            txtApplicationProgramSize.Text = string.Empty;
            txtApplicationDataSize.Text = string.Empty;
            txtApplicationDataFree.Text = string.Empty;
            txtRetainDataSize.Text = string.Empty;
            txtRetainDataFree.Text = string.Empty;
            txtProgramState.Text = string.Empty;
            txtProiorityLevel.Text = string.Empty;
            chkImageInstalled.Checked = false;
            chkSourceInstalled.Checked = false;

            if (StateUpdateTimer != null)
            {
                StateUpdateTimer.Stop();
                StateUpdateTimer.Dispose();
                StateUpdateTimer = null;
            }

            if (sender != null)
                progressBar1.Visible = false;
        }

        private async void UpdateControllerStatus()
        {
            ProgramState State = await Controller.GetProgramState();
            if (txtProgramState.InvokeRequired)
            {
                txtProgramState.Invoke(new MethodInvoker(() => UpdateControllerStatus()));
            }
            else
            {
                txtProgramState.Text = State.ToString();
                btnColdStart.Enabled = btnWarmStart.Enabled = btnHotStart.Enabled = State != ProgramState.Running;
                btnStop.Enabled = State == ProgramState.Running || State == ProgramState.Starting;
                btnInitialize.Enabled = State == ProgramState.On;
                btnReset.Enabled = true;
            }
        }

        private async void btnConnect_Click(object sender, EventArgs e)
        {
            progressBar1.Visible = true;
            btnDisconnect_Click(null, null);

            if (await DoConnect(txtControllerAddress.Text))
            {
                grpController.Text = await Controller.GetManufacturer();// + " " + await Controller.GetProductName();
                grpController.Enabled = true;
                //txtDeviceIdentity.Text =  (await Controller.GetDeviceIdentity()).ToString();
                //txtHardwareVersion.Text = await Controller.GetHardwareVersion();
                //txtFirmwareVersion.Text = await Controller.GetFirmwareVersion();
                //txtClrRuntimeVersion.Text = await Controller.GetRuntimeVersion();

                MPiecMemory Memory = await Controller.GetMemoryState();
                txtApplicationDataFree.Text = Memory.DataFree.ToByteSizeString();
                txtApplicationDataSize.Text = Memory.DataSize.ToByteSizeString();
                txtApplicationProgramFree.Text = Memory.ApplicationFree.ToByteSizeString();
                txtApplicationProgramSize.Text = Memory.ApplicationSize.ToByteSizeString();
                txtRetainDataFree.Text = Memory.RetainFree.ToByteSizeString();
                txtRetainDataSize.Text = Memory.RetainSize.ToByteSizeString();

                chkImageInstalled.Checked = await Controller.IsImageInstalled();
                //txtBootImageName.Text = await Controller.GetBootImageName();
                txtImageDate.Text = (await Controller.GetImageBuildDate()).ToLocalTime().ToString();
                txtProiorityLevel.Text = (await Controller.GetThreadingPriorityLevel()).ToString();
                chkSourceInstalled.Checked = await Controller.IsSourceInstalled();

                StateUpdateTimer = new Timer();
                StateUpdateTimer.Interval = 100;
                StateUpdateTimer.Tick += StateUpdateTimer_Tick;
                StateUpdateTimer.Start();
            }
            progressBar1.Visible = false;
        }

        private void StateUpdateTimer_Tick(object sender, EventArgs e)
        {
            UpdateControllerStatus();
        }

        private void btnDeviceVariables_Click(object sender, EventArgs e)
        {
            frmDeviceVariables DeviceVariables = new frmDeviceVariables(Controller);
            DeviceVariables.Show(this);
        }

        private async void btnColdStart_Click(object sender, EventArgs e)
        {
            btnColdStart.Enabled = btnWarmStart.Enabled = btnHotStart.Enabled = btnStop.Enabled = btnReset.Enabled = btnInitialize.Enabled = false;
            await Controller.ColdStart();
        }

        private async void btnWarmStart_Click(object sender, EventArgs e)
        {
            btnColdStart.Enabled = btnWarmStart.Enabled = btnHotStart.Enabled = btnStop.Enabled = btnReset.Enabled = btnInitialize.Enabled = false;
            await Controller.WarmStart();
        }

        private async void btnHotStart_Click(object sender, EventArgs e)
        {
            btnColdStart.Enabled = btnWarmStart.Enabled = btnHotStart.Enabled = btnStop.Enabled = btnReset.Enabled = btnInitialize.Enabled = false;
            await Controller.HotStart();
        }

        private async void btnReset_Click(object sender, EventArgs e)
        {
            btnColdStart.Enabled = btnWarmStart.Enabled = btnHotStart.Enabled = btnStop.Enabled = btnReset.Enabled = btnInitialize.Enabled = false;
            await Controller.ResetPLC();
        }

        private async void btnStop_Click(object sender, EventArgs e)
        {
            btnColdStart.Enabled = btnWarmStart.Enabled = btnHotStart.Enabled = btnStop.Enabled = btnReset.Enabled = btnInitialize.Enabled = false;
            await Controller.Stop();
        }

        private async void btnInitialize_Click(object sender, EventArgs e)
        {
            btnColdStart.Enabled = btnWarmStart.Enabled = btnHotStart.Enabled = btnStop.Enabled = btnReset.Enabled = btnInitialize.Enabled = false;
            await Controller.Initialize();
        }

        private void btnFileIO_Click(object sender, EventArgs e)
        {
            frmDeviceFiles deviceFiles = new frmDeviceFiles(Controller);
            deviceFiles.Show();
        }
    }
}
