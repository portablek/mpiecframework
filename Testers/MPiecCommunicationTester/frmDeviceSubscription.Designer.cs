﻿namespace MPiecCommunicationTester
{
    partial class frmDeviceSubscription
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtSubscriptionName = new System.Windows.Forms.TextBox();
            this.txtSubscriptionValues = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numAutoreadInterval = new System.Windows.Forms.NumericUpDown();
            this.btnStartAutoread = new System.Windows.Forms.Button();
            this.btnStopAutoread = new System.Windows.Forms.Button();
            this.btnReadSubscription = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numAutoreadInterval)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Location = new System.Drawing.Point(22, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(882, 27);
            this.label1.TabIndex = 14;
            this.label1.Text = "Subscription Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSubscriptionName
            // 
            this.txtSubscriptionName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSubscriptionName.Location = new System.Drawing.Point(22, 46);
            this.txtSubscriptionName.Margin = new System.Windows.Forms.Padding(6);
            this.txtSubscriptionName.Name = "txtSubscriptionName";
            this.txtSubscriptionName.ReadOnly = true;
            this.txtSubscriptionName.Size = new System.Drawing.Size(878, 31);
            this.txtSubscriptionName.TabIndex = 15;
            // 
            // txtSubscriptionValues
            // 
            this.txtSubscriptionValues.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSubscriptionValues.Location = new System.Drawing.Point(16, 233);
            this.txtSubscriptionValues.Margin = new System.Windows.Forms.Padding(6);
            this.txtSubscriptionValues.Multiline = true;
            this.txtSubscriptionValues.Name = "txtSubscriptionValues";
            this.txtSubscriptionValues.ReadOnly = true;
            this.txtSubscriptionValues.Size = new System.Drawing.Size(878, 192);
            this.txtSubscriptionValues.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label2.Location = new System.Drawing.Point(16, 204);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(882, 27);
            this.label2.TabIndex = 16;
            this.label2.Text = "Last Read Values";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label3.Location = new System.Drawing.Point(22, 106);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(320, 27);
            this.label3.TabIndex = 18;
            this.label3.Text = "AutoRead Interval (ms)";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numAutoreadInterval
            // 
            this.numAutoreadInterval.Increment = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.numAutoreadInterval.Location = new System.Drawing.Point(22, 137);
            this.numAutoreadInterval.Margin = new System.Windows.Forms.Padding(6);
            this.numAutoreadInterval.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numAutoreadInterval.Minimum = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.numAutoreadInterval.Name = "numAutoreadInterval";
            this.numAutoreadInterval.Size = new System.Drawing.Size(320, 31);
            this.numAutoreadInterval.TabIndex = 19;
            this.numAutoreadInterval.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // btnStartAutoread
            // 
            this.btnStartAutoread.Location = new System.Drawing.Point(370, 96);
            this.btnStartAutoread.Margin = new System.Windows.Forms.Padding(6);
            this.btnStartAutoread.Name = "btnStartAutoread";
            this.btnStartAutoread.Size = new System.Drawing.Size(218, 44);
            this.btnStartAutoread.TabIndex = 20;
            this.btnStartAutoread.Text = "Start AutoRead";
            this.btnStartAutoread.UseVisualStyleBackColor = true;
            this.btnStartAutoread.Click += new System.EventHandler(this.btnStartAutoread_Click);
            // 
            // btnStopAutoread
            // 
            this.btnStopAutoread.Enabled = false;
            this.btnStopAutoread.Location = new System.Drawing.Point(370, 138);
            this.btnStopAutoread.Margin = new System.Windows.Forms.Padding(6);
            this.btnStopAutoread.Name = "btnStopAutoread";
            this.btnStopAutoread.Size = new System.Drawing.Size(218, 44);
            this.btnStopAutoread.TabIndex = 21;
            this.btnStopAutoread.Text = "Stop AutoRead";
            this.btnStopAutoread.UseVisualStyleBackColor = true;
            this.btnStopAutoread.Click += new System.EventHandler(this.btnStopAutoread_Click);
            // 
            // btnReadSubscription
            // 
            this.btnReadSubscription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReadSubscription.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadSubscription.Location = new System.Drawing.Point(680, 96);
            this.btnReadSubscription.Margin = new System.Windows.Forms.Padding(6);
            this.btnReadSubscription.Name = "btnReadSubscription";
            this.btnReadSubscription.Size = new System.Drawing.Size(218, 44);
            this.btnReadSubscription.TabIndex = 22;
            this.btnReadSubscription.Text = "Read Now";
            this.btnReadSubscription.UseVisualStyleBackColor = true;
            this.btnReadSubscription.Click += new System.EventHandler(this.btnReadSubscription_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(748, 446);
            this.btnClose.Margin = new System.Windows.Forms.Padding(6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(150, 44);
            this.btnClose.TabIndex = 23;
            this.btnClose.Text = "OK / Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // frmDeviceSubscription
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 510);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnReadSubscription);
            this.Controls.Add(this.btnStopAutoread);
            this.Controls.Add(this.btnStartAutoread);
            this.Controls.Add(this.numAutoreadInterval);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtSubscriptionValues);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSubscriptionName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "frmDeviceSubscription";
            this.Text = "Device Subscription";
            ((System.ComponentModel.ISupportInitialize)(this.numAutoreadInterval)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSubscriptionName;
        private System.Windows.Forms.TextBox txtSubscriptionValues;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numAutoreadInterval;
        private System.Windows.Forms.Button btnStartAutoread;
        private System.Windows.Forms.Button btnStopAutoread;
        private System.Windows.Forms.Button btnReadSubscription;
        private System.Windows.Forms.Button btnClose;
    }
}