﻿using MPiecFramework;
using MPiecFramework.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MPiecCommunicationTester
{
    public partial class frmDeviceSubscription : Form
    {
        IMPiec Controller;
        System.Timers.Timer readTimer;

        public frmDeviceSubscription(IMPiec Controller, string SubscriptionID)
        {
            InitializeComponent();
            this.Controller = Controller;
            txtSubscriptionName.Text = SubscriptionID;
        }

        private void UpdateSubscriptionValues(Dictionary<string, dynamic> Vals)
        {
            if (txtSubscriptionValues.InvokeRequired)
            {
                txtSubscriptionValues.Invoke(new MethodInvoker(() => UpdateSubscriptionValues(Vals)));
            }
            else
            {
                txtSubscriptionValues.Text = null;
                foreach (string variable in Vals.Keys)
                {
                    txtSubscriptionValues.Text += variable + " = " + Vals[variable].ToString() + "\r\n";
                }
            }
        }

        private async void btnReadSubscription_Click(object sender, EventArgs e)
        {
            Dictionary<string, dynamic> Vals = await Controller.ReadSubscriptionValues(txtSubscriptionName.Text);
            UpdateSubscriptionValues(Vals);
        }

        private void btnStartAutoread_Click(object sender, EventArgs e)
        {
            try
            {
                readTimer = Controller.StartSubscriptionReadTimer(txtSubscriptionName.Text, (int)numAutoreadInterval.Value,
                    (object readsender, MPIECSubscriptionReadEventArgs read_e) =>
                {
                    UpdateSubscriptionValues(read_e.VariableValues);
                });
            } 
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + (ex.InnerException != null ? ":" + ex.InnerException.Message : ""), "Unable to start autoread",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                btnStartAutoread.Enabled = (readTimer == null);
                btnStopAutoread.Enabled = (readTimer != null);
            }
        }
        private void btnStopAutoread_Click(object sender, EventArgs e)
        {
            try
            {
                Controller.StopSubscriptionReadTimer(readTimer);
                readTimer = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + (ex.InnerException != null ? ":" + ex.InnerException.Message : ""), "Unable to stop autoread",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                btnStartAutoread.Enabled = (readTimer == null);
                btnStopAutoread.Enabled = (readTimer != null);
            }
        }

        private async void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                Controller.StopSubscriptionReadTimer(readTimer);
                await Controller.Unsubscribe(txtSubscriptionName.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + (ex.InnerException != null ? ":" + ex.InnerException.Message : ""), "Unable to unsubscribe",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Close();
            }
        }

    }
}
