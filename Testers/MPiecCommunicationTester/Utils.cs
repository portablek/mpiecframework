﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPiecCommunicationTester
{
    /// <summary>
    /// General utilities and extensions
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// A collection of possible byte size
        /// </summary>
        public enum ByteSizeUnit { B, KB, MB, GB, TB, PB, EB, ZB, YB };

        /// <summary>
        /// Converts a numeric value into a string that represents the number expressed as a size value in bytes,
        /// kilobytes, megabytes, or gigabytes, depending on the size.
        /// </summary>
        /// <param name="byteSize">The numeric value to be converted.</param>
        /// <returns>The converted string.</returns>
        public static string FormatByteSize(double byteSize)
        {
            ByteSizeUnit unit = ByteSizeUnit.B;
            ByteSizeUnit maxUnit = Enum.GetValues(typeof(ByteSizeUnit)).Cast<ByteSizeUnit>().Max();
            while ((byteSize >= 1024) && (unit < maxUnit))
            {
                byteSize = byteSize / 1024;
                unit++;
            }
            return string.Format("{0:0.##} {1}", byteSize, unit);
        }

        /// <summary>
        /// Convert the value into a byte-size formatted string
        /// </summary>
        public static string ToByteSizeString(this UInt32 value)
        {
            return FormatByteSize(value);
        }
    }
}
