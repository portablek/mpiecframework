﻿using MPiecFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace MPiecCommunicationTester
{
    public partial class frmDeviceFiles : Form
    {
        private IMPiec Controller;

        public frmDeviceFiles(IMPiec Controller)
        {
            InitializeComponent();
            this.Controller = Controller;
        }

        private async void btnReadFile_Click(object sender, EventArgs e)
        {
            string fileName = txtFilename.Text;
            int pathindex = fileName.LastIndexOf('/');
            if (pathindex > 0)
                fileName = fileName.Substring(pathindex+1);

            saveFileDialog1.FileName = fileName;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                MemoryStream fileData = await Controller.GetFile(txtFilename.Text);
                if (fileData != null)
                {
                    File.WriteAllBytes(saveFileDialog1.FileName, fileData.ToArray());
                    MessageBox.Show("File data saved to " + saveFileDialog1.FileName, "File Retreived", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private async void btnWriteFile_Click(object sender, EventArgs e)
        {
            string fileName = txtFilename.Text;
            int pathindex = fileName.LastIndexOf('/');
            if (pathindex > 0)
                fileName = fileName.Substring(pathindex + 1);

            openFileDialog1.FileName = fileName;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                MemoryStream fileData = new MemoryStream();
                using (FileStream file = new FileStream(openFileDialog1.FileName, FileMode.Open, FileAccess.Read))
                    file.CopyTo(fileData);
                if (await Controller.PutFile(txtFilename.Text, fileData))
                    MessageBox.Show("Data from " + openFileDialog1.FileName + " saved to " + txtFilename.Text, "File Sent", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private async void btnDeleteFile_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Permamently remove " + txtFilename.Text + " from the device?", "Remove File", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                await Controller.DeleteFile(txtFilename.Text);
            }
        }
    }
}
