﻿namespace MPiecCommunicationTester
{
    partial class frmDeviceVariables
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpSubscriptions = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbSubscriptionName = new System.Windows.Forms.ComboBox();
            this.cmbSubscriptionDefinitions = new System.Windows.Forms.ComboBox();
            this.btnSubscribe = new System.Windows.Forms.Button();
            this.grpWrite = new System.Windows.Forms.GroupBox();
            this.cmbWriteDefinitions = new System.Windows.Forms.ComboBox();
            this.btnWriteVariables = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.grpSubscriptions.SuspendLayout();
            this.grpWrite.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpSubscriptions
            // 
            this.grpSubscriptions.Controls.Add(this.label2);
            this.grpSubscriptions.Controls.Add(this.label1);
            this.grpSubscriptions.Controls.Add(this.cmbSubscriptionName);
            this.grpSubscriptions.Controls.Add(this.cmbSubscriptionDefinitions);
            this.grpSubscriptions.Controls.Add(this.btnSubscribe);
            this.grpSubscriptions.Location = new System.Drawing.Point(20, 219);
            this.grpSubscriptions.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpSubscriptions.Name = "grpSubscriptions";
            this.grpSubscriptions.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpSubscriptions.Size = new System.Drawing.Size(1100, 292);
            this.grpSubscriptions.TabIndex = 0;
            this.grpSubscriptions.TabStop = false;
            this.grpSubscriptions.Text = "Subscriptions";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label2.Location = new System.Drawing.Point(24, 125);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1050, 27);
            this.label2.TabIndex = 13;
            this.label2.Text = "Subscription Definition";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Location = new System.Drawing.Point(24, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1050, 27);
            this.label1.TabIndex = 12;
            this.label1.Text = "Subscription Name (optional)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbSubscriptionName
            // 
            this.cmbSubscriptionName.FormattingEnabled = true;
            this.cmbSubscriptionName.Location = new System.Drawing.Point(24, 69);
            this.cmbSubscriptionName.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.cmbSubscriptionName.Name = "cmbSubscriptionName";
            this.cmbSubscriptionName.Size = new System.Drawing.Size(1046, 33);
            this.cmbSubscriptionName.TabIndex = 11;
            this.cmbSubscriptionName.Text = "Test Subscription";
            // 
            // cmbSubscriptionDefinitions
            // 
            this.cmbSubscriptionDefinitions.FormattingEnabled = true;
            this.cmbSubscriptionDefinitions.Location = new System.Drawing.Point(24, 154);
            this.cmbSubscriptionDefinitions.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.cmbSubscriptionDefinitions.Name = "cmbSubscriptionDefinitions";
            this.cmbSubscriptionDefinitions.Size = new System.Drawing.Size(1046, 33);
            this.cmbSubscriptionDefinitions.TabIndex = 9;
            this.cmbSubscriptionDefinitions.Text = "BoolAndOut:BOOL,BoolOrOut:BOOL,RealSumOutput:REAL";
            // 
            // btnSubscribe
            // 
            this.btnSubscribe.Location = new System.Drawing.Point(24, 221);
            this.btnSubscribe.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSubscribe.Name = "btnSubscribe";
            this.btnSubscribe.Size = new System.Drawing.Size(284, 46);
            this.btnSubscribe.TabIndex = 2;
            this.btnSubscribe.Text = "Create Subscription...";
            this.btnSubscribe.UseVisualStyleBackColor = true;
            this.btnSubscribe.Click += new System.EventHandler(this.btnSubscribe_Click);
            // 
            // grpWrite
            // 
            this.grpWrite.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpWrite.Controls.Add(this.cmbWriteDefinitions);
            this.grpWrite.Controls.Add(this.btnWriteVariables);
            this.grpWrite.Controls.Add(this.label4);
            this.grpWrite.Location = new System.Drawing.Point(20, 12);
            this.grpWrite.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpWrite.Name = "grpWrite";
            this.grpWrite.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpWrite.Size = new System.Drawing.Size(1100, 188);
            this.grpWrite.TabIndex = 7;
            this.grpWrite.TabStop = false;
            this.grpWrite.Text = "Variable Write";
            // 
            // cmbWriteDefinitions
            // 
            this.cmbWriteDefinitions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbWriteDefinitions.FormattingEnabled = true;
            this.cmbWriteDefinitions.Location = new System.Drawing.Point(24, 79);
            this.cmbWriteDefinitions.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.cmbWriteDefinitions.Name = "cmbWriteDefinitions";
            this.cmbWriteDefinitions.Size = new System.Drawing.Size(1046, 33);
            this.cmbWriteDefinitions.TabIndex = 3;
            this.cmbWriteDefinitions.Text = "BoolInput1:BOOL=true,BoolInput2:BOOL=false,RealInput1:REAL=1.234,RealInput2:REAL=" +
    "0.251";
            // 
            // btnWriteVariables
            // 
            this.btnWriteVariables.Location = new System.Drawing.Point(24, 131);
            this.btnWriteVariables.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnWriteVariables.Name = "btnWriteVariables";
            this.btnWriteVariables.Size = new System.Drawing.Size(156, 46);
            this.btnWriteVariables.TabIndex = 2;
            this.btnWriteVariables.Text = "Write...";
            this.btnWriteVariables.UseVisualStyleBackColor = true;
            this.btnWriteVariables.Click += new System.EventHandler(this.btnWriteVariables_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label4.Location = new System.Drawing.Point(24, 48);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(1050, 27);
            this.label4.TabIndex = 1;
            this.label4.Text = "Write Definitions";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(970, 521);
            this.btnClose.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(150, 67);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "OK / Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // frmDeviceVariables
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1136, 612);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.grpWrite);
            this.Controls.Add(this.grpSubscriptions);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmDeviceVariables";
            this.Text = "Device Variables";
            this.grpSubscriptions.ResumeLayout(false);
            this.grpWrite.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpSubscriptions;
        private System.Windows.Forms.Button btnSubscribe;
        private System.Windows.Forms.GroupBox grpWrite;
        private System.Windows.Forms.Button btnWriteVariables;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbSubscriptionName;
        private System.Windows.Forms.ComboBox cmbSubscriptionDefinitions;
        private System.Windows.Forms.ComboBox cmbWriteDefinitions;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}